package com.techwin.omatochi.utils

import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.*
import com.techwin.omatochi.ui.home_caregiver.MenuItemBean2
import com.techwin.omatochi.ui.home_payer.MenuItemBean
import com.techwin.omatochi.ui.home_payer.account.carePlans.CarePlanBean
import com.techwin.omatochi.ui.home_payer.account.carePlans.buddy.BuddyCareBean
import com.techwin.omatochi.ui.home_payer.fragment.contacts.ContactBean
import com.techwin.omatochi.ui.home_payer.fragment.dashboard.feeds.FeedsBean
import com.techwin.omatochi.ui.home_payer.fragment.past_visits.PastVisitBean
import com.techwin.omatochi.ui.home_payer.new_note.NoteHistoryModel
import com.techwin.omatochi.ui.home_payer.account.payments_invoice.CreditCardBean

class AppUtils {
    companion object {

        fun getCurrentList(): List<CurrentModel>{
            val list: ArrayList<CurrentModel> = ArrayList()
            list.add(CurrentModel(R.drawable.dummytylor, "Tylor Joseph", "Last visited on Oct 21,2019"))
            list.add(CurrentModel(R.drawable.dummytylor, "Tylor Joseph", "Last visited on Oct 21,2019"))
            list.add(CurrentModel(R.drawable.dummytylor, "Tylor Joseph", "Last visited on Oct 21,2019"))
            return list
        }

        fun getPastShiftList(): List<PastShiftModel> {
            val list: ArrayList<PastShiftModel> = ArrayList()
            list.add(
                PastShiftModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,"
                )
            )
            list.add(
                PastShiftModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,"
                )
            )
            list.add(
                PastShiftModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,"
                )
            )
            list.add(
                PastShiftModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,"
                )
            )
            list.add(
                PastShiftModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,"
                )
            )
            list.add(
                PastShiftModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,"
                )
            )
            list.add(
                PastShiftModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,"
                )
            )
            list.add(
                PastShiftModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,",
                    true
                )
            )

            return list
        }

        fun getPastList(): List<PastModel>{
            val list: ArrayList<PastModel> = ArrayList()
            list.add(PastModel(R.drawable.dummytylor, "Tylor Joseph", "Last visited on Oct 21,2019"))
            list.add(PastModel(R.drawable.dummytylor, "Tylor Joseph", "Last visited on Oct 21,2019"))
            list.add(PastModel(R.drawable.dummytylor, "Tylor Joseph", "Last visited on Oct 21,2019"))
            return list
        }

        fun getSideMenuList(): List<MenuItemBean> {
            val mList: ArrayList<MenuItemBean> = ArrayList()
            mList.add(
                MenuItemBean(
                    "Account",
                    expendable = true,
                    expended = false,
                    subTitle = getMyAccountList(true)
                )
            )
            mList.add(
                MenuItemBean(
                    "Health Records",
                    expendable = true,
                    expended = false,
                    subTitle = getHealthRecordList(true)
                )
            )
            mList.add(
                MenuItemBean(
                    "Support",
                    expendable = true,
                    expended = false,
                    subTitle = getSupportList(true)
                )
            )
            mList.add(
                MenuItemBean(
                    "About",
                    expendable = true,
                    expended = false,
                    subTitle = getAboutList(true)
                )
            )
            /* mList.add(
                 MenuItemBean(
                     "Privacy Policy",
                     expendable = false,
                     expended = false,
                     subTitle = getMyAccountList(false)
                 )
             )
             mList.add(
                 MenuItemBean(
                     "FAQs",
                     expendable = false,
                     expended = false,
                     subTitle = getMyAccountList(false)
                 )
             )
             mList.add(
                 MenuItemBean(
                     "Support",
                     expendable = false,
                     expended = false,
                     subTitle = getMyAccountList(false)
                 )
             )*/
            return mList
        }

        fun getSideMenuList2(): List<MenuItemBean2> {
            val mList: ArrayList<MenuItemBean2> = ArrayList()
            mList.add(
                MenuItemBean2(
                    "Help Guide",
                    expendable = false,
                    expended = true,
                    subTitle = getMyAccountList2(false)
                )
            )
            mList.add(
                MenuItemBean2(
                    "My Profile",
                    expendable = true,
                    expended = true,
                    subTitle = getMyAccountList2(true)
                )
            )
            mList.add(
                MenuItemBean2(
                    "Shift",
                    expendable = false,
                    expended = true,
                    subTitle = getMyAccountList2(false)
                )
            )
            mList.add(
                MenuItemBean2(
                    "Rate Us",
                    expendable = false,
                    expended = true,
                    subTitle = getMyAccountList2(false)
                )
            )

            mList.add(
                MenuItemBean2(
                    "Terms and Conditions",
                    expendable = false,
                    expended = true,
                    subTitle = getMyAccountList2(false)
                )
            )
            mList.add(
                MenuItemBean2(
                    "Privacy Policy",
                    expendable = false,
                    expended = true,
                    subTitle = getMyAccountList2(false)
                )
            )
            mList.add(
                MenuItemBean2(
                    "FAQs",
                    expendable = false,
                    expended = true,
                    subTitle = getMyAccountList2(false)
                )
            )

            mList.add(
                MenuItemBean2(
                    "Support",
                    expendable = false,
                    expended = true,
                    subTitle = getMyAccountList2(false)
                )
            )
            return mList
        }

        private fun getMyAccountList(check: Boolean): List<String> {
            val mList: ArrayList<String> = ArrayList()
            if (check) {
                mList.add("Profile")
                mList.add("Billing & Payments")
                mList.add("Care Plans")
            }
            return mList
        }

        private fun getHealthRecordList(check: Boolean): List<String> {
            val mList: ArrayList<String> = ArrayList()
            if (check) {
                mList.add("Medication")
                mList.add("Allergy")
                mList.add("Exercise")
            }
            return mList
        }

        private fun getSupportList(check: Boolean): List<String> {
            val mList: ArrayList<String> = ArrayList()
            if (check) {
                mList.add("Omatochi Support")
                mList.add("User Guide (FAQs)")
            }
            return mList
        }

        private fun getAboutList(check: Boolean): List<String> {
            val mList: ArrayList<String> = ArrayList()
            if (check) {
                mList.add("App Version")
                mList.add("Terms & Conditions")
                mList.add("Privacy Policy")
                mList.add("Legal Info")
            }
            return mList
        }

        private fun getMyAccountList2(check: Boolean): List<String> {
            val mList: ArrayList<String> = ArrayList()
            if (check) {
                mList.add("Profile")
                mList.add("Banking")
                mList.add("Care Plans")
            }
            return mList
        }

        fun getNewShiftList(): List<NewShiftModel> {
            val list: ArrayList<NewShiftModel> = ArrayList()
            list.add(
                NewShiftModel(
                    "Josh Hazel",
                    " Timing: 13:00-17:00",
                    "Bond Street, Half Valley, USA",
                    R.drawable.dummytylor
                )
            )
            list.add(
                NewShiftModel(
                    "Josh Hazel",
                    " Timing: 13:00-17:00",
                    "Bond Street, Half Valley, USA",
                    R.drawable.dummytylor
                )
            )
            list.add(
                NewShiftModel(
                    "Josh Hazel",
                    " Timing: 13:00-17:00",
                    "Bond Street, Half Valley, USA",
                    R.drawable.dummytylor,true
                )
            )
            list.add(
                NewShiftModel(
                    "Josh Hazel",
                    " Timing: 13:00-17:00",
                    "Bond Street, Half Valley, USA",
                    R.drawable.dummytylor
                )
            )
            list.add(
                NewShiftModel(
                    "Josh Hazel",
                    " Timing: 13:00-17:00",
                    "Bond Street, Half Valley, USA",
                    R.drawable.dummytylor,
                )
            )
            list.add(
                NewShiftModel(
                    "Josh Hazel",
                    " Timing: 13:00-17:00",
                    "Bond Street, Half Valley, USA",
                    R.drawable.dummytylor
                )
            )
            return list
        }

        fun getNewVisitItems(): List<NewVisitModel> {
            val list: ArrayList<NewVisitModel> = ArrayList()
            list.add(
                NewVisitModel(
                    "Activity", R.drawable.ic_yoga
                )
            )
            list.add(
                NewVisitModel(
                    "Medication", R.drawable.ic_hand
                )
            )
            list.add(
                NewVisitModel(
                    "Exercise", R.drawable.ic_exercise
                )
            )
            list.add(
                NewVisitModel(
                    "Diet", R.drawable.ic_diet
                )
            )
            return list
        }

        fun getMealType(): List<MealTypeModel> {
            val list: ArrayList<MealTypeModel> = ArrayList()
            list.add(
                MealTypeModel(
                    "Breakfast", R.drawable.ic_breakfast
                )
            )
            list.add(
                MealTypeModel(
                    "Lunch", R.drawable.ic_lunch
                )
            )
            list.add(
                MealTypeModel(
                    "Dinner", R.drawable.ic_dinner
                )
            )
            list.add(
                MealTypeModel(
                    "Snacks", R.drawable.ic_snacks
                )
            )
            return list
        }


        fun getPastVisitList(): List<PastVisitBean> {
            val mList: ArrayList<PastVisitBean> = ArrayList()
            mList.add(PastVisitBean(""))
            mList.add(PastVisitBean(""))
            mList.add(PastVisitBean(""))
            mList.add(PastVisitBean(""))
            mList.add(PastVisitBean(""))
            mList.add(PastVisitBean(""))
            mList.add(PastVisitBean(""))
            mList.add(PastVisitBean(""))
            mList.add(PastVisitBean(""))
            mList.add(PastVisitBean(""))
            mList.add(PastVisitBean(""))
            mList.add(PastVisitBean(""))
            return mList
        }

        fun getCreditCardList(): ArrayList<CreditCardBean> {
            val list = ArrayList<CreditCardBean>()
            list?.apply {
                add(
                    CreditCardBean(
                        R.drawable.diners,
                        "Adam Milne",
                        "04/22",
                        "xxxx-xxxx-xxxx-4242",
                        "On",
                        false
                    )
                )
                add(
                    CreditCardBean(
                        R.drawable.discover,
                        "Adam Milne",
                        "04/22",
                        "xxxx-xxxx-xxxx-4242",
                        "On",
                        false
                    )
                )
                add(
                    CreditCardBean(
                        R.drawable.visaa,
                        "Adam Milne",
                        "04/22",
                        "xxxx-xxxx-xxxx-4242",
                        "On",
                        false
                    )
                )
                add(
                    CreditCardBean(
                        R.drawable.jcb,
                        "Adam Milne",
                        "04/22",
                        "xxxx-xxxx-xxxx-4242",
                        "On",
                        false
                    )
                )
                add(
                    CreditCardBean(
                        R.drawable.amex,
                        "Adam Milne",
                        "04/22",
                        "xxxx-xxxx-xxxx-4242",
                        "On",
                        false
                    )
                )
                add(
                    CreditCardBean(
                        R.drawable.master_,
                        "Adam Milne",
                        "04/22",
                        "xxxx-xxxx-xxxx-4242",
                        "On",
                        false
                    )
                )
            }
            return list
        }

        fun getCarePlanList(): List<CarePlanBean> {
            val list: ArrayList<CarePlanBean> = ArrayList()
            list.add(CarePlanBean(R.drawable.environment_care, "Buddy Care", "15 Hours/Week", false))
            list.add(CarePlanBean(R.drawable.person_home, "Companion Care", "25 Hours/Week", true))
            list.add(
                CarePlanBean(
                    R.drawable.trust,
                    "Champion Care",
                    "35 Hours/Week",
                    false
                )
            )
            return list
        }

        fun getBuddyCarePlanList(): List<BuddyCareBean> {
            val mList: ArrayList<BuddyCareBean> = ArrayList()
            mList.add(BuddyCareBean("Up to 75 miles per week of driving"))
            mList.add(BuddyCareBean("Companionship for 2 external social activities per month "))
            mList.add(BuddyCareBean("Transportation to medical visits [appointments to the doctor, lab)"))
            mList.add(BuddyCareBean("Customized social calendar "))
            mList.add(BuddyCareBean("Light housekeeping"))
            mList.add(BuddyCareBean("Personal Care"))
            mList.add(BuddyCareBean("Family and user access to Omatochi App"))
            mList.add(BuddyCareBean("Monthly care reports pickup"))
            mList.add(BuddyCareBean("Dedicated Family Care Advisor"))
            return mList
        }

        fun getContactList(): List<ContactBean> {
            val list: ArrayList<ContactBean> = ArrayList()
            //list.add(ContactBean("Family", R.drawable.ic_family, false))
            list.add(ContactBean("Care Provider", R.drawable.ic_care_provider, false))
            return list
        }

        fun getFeedList(): List<FeedsBean> {
            val list: ArrayList<FeedsBean> = ArrayList()
            list.add(FeedsBean("", false))
            list.add(FeedsBean("", false))
            list.add(FeedsBean("", false))
            list.add(FeedsBean("", false))
            list.add(FeedsBean("", false))
            list.add(FeedsBean("", false))
            list.add(FeedsBean("", false))
            list.add(FeedsBean("", false))
            list.add(FeedsBean("", false))
            list.add(FeedsBean("", false))
            return list
        }

        fun getInActiveMedList(): List<InActiveMedicationModel> {
            val list: ArrayList<InActiveMedicationModel> = ArrayList()
            list.add(InActiveMedicationModel("Amlodipine", "", "", "", false))
            list.add(InActiveMedicationModel("Cytarabine", "", "", "", false))
            list.add(InActiveMedicationModel("Dexamethasone", "", "", "", false))
            list.add(InActiveMedicationModel("Mitotane", "", "", "", false))
            list.add(InActiveMedicationModel("Imatinib New", "", "", "", false))
            list.add(InActiveMedicationModel("Emicizumab", "", "", "", false))
            return list
        }

        fun getInActiveAllergyList(): List<AllergyModel> {
            val list: ArrayList<AllergyModel> = ArrayList()
            list.add(
                AllergyModel(
                    "Drug Allergy",
                    "Allergic diseases mediated by T helper type (Th) 2 cell immune responses are rising dramatically in most developed countries. Exaggerated Th2 cell reactivity could result, for example, from diminished exposure to Th1 cell-inducing microbial infections. Epidemiological studies, however, indicate that Th2 cell-stimulating helminth parasites may also counteract allergies, possibly by generating regulatory T cells which suppress both Th1 and Th2 arms of immunity.",
                    false
                )
            )
            list.add(
                AllergyModel(
                    "Drug Allergy",
                    "Allergic diseases mediated by T helper type (Th) 2 cell immune responses are rising dramatically in most developed countries. Exaggerated Th2 cell reactivity could result, for example, from diminished exposure to Th1 cell-inducing microbial infections. Epidemiological studies, however, indicate that Th2 cell-stimulating helminth parasites may also counteract allergies, possibly by generating regulatory T cells which suppress both Th1 and Th2 arms of immunity.",
                    false
                )
            )
            list.add(
                AllergyModel(
                    "Drug Allergy",
                    "Allergic diseases mediated by T helper type (Th) 2 cell immune responses are rising dramatically in most developed countries. Exaggerated Th2 cell reactivity could result, for example, from diminished exposure to Th1 cell-inducing microbial infections. Epidemiological studies, however, indicate that Th2 cell-stimulating helminth parasites may also counteract allergies, possibly by generating regulatory T cells which suppress both Th1 and Th2 arms of immunity.",
                    false
                )
            )
            list.add(
                AllergyModel(
                    "Drug Allergy",
                    "Allergic diseases mediated by T helper type (Th) 2 cell immune responses are rising dramatically in most developed countries. Exaggerated Th2 cell reactivity could result, for example, from diminished exposure to Th1 cell-inducing microbial infections. Epidemiological studies, however, indicate that Th2 cell-stimulating helminth parasites may also counteract allergies, possibly by generating regulatory T cells which suppress both Th1 and Th2 arms of immunity.",
                    false
                )
            )
            list.add(
                AllergyModel(
                    "Drug Allergy",
                    "Allergic diseases mediated by T helper type (Th) 2 cell immune responses are rising dramatically in most developed countries. Exaggerated Th2 cell reactivity could result, for example, from diminished exposure to Th1 cell-inducing microbial infections. Epidemiological studies, however, indicate that Th2 cell-stimulating helminth parasites may also counteract allergies, possibly by generating regulatory T cells which suppress both Th1 and Th2 arms of immunity.",
                    false
                )
            )
            list.add(
                AllergyModel(
                    "Drug Allergy",
                    "Allergic diseases mediated by T helper type (Th) 2 cell immune responses are rising dramatically in most developed countries. Exaggerated Th2 cell reactivity could result, for example, from diminished exposure to Th1 cell-inducing microbial infections. Epidemiological studies, however, indicate that Th2 cell-stimulating helminth parasites may also counteract allergies, possibly by generating regulatory T cells which suppress both Th1 and Th2 arms of immunity.",
                    false
                )
            )
            list.add(
                AllergyModel(
                    "Drug Allergy",
                    "Allergic diseases mediated by T helper type (Th) 2 cell immune responses are rising dramatically in most developed countries. Exaggerated Th2 cell reactivity could result, for example, from diminished exposure to Th1 cell-inducing microbial infections. Epidemiological studies, however, indicate that Th2 cell-stimulating helminth parasites may also counteract allergies, possibly by generating regulatory T cells which suppress both Th1 and Th2 arms of immunity.",
                    false
                )
            )
            return list
        }

        fun getContactFavList(): List<ContactBean> {
            val list: ArrayList<ContactBean> = ArrayList()
            list.add(ContactBean("Annie Winthrop", R.drawable.ic_dummy_1, false))
            list.add(ContactBean("Annie Winthrop", R.drawable.ic_dummy_1, false))
            list.add(ContactBean("Annie Winthrop", R.drawable.ic_dummy_1, false))
            list.add(ContactBean("Annie Winthrop", R.drawable.ic_dummy_1, false))
            return list
        }

        fun getFavList(): List<ContactBean> {
            val list: ArrayList<ContactBean> = ArrayList()
            list.add(ContactBean("Annie Winthrop", R.drawable.ic_dummy_1, true))
            list.add(ContactBean("Annie Winthrop", R.drawable.ic_dummy_1, true))
            list.add(ContactBean("Annie Winthrop", R.drawable.ic_dummy_1, true))
            list.add(ContactBean("Annie Winthrop", R.drawable.ic_dummy_1, true))
            return list
        }

        fun getNoteHistoryList(): List<NoteHistoryModel> {
            val list: ArrayList<NoteHistoryModel> = ArrayList()
            list.add(
                NoteHistoryModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,"
                )
            )
            list.add(
                NoteHistoryModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,"
                )
            )
            list.add(
                NoteHistoryModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,"
                )
            )
            list.add(
                NoteHistoryModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,"
                )
            )
            list.add(
                NoteHistoryModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,"
                )
            )
            list.add(
                NoteHistoryModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,"
                )
            )
            list.add(
                NoteHistoryModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,"
                )
            )
            list.add(
                NoteHistoryModel(
                    "08:45",
                    "Annie Winthrop",
                    "Betty had a great lunch today,Betty had a great lunch today,Betty had a great lunch today,",
                    true
                )
            )
            return list
        }


        fun getInboxMessagesList2(): List<InboxMessagesModelCG> {
            val list: ArrayList<InboxMessagesModelCG> = ArrayList()
            list.add(
                InboxMessagesModelCG(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModelCG(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModelCG(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModelCG(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModelCG(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModelCG(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModelCG(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModelCG(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModelCG(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModelCG(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModelCG(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModelCG(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModelCG(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )

            return list
        }

        fun getNoteList1(): List<NotesModel> {
            val list: ArrayList<NotesModel> = ArrayList()
            list.add(
                NotesModel(
                    "08:45", "Samantha Fox", "Lorem psum has been the industry's\n" +
                            "standard dummy but also the leap "
                )
            )
            list.add(
                NotesModel(
                    "08:45", "Samantha Fox", "Lorem psum has been the industry's\n" +
                            "standard dummy but also the leap "
                )
            )
            list.add(
                NotesModel(
                    "08:45", "Samantha Fox", "Lorem psum has been the industry's\n" +
                            "standard dummy but also the leap "
                )
            )
            list.add(
                NotesModel(
                    "08:45", "Samantha Fox", "Lorem psum has been the industry's\n" +
                            "standard dummy but also the leap "
                )
            )
            list.add(
                NotesModel(
                    "08:45", "Samantha Fox", "Lorem psum has been the industry's\n" +
                            "standard dummy but also the leap "
                )
            )
            list.add(
                NotesModel(
                    "08:45", "Samantha Fox", "Lorem psum has been the industry's\n" +
                            "standard dummy but also the leap "
                ,true
                )
            )
            return list
        }

        fun getNoteList(): List<NoteHistoryModel> {
            val list: ArrayList<NoteHistoryModel> = ArrayList()
            list.add(
                NoteHistoryModel(
                    "08:45", "Samantha Fox", "Lorem psum has been the industry's\n" +
                            "standard dummy but also the leap "
                )
            )
            list.add(
                NoteHistoryModel(
                    "08:45", "Samantha Fox", "Lorem psum has been the industry's\n" +
                            "standard dummy but also the leap "
                )
            )
            list.add(
                NoteHistoryModel(
                    "08:45", "Samantha Fox", "Lorem psum has been the industry's\n" +
                            "standard dummy but also the leap "
                )
            )
            list.add(
                NoteHistoryModel(
                    "08:45", "Samantha Fox", "Lorem psum has been the industry's\n" +
                            "standard dummy but also the leap "
                )
            )
            list.add(
                NoteHistoryModel(
                    "08:45", "Samantha Fox", "Lorem psum has been the industry's\n" +
                            "standard dummy but also the leap "
                )
            )
            list.add(
                NoteHistoryModel(
                    "08:45", "Samantha Fox", "Lorem psum has been the industry's\n" +
                            "standard dummy but also the leap "
                )
            )
            return list
        }

        fun getNotificationList(): List<MessageNotificationsModel> {
            val list: ArrayList<MessageNotificationsModel> = ArrayList()
            list.add(
                MessageNotificationsModel(
                    R.drawable.dummytylor,
                    "Tylor add a new photo for exercise.",
                    R.drawable.dummy_exercise,
                    "34 min ago"
                )
            )
            list.add(
                MessageNotificationsModel(
                    R.drawable.dummytylor,
                    "Tylor add a new photo for exercise.",
                    R.drawable.dummy_exercise,
                    "34 min ago"
                )
            )
            list.add(
                MessageNotificationsModel(
                    R.drawable.dummytylor,
                    "Tylor add a new photo for exercise.",
                    R.drawable.dummy_exercise,
                    "34 min ago"
                )
            )
            list.add(
                MessageNotificationsModel(
                    R.drawable.dummytylor,
                    "Tylor add a new photo for exercise.",
                    R.drawable.dummy_exercise,
                    "34 min ago"
                )
            )
            list.add(
                MessageNotificationsModel(
                    R.drawable.dummytylor,
                    "Tylor add a new photo for exercise.",
                    R.drawable.dummy_exercise,
                    "34 min ago"
                )
            )
            return list
        }

        fun getInboxMessagesList(): List<InboxMessagesModel> {
            val list: ArrayList<InboxMessagesModel> = ArrayList()
            list.add(
                InboxMessagesModel(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModel(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModel(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModel(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModel(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModel(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModel(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModel(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModel(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModel(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModel(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModel(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )
            list.add(
                InboxMessagesModel(
                    R.drawable.dummytylor,
                    "Tylor Joseph",
                    "10:20 PM",
                    "Hey , what's up man , Listen to my new here.",
                    "2"
                )
            )

            return list
        }

        fun getAllCommentsList(): List<AllCommentsModel> {
            val list: ArrayList<AllCommentsModel> = ArrayList()
            list.add(
                AllCommentsModel(
                    "Alan Topley",
                    R.drawable.ic_user_demo_image,
                    "Betty has a great lunch today, She even wants to go for a walk after lunch. She looks happy throughout the day and doing well",
                    "35 m"
                )
            )
            list.add(
                AllCommentsModel(
                    "Alan Topley",
                    R.drawable.ic_user_demo_image,
                    "Betty has a great lunch today, She even wants to go for a walk after lunch. She looks happy throughout the day and doing well",
                    "35 m"
                )
            )
            list.add(
                AllCommentsModel(
                    "Alan Topley",
                    R.drawable.ic_user_demo_image,
                    "Betty has a great lunch today, She even wants to go for a walk after lunch. She looks happy throughout the day and doing well",
                    "35 m"
                )
            )
            list.add(
                AllCommentsModel(
                    "Alan Topley",
                    R.drawable.ic_user_demo_image,
                    "Betty has a great lunch today, She even wants to go for a walk after lunch. She looks happy throughout the day and doing well",
                    "35 m"
                )
            )
            list.add(
                AllCommentsModel(
                    "Alan Topley",
                    R.drawable.ic_user_demo_image,
                    "Betty has a great lunch today, She even wants to go for a walk after lunch. She looks happy throughout the day and doing well",
                    "35 m"
                )
            )
            list.add(
                AllCommentsModel(
                    "Alan Topley",
                    R.drawable.ic_user_demo_image,
                    "Betty has a great lunch today, She even wants to go for a walk after lunch. She looks happy throughout the day and doing well",
                    "35 m"
                )
            )
            list.add(
                AllCommentsModel(
                    "Alan Topley",
                    R.drawable.ic_user_demo_image,
                    "Betty has a great lunch today, She even wants to go for a walk after lunch. She looks happy throughout the day and doing well",
                    "35 m"
                )
            )
            list.add(
                AllCommentsModel(
                    "Alan Topley",
                    R.drawable.ic_user_demo_image,
                    "Betty has a great lunch today, She even wants to go for a walk after lunch. She looks happy throughout the day and doing well",
                    "35 m"
                )
            )
            list.add(
                AllCommentsModel(
                    "Alan Topley",
                    R.drawable.ic_user_demo_image,
                    "Betty has a great lunch today, She even wants to go for a walk after lunch. She looks happy throughout the day and doing well",
                    "35 m"
                )
            )
            list.add(
                AllCommentsModel(
                    "Alan Topley",
                    R.drawable.ic_user_demo_image,
                    "Betty has a great lunch today, She even wants to go for a walk after lunch. She looks happy throughout the day and doing well",
                    "35 m"
                )
            )
            list.add(
                AllCommentsModel(
                    "Alan Topley",
                    R.drawable.ic_user_demo_image,
                    "Betty has a great lunch today, She even wants to go for a walk after lunch. She looks happy throughout the day and doing well",
                    "35 m"
                )
            )

            return list
        }

        fun getSupportOptionsList(): List<String> {
            val list: ArrayList<String> = ArrayList()
            list.add("Mobile Application")
            list.add("Billing/Payment")
            list.add("Care Plan")
            list.add("Other")
            return list
        }

        fun getAddNewNoteTypeList(): List<String> {
            val list: ArrayList<String> = ArrayList()
            list.add("Please Select")
            list.add("Activity")
            list.add("Medication")
            list.add("Exercise")
            list.add("Diet")
            return list
        }

        fun getAddNewAllergyTypeList(): List<String> {
            val list: ArrayList<String> = ArrayList()
            list.add("Food Allergy")
            list.add("Drug Allergy")
            list.add("Environmental Allergy")
            list.add("Other")
            return list
        }

        fun getMonth(month: Int): String {
            when (month.toString()) {
                "01", "1" -> {
                    return "Jan"
                }
                "02", "2" -> {
                    return "Feb"
                }
                "03", "3" -> {
                    return "Mar"
                }
                "04", "4" -> {
                    return "Apr"
                }
                "05", "5" -> {
                    return "May"
                }
                "06", "6" -> {
                    return "June"
                }
                "07", "7" -> {
                    return "July"
                }
                "08", "8" -> {
                    return "Aug"
                }
                "09", "9" -> {
                    return "Sept"
                }
                "10" -> {
                    return "Oct"
                }
                "11" -> {
                    return "Nov"
                }
                "12" -> {
                    return "Dec"
                }

            }
            return ""
        }


        fun getNotificationList2(): List<MessageNotificationsModelCG> {
            val list: ArrayList<MessageNotificationsModelCG> = ArrayList()
            list.add(
                MessageNotificationsModelCG(
                    R.drawable.dummytylor,
                    "Tylor add a new photo for exercise.",
                    R.drawable.dummy_exercise,
                    "34 min ago"
                )
            )
            list.add(
                MessageNotificationsModelCG(
                    R.drawable.dummytylor,
                    "Tylor add a new photo for exercise.",
                    R.drawable.dummy_exercise,
                    "34 min ago"
                )
            )
            list.add(
                MessageNotificationsModelCG(
                    R.drawable.dummytylor,
                    "Tylor add a new photo for exercise.",
                    R.drawable.dummy_exercise,
                    "34 min ago"
                )
            )
            list.add(
                MessageNotificationsModelCG(
                    R.drawable.dummytylor,
                    "Tylor add a new photo for exercise.",
                    R.drawable.dummy_exercise,
                    "34 min ago"
                )
            )
            list.add(
                MessageNotificationsModelCG(
                    R.drawable.dummytylor,
                    "Tylor add a new photo for exercise.",
                    R.drawable.dummy_exercise,
                    "34 min ago"
                )
            )
            return list
        }
    }
}