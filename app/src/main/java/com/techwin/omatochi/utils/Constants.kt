package com.techwin.omatochi.utils

class Constants {
    companion object{
        const val GOOGLE_PLACE_KEY = "AIzaSyCukUHv0Ysf-itKqezsVaaBbNjrtm03hp4"

        const val MODEL_CLASS_PASS = "model_class_pass"

        const val FRESHDESK_DOMAIN = "msdk.freshchat.com"
        const val FRESHDESK_APPID = "8ea37c0e-80da-4b11-9446-8427eb76d2da"
        const val FRESHDESK_APP_KEY = "50bf64bc-b84e-43a1-9849-ca05ab581c57"

        const val GOOGLE_VIEWER_URL = "https://drive.google.com/viewerng/viewer?embedded=true&url="
        const val CONTACT_US_URL = "https://omatochi.com/contact-us/"
        const val FAQ_URL = "https://omatochi.com/faqs"
        const val TERMS_CONDITIONS_URL = "https://www.lipsum.com/privacy.pdf"
        const val PRIVACY_POLICY_URL = "https://www.lipsum.com/privacy.pdf"
    }
}