package com.techwin.omatochi.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}