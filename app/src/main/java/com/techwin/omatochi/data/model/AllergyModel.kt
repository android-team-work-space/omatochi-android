package com.techwin.omatochi.data.model

class AllergyModel(
    var allergyName: String,
    var allergyDes: String,
    var expended: Boolean
    )