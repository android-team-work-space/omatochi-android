package com.techwin.omatochi.data.model

data class MessageNotificationsModel(
    var profile_pic: Int,
    var message: String,
    var exercise_image: Int,
    var time: String,
)
