package com.techwin.omatochi.data.model

class NewShiftModel (
    var name: String,
    var time: String,
    var address: String,
    var image: Int,
    var clockInVisibility: Boolean = false)
