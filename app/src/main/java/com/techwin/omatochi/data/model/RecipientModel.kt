package com.techwin.omatochi.data.model

class RecipientModel(
    var image: Int,
    var name: String,
    var date: String
)