package com.techwin.omatochi.data.model

class PastModel(
    var image: Int,
    var name: String,
    var date: String
)