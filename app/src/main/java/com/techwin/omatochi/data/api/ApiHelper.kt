package com.techwin.omatochi.data.api

import com.techwin.omatochi.data.model.User
import retrofit2.Response

interface ApiHelper {

    suspend fun getUsers(): Response<List<User>>
}