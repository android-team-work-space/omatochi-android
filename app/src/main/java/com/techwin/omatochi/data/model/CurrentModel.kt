package com.techwin.omatochi.data.model

class CurrentModel(
    var image: Int,
    var name: String,
    var date: String
)