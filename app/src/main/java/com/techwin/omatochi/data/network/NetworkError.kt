package com.techwinlabs.tracknic.network

class NetworkError(val errorCode: Int, override val message: String?) : Throwable(message)
