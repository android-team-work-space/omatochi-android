package com.techwin.omatochi.data.model

class AllCommentsModel(
    var name: String,
    var img: Int,
    var des: String,
    var time: String
)