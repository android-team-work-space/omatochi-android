package com.techwin.omatochi.data.model

class NotesModel(
    var time: String,
    var name: String,
    var des: String,
    var lastLineView: Boolean = false
)