package com.techwin.omatochi.data.model

data class InboxMessagesModelCG(
    var profile_pic: Int,
    var name: String,
    var time: String,
    var message: String,
    var messageCount: String,
)
