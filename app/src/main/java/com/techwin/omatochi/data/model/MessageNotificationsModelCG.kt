package com.techwin.omatochi.data.model

data class MessageNotificationsModelCG(
    var profile_pic: Int,
    var message: String,
    var exercise_image: Int,
    var time: String,
)
