package com.techwin.omatochi.data.model

class InActiveMedicationModel(
    var medName: String,
    var medFreq: String,
    var medStrength: String,
    var medDose: String,
    var expended: Boolean,
)