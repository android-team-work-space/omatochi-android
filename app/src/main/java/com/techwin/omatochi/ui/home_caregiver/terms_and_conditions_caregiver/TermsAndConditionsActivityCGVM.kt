package com.techwin.omatochi.ui.home_caregiver.terms_and_conditions_caregiver

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class TermsAndConditionsActivityCGVM @Inject constructor() : BaseViewModel()
