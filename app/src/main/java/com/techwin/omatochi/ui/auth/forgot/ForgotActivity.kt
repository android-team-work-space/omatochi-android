package com.techwin.omatochi.ui.auth.forgot

import android.app.Activity
import android.content.Intent
import androidx.activity.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.AndroidEntryPoint

import com.techwin.omatochi.databinding.ActivityForgotBinding
import com.techwin.omatochi.ui.auth.verification.VerificationActivity


@AndroidEntryPoint
class ForgotActivity : BaseActivity<ActivityForgotBinding>() {

    private val viewModel: ForgotActivityVM by viewModels()

    companion object {
        fun newIntent(activity: Activity): Intent {
            val intent = Intent(activity, ForgotActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_forgot
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnclick()
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnForgot -> {
                    if (binding.etEmail.text.toString() == "") {
                        showToast("Please enter email")
                    } else {
                        val intent = VerificationActivity.newIntent(this)
                        startActivity(intent)
                    }
                }
            }
        }
    }

    private fun initView() {
        binding.toolBar.tvTitle.text = getString(R.string.forgot_password2)
    }

}