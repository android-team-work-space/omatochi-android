package com.techwin.omatochi.ui.home_caregiver.profile

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.view.View
import android.widget.DatePicker
import android.widget.PopupMenu
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ProfileCaregiverBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_caregiver.fragment.profile.CareGiverProfileVM
import com.techwin.omatochi.utils.Constants
import com.techwin.omatochi.utils.permission.Permission
import com.techwin.omatochi.utils.permission.PermissionHandler
import com.techwin.omatochi.utils.showErrorToast
import com.techwin.omatochi.utils.showInfoToast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CareGiverProfile : BaseActivity<ProfileCaregiverBinding>(),
    DatePickerDialog.OnDateSetListener {
    private val viewModel: CareGiverProfileVM by viewModels()
    private lateinit var popupGender: PopupMenu
    private lateinit var popupSpecialisation: PopupMenu

    companion object{
        fun newIntent(activity: Activity): Intent{
            val intent = Intent(activity, CareGiverProfile::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }
    }
    
    override fun getLayoutResource(): Int {
        return R.layout.profile_caregiver
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initOnClick()
        initView()
        initPlaceSearch()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.etGender -> {
                    initGenderPopup(binding.etGender)
                }
                R.id.etDateOfBirth -> {
                    openDatePicker()
                }
                R.id.etAddress -> {
                    openPlaceSearchFragment()
                }
                R.id.etSpecialisation -> {
                    initSpecialisationPopup(binding.etSpecialisation)
                }

                R.id.ivEditProfile -> {
                    getLocalImage()
                }
                R.id.btnSave -> {
                    showInfoToast("Saved")
                }
            }
        }
    }
    
    private fun getLocalImage() {
        Permission.check(this, arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
        ), 0, Permission.Options(),
            object : PermissionHandler() {
                override fun onGranted() {
                    ImagePicker.with(this@CareGiverProfile)
                        .crop()
                        .start()
                }
            })
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            val uri = data.data
            binding.ivImage.setImageURI(uri)
        }
    }

    private var resultPlaceSearch = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        try {
            if (result.resultCode == Activity.RESULT_OK) {
                val place = Autocomplete.getPlaceFromIntent(result.data!!)
                binding.etAddress.setText(place.address)
            }
        } catch (e: Exception) {
            showErrorToast("Place result Exception : " + e.message)
        }

    }

    private fun initPlaceSearch() {
        Places.initialize(this, Constants.GOOGLE_PLACE_KEY)
    }

    private fun openPlaceSearchFragment() {
        val fields = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS)
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
            .build(this)
        resultPlaceSearch.launch(intent)
    }

    private fun initGenderPopup(view: View) {
        popupGender = PopupMenu(this, view)
        popupGender.inflate(R.menu.popup_select_gender)
        popupGender.setOnMenuItemClickListener { menuItem ->
            binding.etGender.setText(menuItem.title.toString())
            true
        }
        popupGender.show()
    }


    private fun initSpecialisationPopup(view: View){
        popupSpecialisation = PopupMenu(this, view)
        popupSpecialisation.inflate(R.menu.popup_select_specialisation)
        popupSpecialisation.setOnMenuItemClickListener {
            binding.etSpecialisation.setText(it.title.toString())
            if (it.title.toString() == "Other"){
                binding.tvSpecialisationOther.visibility = View.VISIBLE
                binding.etSpecialisationOther.visibility = View.VISIBLE
            }else{
                binding.tvSpecialisationOther.visibility = View.GONE
                binding.etSpecialisationOther.visibility = View.GONE
            }
            true
        }
        popupSpecialisation.show()
    }

    private fun initView() {
        window.statusBarColor = ContextCompat.getColor(this, R.color.green)
        binding.toolBar.tvTitle.text = getString(R.string.edit_profile)
        binding.toolBar.ivBack.setColorFilter(ContextCompat.getColor(this, R.color.white))
        binding.toolBar.tvTitle.setTextColor(ContextCompat.getColor(this, R.color.white))
    }

    private fun openDatePicker() {
        val myCalendar = java.util.Calendar.getInstance()
        val year = myCalendar.get(java.util.Calendar.YEAR)
        val month = myCalendar.get(java.util.Calendar.MONTH)
        val day = myCalendar.get(java.util.Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(this, this, year, month, day)
        datePickerDialog.show()
    }

    @SuppressLint("SetTextI18n")
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        binding.etDateOfBirth.setText("$dayOfMonth/$month/$year")
    }
}