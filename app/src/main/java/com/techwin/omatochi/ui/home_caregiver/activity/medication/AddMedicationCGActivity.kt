package com.techwin.omatochi.ui.home_caregiver.activity.medication

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.provider.MediaStore
import android.view.View
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityAddMedicationCaregiverBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddMedicationCGActivity : BaseActivity<ActivityAddMedicationCaregiverBinding>(),
    DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private val viewModel: AddMedicationCGActivityVM by viewModels()
    private val MY_CAMERA_PERMISSION_CODE = 100


    override fun getLayoutResource(): Int {
        return R.layout.activity_add_medication_caregiver
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView() {
        initView()
        initOnClick()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnAddMedication -> {
                    when {
                        binding.etMedicationType.text.toString() == "" -> {
                            showToast("Please select medication type.")
                        }
                        binding.etDate.text.toString() == "" -> {
                            showToast("please select a date.")
                        }
                        binding.etTime.text.toString() == "" -> {
                            showToast("please select a time.")
                        }
                        binding.etNote.text.toString() == "" -> {
                            showToast("please enter note.")
                        }
                        else -> {
                            showToast("Medication Will be added")
                        }
                    }
                }
                R.id.llAddVideoImage -> {
                    if (checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                            arrayOf(android.Manifest.permission.CAMERA),
                            MY_CAMERA_PERMISSION_CODE
                        )
                    } else {
                        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        resultLauncher.launch(cameraIntent)
                    }
                }
                R.id.etDate -> {
                    openDatePicker()
                }
                R.id.etTime -> {
                    openTimePicker()
                }
            }
        }
    }

    private fun openTimePicker() {
        val calendar = java.util.Calendar.getInstance()
        val hourOfDay = calendar.get(java.util.Calendar.HOUR_OF_DAY)
        val minute = calendar.get(java.util.Calendar.MINUTE)
        val timePickerDialog = TimePickerDialog(this, this, hourOfDay, minute, true)
        timePickerDialog.show()
    }

    private fun openDatePicker() {
        val myCalendar = java.util.Calendar.getInstance()
        val year = myCalendar.get(java.util.Calendar.YEAR)
        val month = myCalendar.get(java.util.Calendar.MONTH)
        val day = myCalendar.get(java.util.Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(this, this, year, month, day)
        datePickerDialog.show()
    }

    @SuppressLint("SetTextI18n")
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
        binding.etDate.setText("$day/$month/$year")
    }

    @SuppressLint("SetTextI18n")
    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        binding.etTime.setText("$hourOfDay:$minute")
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                val photo = data?.extras!!["data"] as Bitmap?
                binding.ivAddVideoImage.visibility = View.VISIBLE
                binding.ivAddVideoImagePlus.visibility = View.GONE
                binding.ivAddVideoImage.setImageBitmap(photo)
            }
        }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                resultLauncher.launch(cameraIntent)
            } else {
                showToast("camera permission required")
            }
        }
    }

    private fun initView() {
        binding.tbAddMedication.tvTitle.text = getString(R.string.add_medication)
    }

}