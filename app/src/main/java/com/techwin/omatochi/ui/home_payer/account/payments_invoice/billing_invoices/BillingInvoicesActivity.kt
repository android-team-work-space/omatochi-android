package com.techwin.omatochi.ui.home_payer.account.payments_invoice.billing_invoices

import android.view.View
import androidx.activity.viewModels
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.InboxMessagesModel
import com.techwin.omatochi.databinding.ActivityBillingInvoicesBinding
import com.techwin.omatochi.databinding.CustomInvoicesItemBinding
import com.techwin.omatochi.databinding.CustomPreviousInvoiceItemBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.utils.AppUtils

class BillingInvoicesActivity : BaseActivity<ActivityBillingInvoicesBinding>() {

    private val viewModel: BillingInvoicesActivityVM by viewModels()
    private lateinit var currentInvoiceAdapter: RVAdapter<InboxMessagesModel, CustomInvoicesItemBinding> //model is not created yet
    private lateinit var previousInvoiceAdapter: RVAdapter<InboxMessagesModel, CustomPreviousInvoiceItemBinding>

    override fun getLayoutResource(): Int {
        return R.layout.activity_billing_invoices
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnclick()
    }

    private fun initView() {
        binding.tbBillingInvoices.tvTitle.text = getString(R.string.billing_invoices)
        initCurrentInvoiceAdapter()
        initPreviousInvoiceAdapter()
    }

    private fun initCurrentInvoiceAdapter() {
        currentInvoiceAdapter = RVAdapter(
            R.layout.custom_invoices_item,
            BR.bean,
            object : RVAdapter.Callback<InboxMessagesModel> {
                override fun onItemClick(v: View?, m: InboxMessagesModel) {

                }
            })
        binding.rvCurrentInvoices.adapter = currentInvoiceAdapter
        currentInvoiceAdapter.list = AppUtils.getInboxMessagesList()

    }

    private fun initPreviousInvoiceAdapter() {
        previousInvoiceAdapter = RVAdapter(
            R.layout.custom_previous_invoice_item,
            BR.bean,
            object : RVAdapter.Callback<InboxMessagesModel> {
                override fun onItemClick(v: View?, m: InboxMessagesModel) {

                }
            })
        binding.rvPreviousInvoices.adapter = previousInvoiceAdapter
        previousInvoiceAdapter.list = AppUtils.getInboxMessagesList()

    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
            }
        }
    }
}