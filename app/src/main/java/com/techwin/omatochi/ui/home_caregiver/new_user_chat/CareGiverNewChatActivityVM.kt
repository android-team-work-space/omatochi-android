package com.techwin.omatochi.ui.home_caregiver.new_user_chat

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CareGiverNewChatActivityVM @Inject constructor() : BaseViewModel() {

}