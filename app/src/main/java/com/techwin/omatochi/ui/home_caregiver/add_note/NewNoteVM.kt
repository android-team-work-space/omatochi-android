package com.techwin.omatochi.ui.home_caregiver.add_note

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NewNoteVM @Inject constructor() : BaseViewModel()