package com.techwin.omatochi.ui.home_caregiver.activity.diet

import android.annotation.SuppressLint
import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.provider.MediaStore
import android.view.View
import android.widget.TimePicker
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.MealTypeModel
import com.techwin.omatochi.databinding.ActivityAddDietBinding
import com.techwin.omatochi.databinding.SelectMealItemBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddDietActivity : BaseActivity<ActivityAddDietBinding>(), TimePickerDialog.OnTimeSetListener {
    private val viewModel: AddDietActivityVM by viewModels()
    private val MY_CAMERA_PERMISSION_CODE = 100
    private lateinit var rvMealType: RVAdapter<MealTypeModel, SelectMealItemBinding>

    override fun getLayoutResource(): Int {
        return R.layout.activity_add_diet
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView() {
        initView()
        initOnClick()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.llAddImage -> {
                    if (checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                            arrayOf(android.Manifest.permission.CAMERA),
                            MY_CAMERA_PERMISSION_CODE
                        )
                    } else {
                        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        resultLauncher.launch(cameraIntent)
                    }
                }
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnAddDiet -> {
                    when {
                        binding.etMealType.text.toString() == "" -> {
                            showToast("Please enter meal type.")
                        }
                        binding.etTime.text.toString() == "" -> {
                            showToast("please select a time.")
                        }
                        binding.etNote.text.toString() == "" -> {
                            showToast("please enter note.")
                        }
                        else -> {
                            showToast("Diet Will be added")
                        }
                    }
                }

                R.id.etTime->{
                    openTimePicker()
                }
            }
        }
    }

    private fun openTimePicker() {
        val calendar = java.util.Calendar.getInstance()
        val hourOfDay = calendar.get(java.util.Calendar.HOUR_OF_DAY)
        val minute = calendar.get(java.util.Calendar.MINUTE)
        val timePickerDialog = TimePickerDialog(this, this, hourOfDay, minute, true)
        timePickerDialog.show()
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                val photo = data?.extras!!["data"] as Bitmap?
                binding.ivAddDiet.visibility = View.VISIBLE
                binding.ivAddDietPlus.visibility = View.GONE
                binding.ivAddDiet.setImageBitmap(photo)
            }
        }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                resultLauncher.launch(cameraIntent)
            } else {
                showToast("camera permission required")
            }
        }
    }

    private fun initView() {
        binding.tbAddDiet.tvTitle.text = getString(R.string.add_diet)
        this.window.statusBarColor = ContextCompat.getColor(this,R.color.white)
        initAdapter()
    }

    private fun initAdapter() {
        rvMealType = RVAdapter(
            R.layout.select_meal_item,
            com.techwin.omatochi.BR.bean,
            object : RVAdapter.Callback<MealTypeModel> {
                override fun onItemClick(v: View?, m: MealTypeModel) {
                }
            })
        binding.rvMealType.adapter = rvMealType
        rvMealType.list = AppUtils.getMealType()
    }

    @SuppressLint("SetTextI18n")
    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        binding.etTime.setText("$hourOfDay:$minute")
    }
}