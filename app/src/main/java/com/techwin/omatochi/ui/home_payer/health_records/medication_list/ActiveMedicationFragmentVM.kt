package com.techwin.omatochi.ui.home_payer.health_records.medication_list

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ActiveMedicationFragmentVM @Inject constructor() : BaseViewModel() {

}