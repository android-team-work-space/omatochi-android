package com.techwin.omatochi.ui.home_caregiver.fragment.messages

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MessagesFragmentVM  @Inject constructor(): BaseViewModel(){
}