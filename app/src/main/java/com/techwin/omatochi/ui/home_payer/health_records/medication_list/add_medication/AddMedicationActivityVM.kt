package com.techwin.omatochi.ui.home_payer.health_records.medication_list.add_medication

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AddMedicationActivityVM @Inject constructor(): BaseViewModel() {
}