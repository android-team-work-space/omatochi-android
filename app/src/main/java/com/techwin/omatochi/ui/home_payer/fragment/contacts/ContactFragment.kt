package com.techwin.omatochi.ui.home_payer.fragment.contacts

import android.content.Intent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.BsFamilyContactBinding
import com.techwin.omatochi.databinding.FragmentContactBinding
import com.techwin.omatochi.databinding.HolderContactBinding
import com.techwin.omatochi.databinding.HolderContactsBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_payer.HomePayerActivity
import com.techwin.omatochi.ui.home_payer.account.profile.ProfileActivity
import com.techwin.omatochi.ui.home_payer.addContact.AddContactActivity
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.sheet.BaseCustomBottomSheet
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ContactFragment : BaseFragment<FragmentContactBinding>() {
    private val viewModel: ContactFragmentVM by viewModels()
    private lateinit var homePayerActivity: HomePayerActivity
    private lateinit var adapterContact: RVAdapter<ContactBean, HolderContactsBinding>
    private lateinit var favRVAdapter: RVAdapter<ContactBean, HolderContactBinding>
    private lateinit var bsFavRVAdapter: RVAdapter<ContactBean, HolderContactBinding>
    private lateinit var homeContactBottomSheet: BaseCustomBottomSheet<BsFamilyContactBinding>
    private lateinit var careProviderContactBottomSheet: BaseCustomBottomSheet<BsFamilyContactBinding>

    companion object {
        fun newIntent(): Fragment {
            return ContactFragment()
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_contact
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        initView()
        initOnclick()
        initAdapter()
        //initBottomSheet()
    }

    private fun initAdapter() {
        adapterContact =
            RVAdapter(R.layout.holder_contacts, BR.bean, object : RVAdapter.Callback<ContactBean> {
                override fun onItemClick(v: View?, m: ContactBean) {
                }

                override fun onPositionClick(v: View?, m: ContactBean, pos: Int) {
                    when (m.title) {
                        "Family" -> {
                            initBottomSheet("Family")
                        }
                        "Care Provider" -> {
                            initBottomSheet("Care Provider")
                        }
                    }
                }
            })
        binding.rvContacts.adapter = adapterContact
        adapterContact.list = AppUtils.getContactList()
        favRVAdapter =
            RVAdapter(R.layout.holder_contact, BR.bean, object : RVAdapter.Callback<ContactBean> {
                override fun onItemClick(v: View?, m: ContactBean) {
                }

                override fun onPositionClick(v: View?, m: ContactBean, pos: Int) {
                }
            })
        binding.rvFavourites.adapter = favRVAdapter
        favRVAdapter.list = AppUtils.getContactFavList()

        bsFavRVAdapter =
            RVAdapter(R.layout.holder_contact, BR.bean, object : RVAdapter.Callback<ContactBean> {
                override fun onItemClick(v: View?, m: ContactBean) {
                }

                override fun onPositionClick(v: View?, m: ContactBean, pos: Int) {

                }
            })
        bsFavRVAdapter.list = AppUtils.getFavList()
    }


    private fun initOnclick() {
        homePayerActivity = activity as HomePayerActivity
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivAddNewContact -> {
                    startActivity(Intent(context, AddContactActivity::class.java))
                }
                R.id.ivMenu -> {
                    homePayerActivity.openSideMenu()
                }
                R.id.ivProfile -> {
                    homePayerActivity.startNewActivity(Intent(requireContext(), ProfileActivity::class.java))
                }
            }
        }
    }

    private fun initView() {
        homePayerActivity = activity as HomePayerActivity
        //binding.toolBarContact.tvTitle.text = getString(R.string.contacts)
        homePayerActivity.window.statusBarColor =
            ContextCompat.getColor(homePayerActivity, R.color.blue)
    }

    private fun initBottomSheet(title: String) {
        if (!this@ContactFragment::homeContactBottomSheet.isInitialized) {
            homeContactBottomSheet = BaseCustomBottomSheet(
                requireContext(),
                R.layout.bs_family_contact
            ) {

            }
            homeContactBottomSheet.binding.rvFavourites.adapter = bsFavRVAdapter
            val bottomSheet: View = homeContactBottomSheet.binding.codLayout
            val mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            mBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            homeContactBottomSheet.setCanceledOnTouchOutside(true)
            homeContactBottomSheet.binding.tvFavourite.text = title
            homeContactBottomSheet.show()
        } else {
            homeContactBottomSheet.binding.tvFavourite.text = title
            homeContactBottomSheet.show()
        }
    }
}