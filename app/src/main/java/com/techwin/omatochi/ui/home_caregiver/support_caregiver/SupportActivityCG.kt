package com.techwin.omatochi.ui.home_caregiver.support_caregiver

import android.widget.ArrayAdapter
import androidx.activity.viewModels
import com.freshchat.consumer.sdk.Freshchat
import com.freshchat.consumer.sdk.FreshchatConfig
import com.freshchat.consumer.sdk.FreshchatMessage
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivitySupportCgBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.Constants
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SupportActivityCG : BaseActivity<ActivitySupportCgBinding>() {
    private val viewModel: SupportActivityCGVM by viewModels()

    override fun getLayoutResource(): Int {
        return R.layout.activity_support_cg
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
    }

    private fun initView() {
        binding.tbSupport.tvTitle.text = getString(R.string.omatochi_support)
        initSupportOptionsAdapter()
        initFreshDesk()
    }

    private fun initFreshDesk() {
        val config = FreshchatConfig(Constants.FRESHDESK_APPID, Constants.FRESHDESK_APP_KEY)
        config.domain = Constants.FRESHDESK_DOMAIN
        /*config.isCameraCaptureEnabled = true
        config.isGallerySelectionEnabled = true
        config.isResponseExpectationEnabled = true*/
        Freshchat.getInstance(this).init(config)
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnSubmitSupportFeedback -> {
                    if (binding.edtUserProblemText.text.toString() == "") {
                        showToast("Please enter your message")
                    } else {
                        val tag = binding.edtUserProblemText.text.toString()
                        val msgText =
                            (binding.spSupportOptions.selectedItem.toString() + " : " + binding.edtUserProblemText.text.toString())
                        val freshChatMessage = FreshchatMessage().setTag(tag).setMessage(msgText)
                        Freshchat.sendMessage(this, freshChatMessage)
                        Freshchat.showConversations(this)
                    }
                }
                R.id.btnSupportBubbleGoToChat -> {
                    Freshchat.showConversations(this)
                }
            }
        }
    }

    private fun initSupportOptionsAdapter() {
        val adapter = ArrayAdapter(
            this,
            R.layout.custom_spinner,
            R.id.tvSpinner,
            AppUtils.getSupportOptionsList()
        )
        adapter.setDropDownViewResource(R.layout.custom_spinner)
        binding.spSupportOptions.adapter = adapter
    }
}