package com.techwin.omatochi.ui.home_payer

data class MenuItemBean(
    var title: String,
    val expendable : Boolean,
    var expended : Boolean,
    var subTitle: List<String>
)