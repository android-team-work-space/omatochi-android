package com.techwin.omatochi.ui.home_payer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.techwin.omatochi.R
import kotlinx.android.synthetic.main.activity_add_diet.*
import kotlinx.android.synthetic.main.activity_image_show.*
import kotlinx.android.synthetic.main.toolbar_back.view.*

class ImageShowActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_show)

        this.window.statusBarColor =
            ContextCompat.getColor(this, R.color.white)
        tbImageShow.tvTitle.text = "Image"
        tbImageShow.ivBack.setOnClickListener {
            finish()
        }
    }
}