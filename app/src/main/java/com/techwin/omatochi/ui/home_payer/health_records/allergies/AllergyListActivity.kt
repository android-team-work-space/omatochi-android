package com.techwin.omatochi.ui.home_payer.health_records.allergies

import android.content.Intent
import androidx.activity.viewModels
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityAllergyListBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_payer.health_records.allergies.vm.AllergyListActivityVM
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AllergyListActivity : BaseActivity<ActivityAllergyListBinding>() {

    private val viewModel: AllergyListActivityVM by viewModels()

    override fun getLayoutResource(): Int {
        return R.layout.activity_allergy_list
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
        initViewPager()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.ivAddAllergy -> {
                    startNewActivity(Intent(this, AddAllergyActivity::class.java))
                }
                R.id.ivShareAllergyPdf -> {
                    val sendIntent = Intent(Intent.ACTION_SEND)
                    sendIntent.type = "text/plain"
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to test.")
                    startActivity(sendIntent)
                }
            }
        }
    }

    private fun initView() {
        binding.tbAllergyList.tvTitle.text = getString(R.string.allergies_list)
    }

    private fun initViewPager() {
        binding.tlAllergyList.removeAllTabs()
        binding.tlAllergyList.addTab(
            binding.tlAllergyList.newTab().setText(getString(R.string.active_allergies))
        )
        binding.tlAllergyList.addTab(
            binding.tlAllergyList.newTab().setText(getString(R.string.inactive_allergies))
        )
        binding.tlAllergyList.tabGravity = TabLayout.GRAVITY_FILL
        binding.tlAllergyList.isSmoothScrollingEnabled = true
        binding.vpAllergyList.adapter = AllergyListViewPager(this)
        binding.vpAllergyList.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        binding.vpAllergyList.isUserInputEnabled = false

        binding.tlAllergyList.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                binding.tlAllergyList.setScrollPosition(tab.position, 0f, true)
                binding.vpAllergyList.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

    }

}