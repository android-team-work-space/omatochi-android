package com.techwin.omatochi.ui.home_payer.about

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.techwin.omatochi.R
import kotlinx.android.synthetic.main.activity_app_version.*
import kotlinx.android.synthetic.main.holder_side_menu.view.*
import kotlinx.android.synthetic.main.holder_side_menu.view.tvTitle
import kotlinx.android.synthetic.main.toolbar_back.view.*

class AppVersionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_version)

        tbAppVersions.tvTitle.text = getString(R.string.about_omatochi)

        tbAppVersions.ivBack.setOnClickListener {
            finish()
        }
        tvAppVersionOmatochiLink.setOnClickListener {
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(getString(R.string.https_omatochi_com))
                startActivity(i)
        }
    }
}