package com.techwin.omatochi.ui.home_caregiver.activity.medication

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AddMedicationCGActivityVM @Inject constructor() : BaseViewModel()