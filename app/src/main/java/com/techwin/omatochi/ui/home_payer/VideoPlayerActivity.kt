package com.techwin.omatochi.ui.home_payer
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.MediaController
import android.widget.Toast
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.techwin.omatochi.R
import kotlinx.android.synthetic.main.activity_video_player.*

class VideoPlayerActivity : AppCompatActivity() {
    var simpleVideoView: VideoView? = null
    private var mediaControls: MediaController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_player)
        initView()

        simpleVideoView = findViewById<View>(R.id.simpleVideoView) as VideoView
        if (mediaControls == null) {
            mediaControls = MediaController(this)
            mediaControls?.setAnchorView(this.simpleVideoView)
        }

        simpleVideoView?.setMediaController(mediaControls)
        simpleVideoView?.setVideoURI(
            Uri.parse("android.resource://"
                + packageName + "/" + R.raw.push_up_vid))
        simpleVideoView?.requestFocus()
        simpleVideoView?.start()
        simpleVideoView?.setOnCompletionListener {
            finish()
        }
        simpleVideoView?.setOnErrorListener { mp, what, extra ->
            Toast.makeText(
                applicationContext, "An Error Occurred " + "While Playing Video !!!", Toast.LENGTH_SHORT
            ).show()
            false
        }

        ivBackVideoPlayer.setOnClickListener {
            simpleVideoView?.stopPlayback()
            finish()
        }
    }

    private fun initView(){
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
    }
}