package com.techwin.omatochi.ui.home_payer.about.terms_and_conditions

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class TermsAndConditionsActivityVM @Inject constructor() : BaseViewModel()

