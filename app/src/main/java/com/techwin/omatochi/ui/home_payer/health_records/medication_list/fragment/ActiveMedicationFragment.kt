package com.techwin.omatochi.ui.home_payer.health_records.medication_list.fragment

import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.InActiveMedicationModel
import com.techwin.omatochi.databinding.FragmentActiveMedicationBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_payer.health_records.medication_list.ActiveMedicationFragmentVM
import com.techwin.omatochi.ui.home_payer.health_records.medication_list.MedicationListActivity
import com.techwin.omatochi.ui.home_payer.health_records.medication_list.adapters.CustomActiveMedicationAdapter
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.showToast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ActiveMedicationFragment : BaseFragment<FragmentActiveMedicationBinding>() {

    private val viewModel: ActiveMedicationFragmentVM by viewModels()
    private lateinit var medicationListActivity: MedicationListActivity
    private lateinit var adapterActiveMedication: CustomActiveMedicationAdapter

    companion object {
        fun newIntent(): Fragment {
            return ActiveMedicationFragment()
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_active_medication
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        initView()
        initOnclick()
    }


    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {

            }
        }
    }

    private fun initView() {
        medicationListActivity = activity as MedicationListActivity
        medicationListActivity.window.statusBarColor =
            ContextCompat.getColor(medicationListActivity, R.color.white)

        initAdapter()
    }

    private fun initAdapter() {
        adapterActiveMedication =
            CustomActiveMedicationAdapter(object : CustomActiveMedicationAdapter.CardCallback {
                override fun onItemClick(v: View?, m: InActiveMedicationModel?, pos: Int?) {
                    when (v?.id) {
                        R.id.layEdit -> {

                        }
                        R.id.layInActive -> {
                            showToast("Added to InActive Medication")
                            adapterActiveMedication.swipeLayout?.close()
                        }
                        R.id.clActiveMed,R.id.ivDown -> {
                            m?.expended = !m!!.expended
                            adapterActiveMedication.notifyItemChanged(pos!!)
                        }
                    }
                }
            })
        adapterActiveMedication.setList(AppUtils.getInActiveMedList())
        binding.rvActiveMedication.adapter = adapterActiveMedication
    }

}