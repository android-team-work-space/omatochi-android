package com.techwin.omatochi.ui.home_payer.about.privacy_policy

import android.os.Looper
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityPrivacyPolicyBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_payer.support.PrivacyPolciyActivityVM
import com.techwin.omatochi.utils.Constants
import com.techwin.omatochi.utils.RetrivePDFfromUrl
import dagger.hilt.android.AndroidEntryPoint
import io.ak1.pix.helpers.hide
import io.ak1.pix.helpers.show
import java.util.logging.Handler

@AndroidEntryPoint
class PrivacyPolicyActivity : BaseActivity<ActivityPrivacyPolicyBinding>() {

    private val viewModel: PrivacyPolciyActivityVM by viewModels()

    override fun getLayoutResource(): Int {
        return R.layout.activity_privacy_policy
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
            }
        }
    }

    private fun initView() {
        binding.tbPrivacyPolicy.tvTitle.text = getString(R.string.privacy_policy)
        binding.pbPrivacyPolicy.show()
        loadTermsAndConditionsUrl()
    }

    private fun loadTermsAndConditionsUrl() {
        android.os.Handler(Looper.myLooper()!!).postDelayed({
            binding.pbPrivacyPolicy.hide()
        },1500)
        val retrivePDFfromUrl = RetrivePDFfromUrl(binding.idPDFView)
        retrivePDFfromUrl.execute(Constants.PRIVACY_POLICY_URL)
    }
}