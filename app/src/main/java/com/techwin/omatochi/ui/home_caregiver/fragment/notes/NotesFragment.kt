package com.techwin.omatochi.ui.home_caregiver.fragment.notes

import android.content.Intent
import android.graphics.Color
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.viewModels
import com.elconfidencial.bubbleshowcase.BubbleShowCaseBuilder
import com.elconfidencial.bubbleshowcase.BubbleShowCaseSequence
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.NotesModel
import com.techwin.omatochi.databinding.CustomNotesItemBinding
import com.techwin.omatochi.databinding.NotesFragmentBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_caregiver.HomeCareGiverActivity
import com.techwin.omatochi.ui.home_caregiver.add_note.NewNote
import com.techwin.omatochi.ui.home_caregiver.profile.CareGiverProfile
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.event.SingleLiveEvent
import com.techwin.omatochi.utils.sheet.BaseCustomBottomSheet
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.custom_notes_item.view.*

@AndroidEntryPoint
class NotesFragment : BaseFragment<NotesFragmentBinding>(), BaseCustomBottomSheet.Listener {
    private val viewModel: NotesFragmentVM by viewModels()
    private lateinit var mActivity: HomeCareGiverActivity
    private lateinit var bsNotesRVAdapter: RVAdapter<NotesModel, CustomNotesItemBinding>
    companion object {
        var helpGuide: SingleLiveEvent<Boolean> = SingleLiveEvent()
    }

    override fun getLayoutResource(): Int {
        return R.layout.notes_fragment
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        initOnclick()
        initAdapter()
        initView()
        helpGuide.value = false
        helpGuide.observe(this) {
            if (it!!) {
                getSequence().show()
            }
        }
    }

    private fun initOnclick() {
        mActivity = activity as HomeCareGiverActivity
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivMenu -> {
                    mActivity.openSideMenu()
                }
                R.id.ivAddNewNote -> {
                    startActivity(
                        Intent(
                            context,
                            NewNote::class.java
                        )
                    )
                }
                R.id.ivProfile -> {
                    startActivity(
                        Intent(
                            context,
                            CareGiverProfile::class.java
                        )
                    )
                }
            }
        }
    }

    private fun initAdapter() {
        bsNotesRVAdapter = RVAdapter(
            R.layout.custom_notes_item,
            BR.bean,
            object : RVAdapter.Callback<NotesModel> {
                override fun onItemClick(v: View?, m: NotesModel) {

                }

            })

        binding.rvNotes.adapter = bsNotesRVAdapter
        bsNotesRVAdapter.list = AppUtils.getNoteList1()
    }

    private fun initView() {
        mActivity.window.statusBarColor =
            ContextCompat.getColor(mActivity, R.color.green)
        binding.toolbar.ivMenu.imageTintList = ContextCompat.getColorStateList(requireContext(),R.color.white)
        binding.tvMonth.text =
            (AppUtils.getMonth(binding.calendarView.month + 1) + " " + binding.calendarView.year.toString())
        binding.calendarView.todayItemBackgroundDrawable =
            resources.getDrawable(R.drawable.white_radius)
        binding.calendarView.selectedItemBackgroundDrawable = 
            resources.getDrawable(R.drawable.brown_radius)
        binding.calendarView.todayItemTextColor = resources.getColor(R.color.blue)

        binding.calendarView.selectedItemTextColor = resources.getColor(R.color.white)

        binding.nsvNotes.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (scrollY <= 0) {  // down scroll
                if (!binding.calendarView.expanded) {
                    binding.calendarView.expand(500)
                }
            }
            if (scrollY >= 0) { // up scroll
                if (binding.calendarView.expanded) {
                    binding.calendarView.collapse(500)
                }
            }
        })

        binding.calendarView.setCalendarListener(object : CollapsibleCalendar.CalendarListener {
            override fun onClickListener() {}

            override fun onDataUpdate() {}

            override fun onDayChanged() {}

            override fun onDaySelect() {}

            override fun onItemClick(v: View) {}

            override fun onMonthChange() {
                binding.tvMonth.text =
                    (AppUtils.getMonth(binding.calendarView.month + 1) + " " + binding.calendarView.year.toString())
            }

            override fun onWeekChange(position: Int) {}
        })
    }


    override fun onViewClick(view: View?) {
    }


    private fun getDialog1(): BubbleShowCaseBuilder {
        return BubbleShowCaseBuilder(mActivity)
            .description("Tap for more menu options")
            .targetView(binding.toolbar.ivMenu)
            .backgroundColor(Color.WHITE)
            .textColor(Color.BLACK)
            .descriptionTextSize(24)
    }

    private fun getDialog2(): BubbleShowCaseBuilder {
        return BubbleShowCaseBuilder(mActivity)
            .description("Slide down for full calendar access")
            .targetView(binding.view)
            .backgroundColor(Color.WHITE)
            .textColor(Color.BLACK)
            .descriptionTextSize(22)
    }

    private fun getDialog3(): BubbleShowCaseBuilder {
        return BubbleShowCaseBuilder(mActivity)
            .description("Scroll Vertically to browse through notes")
            .targetView(binding.rvNotes[0])
            .backgroundColor(Color.WHITE)
            .textColor(Color.BLACK)
            .descriptionTextSize(22)
    }

   private fun getDialog4(): BubbleShowCaseBuilder {
        return BubbleShowCaseBuilder(mActivity)
            .description("Tap to edit a particular note")
            .targetView(binding.rvNotes.ivEdit)
            .backgroundColor(Color.WHITE)
            .textColor(Color.BLACK)
            .descriptionTextSize(24)
    }

    private fun getDialog5(): BubbleShowCaseBuilder {
        return BubbleShowCaseBuilder(mActivity)
            .description("You can add new notes by\n'Add Button'")
            .targetView(binding.ivAddNewNote)
            .backgroundColor(Color.WHITE)
            .textColor(Color.BLACK)
            .descriptionTextSize(22)
    }

    private fun getSequence(): BubbleShowCaseSequence {
        return BubbleShowCaseSequence().addShowCases(
            listOf(
                getDialog1(),
                getDialog2(),
                getDialog3(),
                getDialog4(),
                getDialog5()
            )
        )
    }
}