package com.techwin.omatochi.ui.home_payer.fragment.dashboard.dashboard2

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class Dashboard2FragmentVM @Inject constructor() : BaseViewModel()

