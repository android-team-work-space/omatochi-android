package com.techwin.omatochi.ui.home_caregiver.activity.diet

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class AddDietActivityVM  @Inject constructor(): BaseViewModel()