package com.techwin.omatochi.ui.home_payer.fragment.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.techwin.omatochi.R

class DashboardViewPager2(val list: ArrayList<ViewPagerBean>) : RecyclerView.Adapter<DashboardViewPager2.ViewPagerViewHolder>() {

    inner class ViewPagerViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewPagerViewHolder {
        return ViewPagerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.holder_dashboard_view_pager, parent, false))
    }

    override fun onBindViewHolder(holder: ViewPagerViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return list.size
    }
}