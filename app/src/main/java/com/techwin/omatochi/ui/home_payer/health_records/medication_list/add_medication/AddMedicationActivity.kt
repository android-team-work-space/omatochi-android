package com.techwin.omatochi.ui.home_payer.health_records.medication_list.add_medication

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityAddMedicationBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.warkiz.widget.IndicatorSeekBar
import com.warkiz.widget.OnSeekChangeListener
import com.warkiz.widget.SeekParams


class AddMedicationActivity : BaseActivity<ActivityAddMedicationBinding>() {
    private val viewModel: AddMedicationActivityVM by viewModels()

    override fun getLayoutResource(): Int {
        return R.layout.activity_add_medication
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        binding.tbAddMedication.tvTitle.text = getString(R.string.add_medication)
        initOnClick()

        binding.sbFrequency.onSeekChangeListener = object : OnSeekChangeListener {
            @SuppressLint("SetTextI18n")
            override fun onSeeking(seekParams: SeekParams) {
                binding.tvMedicineFrequencyCountValue.text =
                    (seekParams.progress.toString() + " times/day")
            }

            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar) {}
            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar) {}
        }
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnSaveMedicine -> {
                    showToast("Will be saved")
                }
                R.id.tvML -> {
                    binding.tvML.setBackgroundResource(R.drawable.bg_blue__radious)
                    binding.tvML.setTextColor(ContextCompat.getColor(this,R.color.white))
                    binding.tvUG.setTextColor(ContextCompat.getColor(this,R.color.black))
                    binding.tvMG.setTextColor(ContextCompat.getColor(this,R.color.black))
                    binding.tvMG.setBackgroundResource(R.drawable.bg_grey__radious)
                    binding.tvUG.setBackgroundResource(R.drawable.bg_grey__radious)
                }
                R.id.tvMG -> {
                    binding.tvMG.setBackgroundResource(R.drawable.bg_blue__radious)
                    binding.tvMG.setTextColor(ContextCompat.getColor(this,R.color.white))
                    binding.tvUG.setTextColor(ContextCompat.getColor(this,R.color.black))
                    binding.tvML.setTextColor(ContextCompat.getColor(this,R.color.black))
                    binding.tvUG.setBackgroundResource(R.drawable.bg_grey__radious)
                    binding.tvML.setBackgroundResource(R.drawable.bg_grey__radious)
                }
                R.id.tvUG -> {
                    binding.tvUG.setBackgroundResource(R.drawable.bg_blue__radious)
                    binding.tvUG.setTextColor(ContextCompat.getColor(this,R.color.white))
                    binding.tvMG.setTextColor(ContextCompat.getColor(this,R.color.black))
                    binding.tvML.setTextColor(ContextCompat.getColor(this,R.color.black))
                    binding.tvMG.setBackgroundResource(R.drawable.bg_grey__radious)
                    binding.tvML.setBackgroundResource(R.drawable.bg_grey__radious)
                }
                R.id.tvMedicineNameMark -> {
                    openBrowser()
                }
                R.id.tvMedicineFrequencyMark -> {
                    openBrowser()
                }
                R.id.tvMedicineStrengthMark -> {
                    openBrowser()
                }
                R.id.tvMedicineDosageMark -> {
                    openBrowser()
                }
            }
        }
    }

    private fun openBrowser(){
        val url = "https://www.google.com/"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }
}