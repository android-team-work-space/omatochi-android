package com.techwin.omatochi.ui.home_payer.health_records.exercise

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.provider.MediaStore
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityAddExerciseBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel

class AddExerciseActivity : BaseActivity<ActivityAddExerciseBinding>() {

    private val viewModel: AddExerciseActivityVM by viewModels()
    private val MY_CAMERA_PERMISSION_CODE = 100

    override fun getLayoutResource(): Int {
        return R.layout.activity_add_exercise
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView() {
        initView()
        initOnClick()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.llAddVideoOrImage -> {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                            arrayOf(Manifest.permission.CAMERA),
                            MY_CAMERA_PERMISSION_CODE
                        )
                    } else {
                        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        resultLauncher.launch(cameraIntent)
                    }
                }
                R.id.btnAddExercise -> {
                    showToast("Will be added")
                }

            }
        }
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                val photo = data?.extras!!["data"] as Bitmap?
                binding.ivAddExercise.visibility = View.VISIBLE
                binding.ivAddExercisePlus.visibility = View.GONE
                binding.ivAddExercise.setImageBitmap(photo)
            }
        }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                resultLauncher.launch(cameraIntent)
            } else {
                showToast("camera permission required")
            }
        }
    }

    private fun initView() {
        binding.tbAddExercise.tvTitle.text = getString(R.string.add_exercise)
    }
}