package com.techwin.omatochi.ui.home_payer.account.payments_invoice

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.ItemAnimator
import androidx.recyclerview.widget.SimpleItemAnimator
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityPaymentInvoiceBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_payer.account.payments_invoice.addCard.AddCardActivity
import com.techwin.omatochi.ui.home_payer.account.payments_invoice.billing_invoices.BillingInvoicesActivity
import com.techwin.omatochi.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class PaymentInvoiceActivity : BaseActivity<ActivityPaymentInvoiceBinding>() {

    private val viewModel: PaymentInvoiceActivityVM by viewModels()
    private lateinit var adapterCreditCard: CustomPaymentInVoiceAdapter
    var dataList = ArrayList<CreditCardBean>()

    companion object {
        fun newIntent(activity: Activity): Intent {
            val intent = Intent(activity, PaymentInvoiceActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_payment_invoice
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        dataList = AppUtils.getCreditCardList()
        initView()
        initAdapter()
        initOnclick()
    }

    private fun initAdapter() {
        adapterCreditCard =
            CustomPaymentInVoiceAdapter(object : CustomPaymentInVoiceAdapter.CardCallback {
                override fun onItemClick(v: View?, m: CreditCardBean?, pos: Int?) {
                    when (v?.id) {
                        R.id.layEdit -> {
                            startNewActivity(
                                Intent(
                                    this@PaymentInvoiceActivity,
                                    AddCardActivity::class.java
                                )
                            )
                        }
                        R.id.layDelete -> {
                            showToast("Deleted soon...")
                            adapterCreditCard.swipeLayout?.close()
                        }
                        R.id.clPaymentInVoice -> {
                            updateList(pos)
                        }
                    }
                }
            })
        binding.rvAllCards.layoutManager = LinearLayoutManager(this)
        adapterCreditCard.setHasStableIds(true)
        binding.rvAllCards.adapter = adapterCreditCard
        adapterCreditCard.setList(dataList)
    }


    @SuppressLint("NotifyDataSetChanged")
    private fun updateList(pos: Int?) {
        pos?.let { position ->
            for (i in dataList.withIndex()) {
                dataList[i.index].isSelected = i.index == position
            }
            adapterCreditCard.setList(dataList)
            showToast(getString(R.string.default_card_changed))
        }
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.ivAddNew -> {
                    val intent = AddCardActivity.newIntent(this)
                    startActivity(intent)
                }
                R.id.ivBillingInvoices -> {
                    startNewActivity(Intent(this, BillingInvoicesActivity::class.java))
                }
            }
        }
    }

    private fun initView() {
        binding.toolBar.tvTitle.text = getString(R.string.all_cards)
    }

}