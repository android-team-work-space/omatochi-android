package com.techwin.omatochi.ui.home_caregiver

data class MenuItemBean2(
    var title: String,
    val expendable : Boolean,
    var expended : Boolean,
    var subTitle: List<String>
)