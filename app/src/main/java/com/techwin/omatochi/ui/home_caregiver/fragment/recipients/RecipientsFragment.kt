package com.techwin.omatochi.ui.home_caregiver.fragment.recipients

import android.content.Intent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.CurrentModel
import com.techwin.omatochi.data.model.PastModel
import com.techwin.omatochi.databinding.CurrentItemBinding
import com.techwin.omatochi.databinding.FragmentRecipientBinding
import com.techwin.omatochi.databinding.PastItemBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_caregiver.HomeCareGiverActivity
import com.techwin.omatochi.ui.home_caregiver.profile.CareGiverProfile
import com.techwin.omatochi.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RecipientsFragment : BaseFragment<FragmentRecipientBinding>() {
    private val viewModel: RecipientsFragmentVM by viewModels()
    private lateinit var mActivity: HomeCareGiverActivity
    private lateinit var currentAdapter: RVAdapter<CurrentModel, CurrentItemBinding>
    private lateinit var pastAdapter: RVAdapter<PastModel, PastItemBinding>

    override fun getLayoutResource(): Int {
        return R.layout.fragment_recipient
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        mActivity = activity as HomeCareGiverActivity
        initView()
        initOnClick()
        initCurrentAdapter()
        initPastAdapter()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(viewLifecycleOwner) {
            when (it!!.id) {
                R.id.ivMenu -> {
                    mActivity.openSideMenu()
                }
                R.id.ivProfile -> {
                    startActivity(
                        Intent(
                            context,
                            CareGiverProfile::class.java
                        )
                    )
                }
            }
        }
    }

    private fun initView() {
        mActivity.window.statusBarColor =
            ContextCompat.getColor(mActivity, R.color.white)
    }

    private fun initCurrentAdapter() {
        currentAdapter =
            RVAdapter(R.layout.current_item, BR.bean, object : RVAdapter.Callback<CurrentModel> {
                override fun onItemClick(v: View?, m: CurrentModel) {

                }
            })
        binding.rvCurrent.adapter = currentAdapter
        currentAdapter.list = AppUtils.getCurrentList()
    }

    private fun initPastAdapter() {
        pastAdapter =
            RVAdapter(R.layout.past_item, BR.bean, object : RVAdapter.Callback<PastModel> {
                override fun onItemClick(v: View?, m: PastModel) {

                }
            })
        binding.rvPast.adapter = pastAdapter
        pastAdapter.list = AppUtils.getPastList()
    }
}