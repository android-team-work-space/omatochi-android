package com.techwin.omatochi.ui.home_payer.fragment.dashboard

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DashboardFragmentVM @Inject constructor() : BaseViewModel() {

}