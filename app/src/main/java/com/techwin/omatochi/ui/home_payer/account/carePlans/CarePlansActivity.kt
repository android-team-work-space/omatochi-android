package com.techwin.omatochi.ui.home_payer.account.carePlans

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.activity.viewModels
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityCarePlansBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.AndroidEntryPoint

import com.techwin.omatochi.databinding.HolderCarePlaneBinding
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_payer.account.carePlans.buddy.BuddyCarePlansActivity
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.Constants

@AndroidEntryPoint
class CarePlansActivity : BaseActivity<ActivityCarePlansBinding>() {
    private val viewModel: CarePlansActivityVM by viewModels()
    private lateinit var adapterCarePlan: RVAdapter<CarePlanBean, HolderCarePlaneBinding>

    companion object {
        fun newIntent(activity: Activity): Intent {
            val intent = Intent(activity, CarePlansActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_care_plans
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initAdapter()
        initOnclick()
    }

    private fun initAdapter() {
        adapterCarePlan = RVAdapter(
            R.layout.holder_care_plane,
            BR.bean,
            object : RVAdapter.Callback<CarePlanBean> {
                override fun onItemClick(v: View?, m: CarePlanBean) {

                }

                override fun onPositionClick(v: View?, m: CarePlanBean, pos: Int) {
                    when (pos) {
                        1, 3 -> {
                            val intent = BuddyCarePlansActivity.newIntent(this@CarePlansActivity)
                            intent.putExtra(Constants.MODEL_CLASS_PASS, m)
                            startActivity(intent)
                        }
                    }
                }
            })
        binding.rvCarePlane.adapter = adapterCarePlan
        adapterCarePlan.list = AppUtils.getCarePlanList()
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
            }
        }
    }

    private fun initView() {
        binding.toolBar.tvTitle.text = getString(R.string.care_plans)
    }

}