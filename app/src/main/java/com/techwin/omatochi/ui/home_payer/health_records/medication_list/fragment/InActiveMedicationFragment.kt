package com.techwin.omatochi.ui.home_payer.health_records.medication_list.fragment

import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.InActiveMedicationModel
import com.techwin.omatochi.databinding.FragmentInActiveMedicationBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_payer.health_records.medication_list.InActiveMedicationFragmentVM
import com.techwin.omatochi.ui.home_payer.health_records.medication_list.MedicationListActivity
import com.techwin.omatochi.ui.home_payer.health_records.medication_list.adapters.CustomInActiveMedicationAdapter
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.showToast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InActiveMedicationFragment : BaseFragment<FragmentInActiveMedicationBinding>() {

    private val viewModel: InActiveMedicationFragmentVM by viewModels()
    private lateinit var medicationListActivity: MedicationListActivity
    private lateinit var adapterInActiveMedication: CustomInActiveMedicationAdapter

    companion object {
        fun newIntent(): Fragment {
            return InActiveMedicationFragment()
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_in_active_medication
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        initView()
        initOnclick()
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {

            }
        }
    }

    private fun initView() {
        medicationListActivity = activity as MedicationListActivity
        medicationListActivity.window.statusBarColor =
            ContextCompat.getColor(medicationListActivity, R.color.white)

        initAdapter()
    }

    private fun initAdapter() {
        adapterInActiveMedication =
            CustomInActiveMedicationAdapter(object : CustomInActiveMedicationAdapter.CardCallback {
                override fun onItemClick(v: View?, m: InActiveMedicationModel?, pos: Int?) {
                    when (v?.id) {
                        R.id.layEdit -> {

                        }
                        R.id.layActive -> {
                            showToast("Added to Active Medication")
                            adapterInActiveMedication.swipeLayout?.close()
                        }
                        R.id.ivDownInActiveMed,R.id.clInActiveMed -> {
                            m?.expended = !m!!.expended
                            adapterInActiveMedication.notifyItemChanged(pos!!)
                        }
                    }
                }
            })
        adapterInActiveMedication.setList(AppUtils.getInActiveMedList())
        binding.rvInActiveMedication.adapter = adapterInActiveMedication
    }
}