package com.techwin.omatochi.ui.base.binding

import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.techwin.omatochi.R

object ImageViewBinding {

    @JvmStatic
    @BindingAdapter("setImageInt")
    fun setImageInt(imageView: ImageView, image: Int) {
        Glide.with(imageView.context).load(image).into(imageView)
       // imageView.setBackgroundResource(image)
    }

    @JvmStatic
    @BindingAdapter("setDrawable")
    fun setDrawable(imageView: ConstraintLayout, selected: Boolean) {
        if (selected) {
            imageView.setBackgroundResource(R.drawable.round_stroke_with_filled_solid_light_blue)
        } else {
            imageView.setBackgroundResource(R.drawable.blue_round_stroke)
        }
    }
}