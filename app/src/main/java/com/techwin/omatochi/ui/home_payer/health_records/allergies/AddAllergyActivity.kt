package com.techwin.omatochi.ui.home_payer.health_records.allergies

import android.widget.ArrayAdapter
import androidx.activity.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityAddAllergyBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_payer.health_records.allergies.vm.AddAllergyActivityVM
import com.techwin.omatochi.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddAllergyActivity : BaseActivity<ActivityAddAllergyBinding>() {

    private val viewModel: AddAllergyActivityVM by viewModels()

    override fun getLayoutResource(): Int {
        return R.layout.activity_add_allergy
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
        initNewNoteAdapter()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnAddNewAllergy -> {
                    showToast("Will be saved")
                }
            }
        }
    }

    private fun initView() {
        binding.tbAddAllergy.tvTitle.text = getString(R.string.add_allergy)
    }

    private fun initNewNoteAdapter() {
        val adapter = ArrayAdapter(
            this,
            R.layout.custom_spinner,
            R.id.tvSpinner,
            AppUtils.getAddNewAllergyTypeList()
        )
        adapter.setDropDownViewResource(R.layout.custom_spinner)
        binding.spAddNewAllergy.adapter = adapter
    }
}