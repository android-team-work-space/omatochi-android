package com.techwin.omatochi.ui.home_payer

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.activity.viewModels
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityHomeBinding
import com.techwin.omatochi.databinding.HolderSideMenuBinding
import com.techwin.omatochi.databinding.RateUsDialogPopupBinding
import com.techwin.omatochi.ui.auth.login.LoginActivity
import com.techwin.omatochi.ui.auth.verification.VerificationActivity
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_payer.about.AppVersionActivity
import com.techwin.omatochi.ui.home_payer.account.carePlans.CarePlansActivity
import com.techwin.omatochi.ui.home_payer.support.faqs.FAQActivity
import com.techwin.omatochi.ui.home_payer.health_records.allergies.AllergyListActivity
import com.techwin.omatochi.ui.home_payer.health_records.exercise.ExerciseListActivity
import com.techwin.omatochi.ui.home_payer.health_records.medication_list.MedicationListActivity
import com.techwin.omatochi.ui.home_payer.account.payments_invoice.PaymentInvoiceActivity
import com.techwin.omatochi.ui.home_payer.about.privacy_policy.PrivacyPolicyActivity
import com.techwin.omatochi.ui.home_payer.account.profile.ProfileActivity
import com.techwin.omatochi.ui.home_payer.support.SupportActivity
import com.techwin.omatochi.ui.home_payer.about.terms_and_conditions.TermsAndConditionsActivity
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.dialog.BaseCustomDialog
import com.techwin.omatochi.utils.event.SingleRequestEvent
import com.techwin.omatochi.utils.showInfoToast
import com.techwin.omatochi.utils.showSuccessToast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomePayerActivity : BaseActivity<ActivityHomeBinding>(), BaseCustomDialog.Listener {
    private val viewModel: HomePayerActivityVM by viewModels()
    private lateinit var adapterMenu: RVAdapter<MenuItemBean, HolderSideMenuBinding>
    private lateinit var navController: NavController
    private var exit: Boolean = false
    private lateinit var popup: BaseCustomDialog<RateUsDialogPopupBinding>

    companion object {
        fun newIntent(activity: Activity): Intent {
            val intent = Intent(activity, HomePayerActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }

        var menuSubItemClick: SingleRequestEvent<String> = SingleRequestEvent()
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_home
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initAdapter()
        initOnclick()
        initBottomNavigation()
    }

    private fun initAdapter() {
        adapterMenu = RVAdapter(
            R.layout.holder_side_menu,
            BR.bean,
            object : RVAdapter.Callback<MenuItemBean> {
                override fun onItemClick(v: View?, m: MenuItemBean) {
                }

                override fun onPositionClick(v: View?, m: MenuItemBean, pos: Int) {
                    when (pos) {
                        0 -> {  // account
                            m.expended = !m.expended
                            adapterMenu.notifyItemChanged(pos)
                        }
                        1 -> { // health records
                            m.expended = !m.expended
                            adapterMenu.notifyItemChanged(pos)
                        }
                        2 -> {  // Support
                            m.expended = !m.expended
                            adapterMenu.notifyItemChanged(pos)
                        }
                        3 -> { // about
                            m.expended = !m.expended
                            adapterMenu.notifyItemChanged(pos)
                        }
                    }
                }
            })
        binding.rvMenuItems.adapter = adapterMenu
        adapterMenu.list = AppUtils.getSideMenuList()

        menuSubItemClick.observe(this, Observer {
            when (it.data) {
                "Profile" -> {
                    openSideMenu()
                    val intent = ProfileActivity.newIntent(this)
                    startActivity(intent)
                }
                "Billing & Payments" -> {
                    openSideMenu()
                    val intent = PaymentInvoiceActivity.newIntent(this)
                    startActivity(intent)
                }
                "Care Plans" -> {
                    openSideMenu()
                    val intent = CarePlansActivity.newIntent(this)
                    startActivity(intent)
                }
                "Medication" -> {
                    openSideMenu()
                    startNewActivity(Intent(this, MedicationListActivity::class.java))
                }
                "Allergy" -> {
                    openSideMenu()
                    startNewActivity(Intent(this, AllergyListActivity::class.java))
                }
                "Exercise" -> {
                    openSideMenu()
                    startNewActivity(Intent(this, ExerciseListActivity::class.java))
                }
                "Omatochi Support" -> {
                    openSideMenu()
                    startNewActivity(Intent(this, SupportActivity::class.java))
                }
                "User Guide (FAQs)" -> {
                    openSideMenu()
                    startNewActivity(Intent(this, FAQActivity::class.java))
                }
                "App Version" -> {
                    openSideMenu()
                    startNewActivity(Intent(this, AppVersionActivity::class.java))
                }
                "Terms & Conditions" -> {
                    openSideMenu()
                    startNewActivity(Intent(this, TermsAndConditionsActivity::class.java))
                }
                "Privacy Policy" -> {
                    openSideMenu()
                    startNewActivity(Intent(this, PrivacyPolicyActivity::class.java))
                }
                "Legal Info" -> {
                    showToast("available soon")
                }
            }
        })
    }


    fun openSideMenu() {
        if (binding.mainDl.isDrawerOpen(GravityCompat.START))
            binding.mainDl.closeDrawer(GravityCompat.START)
        else binding.mainDl.openDrawer(GravityCompat.START)
    }

    private fun initBottomNavigation() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.my_nav_host_fragment) as NavHostFragment?
        navController = navHostFragment!!.navController
        NavigationUI.setupWithNavController(binding.bottomNavigation, navController)
    }

    @SuppressLint("RestrictedApi")
    override fun onBackPressed() {
        if (navController.backStack.size > 2) {
            if (!navController.popBackStack())
                backPress()
        } else
            backPress()
    }

    private fun backPress() {
        if (exit) {
            finishAffinity()
        } else {
            showInfoToast("Press again for exit")
            exit = true
            Handler(Looper.myLooper()!!).postDelayed({ exit = false }, 2000)
        }
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnForgot -> {
                    val intent = VerificationActivity.newIntent(this)
                    startActivity(intent)
                }
                R.id.btnLogout -> {
                    openSideMenu()
                    showSuccessToast("Logout successfully.")
                    startNewActivity(Intent(this, LoginActivity::class.java))
                    finish()
                }
            }
        }
    }

    private fun initView() {
        initDialog()
    }

    private fun initDialog() {
        popup = BaseCustomDialog<RateUsDialogPopupBinding>(
            this,
            R.layout.rate_us_dialog_popup,
            this
        )
        popup.setCanceledOnTouchOutside(false)
    }

    override fun onViewClick(view: View?) {
        when (view?.id) {
            R.id.tvCancelRateUS -> {
                popup.dismiss()
            }
            R.id.tvSubmitRateUS -> {
                popup.dismiss()
            }
        }
    }
}