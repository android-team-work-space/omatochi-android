package com.techwin.omatochi.ui.home_payer.fragment.past_visits

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PastVisitFragmentVM @Inject constructor() : BaseViewModel() {

}