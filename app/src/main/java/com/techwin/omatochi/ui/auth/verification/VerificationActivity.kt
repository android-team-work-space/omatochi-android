package com.techwin.omatochi.ui.auth.verification

import android.app.Activity
import android.content.Intent
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityVerificationBinding
import com.techwin.omatochi.ui.auth.createNewPassword.CreateNewPasswordActivity
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.utils.showSuccessToast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VerificationActivity : BaseActivity<ActivityVerificationBinding>() {

    private val viewModel: VerificationActivityVM by viewModels()

    companion object {
        fun newIntent(activity: Activity): Intent {
            val intent = Intent(activity, VerificationActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_verification
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnclick()
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.otpView -> {
                    val inputMethodManager: InputMethodManager = getSystemService(
                        INPUT_METHOD_SERVICE
                    ) as InputMethodManager
                    inputMethodManager.toggleSoftInputFromWindow(
                        binding.otpView.applicationWindowToken,
                        InputMethodManager.SHOW_FORCED,
                        0
                    )
                    binding.otpView.requestFocus()
                }
                R.id.btnForgot -> {
                    if (binding.otpView.otp.length == 4) {
                        showSuccessToast("Otp verified")
                        val intent = CreateNewPasswordActivity.newIntent(this)
                        startActivity(intent)
                    } else {
                        showToast("Please enter complete otp")
                    }
                }
            }
        }
    }

    private fun initView() {
        binding.toolBar.tvTitle.text = getString(R.string.verification)
    }
}