package com.techwin.omatochi.ui.home_caregiver.fragment.new_visits

import android.content.Intent
import android.content.res.ColorStateList
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.github.gcacace.signaturepad.views.SignaturePad
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.NewVisitModel
import com.techwin.omatochi.databinding.NewVisitItemBinding
import com.techwin.omatochi.databinding.NewVisitsFragmentBinding
import com.techwin.omatochi.databinding.ReviewShiftDialogBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_caregiver.HomeCareGiverActivity
import com.techwin.omatochi.ui.home_caregiver.activity.diet.AddDietActivity
import com.techwin.omatochi.ui.home_caregiver.activity.exercise.AddExerciseCaregiverActivity
import com.techwin.omatochi.ui.home_caregiver.activity.medication.AddMedicationCGActivity
import com.techwin.omatochi.ui.home_caregiver.activity.new_activity.AddNewActivity
import com.techwin.omatochi.ui.home_caregiver.profile.CareGiverProfile
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.dialog.BaseCustomDialog

class NewVisitsFragment : BaseFragment<NewVisitsFragmentBinding>() , BaseCustomDialog.Listener{
    private val viewModel: NewVisitsFragmentVM by viewModels()
    private lateinit var homeCareGiverActivity: HomeCareGiverActivity
    private lateinit var rvNewVisit: RVAdapter<NewVisitModel, NewVisitItemBinding>
    private var popup: BaseCustomDialog<ReviewShiftDialogBinding>? = null

    override fun getLayoutResource(): Int {
        return R.layout.new_visits_fragment
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        initView()
        initAdapter()
        initOnClick()
    }

    private fun initOnClick() {
        homeCareGiverActivity = activity as HomeCareGiverActivity
        viewModel.onClick.observe(this) {
            when (it!!.id) {
                R.id.ivMenu ->{
                    homeCareGiverActivity.openSideMenu()
                }
                R.id.ivProfile -> {
                    startActivity(Intent(requireContext(), CareGiverProfile::class.java))
                }
                R.id.btnClockOut -> {
                    binding.clMileage.visibility = View.VISIBLE
                    binding.btnClockOut.visibility = View.GONE
                    binding.clUser.visibility = View.GONE
                }
                R.id.btnEndShift ->{
                    popup?.show()
                }
                R.id.ivClear -> {
                    binding.signPad.clear()
                }
                R.id.ivArrow->{
                    binding.clMileage.visibility = View.GONE
                    binding.btnClockOut.visibility = View.VISIBLE
                    binding.clUser.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun initDialog() {
        popup = BaseCustomDialog<ReviewShiftDialogBinding>(
            homeCareGiverActivity, R.layout.review_shift_dialog, this)
        popup!!.setCanceledOnTouchOutside(true)
    }

    private fun initView() {
        homeCareGiverActivity = activity as HomeCareGiverActivity
        homeCareGiverActivity.window.statusBarColor =
            ContextCompat.getColor(homeCareGiverActivity, R.color.green)
        binding.toolbarNewVisit.ivMenu.imageTintList = ContextCompat.getColorStateList(requireContext(),R.color.white)
        initDialog()
        initSign()
    }

    private fun initAdapter() {
        rvNewVisit = RVAdapter(
            R.layout.new_visit_item,
            com.techwin.omatochi.BR.bean,
            object : RVAdapter.Callback<NewVisitModel> {
                override fun onItemClick(v: View?, m: NewVisitModel) {
                }

                override fun onPositionClick(v: View?, m: NewVisitModel, pos: Int) {
                    super.onPositionClick(v, m, pos)
                    when (pos) {
                        0 -> {
                            startActivity(Intent(context, AddNewActivity::class.java))
                        }
                        1 -> {
                            startActivity(Intent(context, AddMedicationCGActivity::class.java))
                        }
                        2 -> {
                            startActivity(Intent(context, AddExerciseCaregiverActivity::class.java))
                        }
                        3 -> {
                            startActivity(Intent(context, AddDietActivity::class.java))
                        }
                    }
                }
            })
        binding.rvNewVisit.adapter = rvNewVisit
        rvNewVisit.list = AppUtils.getNewVisitItems()
    }

    private fun initSign() {
        binding.signPad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() {
            }

            override fun onSigned() {
                binding.ivClear.isEnabled = true
            }

            override fun onClear() {
                binding.ivClear.isEnabled = false
            }
        })
    }

    override fun onViewClick(view: View?) {
        when (view?.id){
            R.id.btnSubmit->{
                popup?.cancel()
                findNavController().navigate(R.id.newShiftsFragment)
                homeCareGiverActivity.navigateNewShift()
            }
        }
    }
}