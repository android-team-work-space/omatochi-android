package com.techwin.omatochi.ui.home_payer.account.carePlans.buddy

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.activity.viewModels
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityBuddyCarePlansBinding
import com.techwin.omatochi.databinding.HolderBuddyCarePlanBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.AndroidEntryPoint

import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_payer.account.carePlans.CarePlanBean
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.Constants
import com.techwin.omatochi.utils.showSuccessToast

@AndroidEntryPoint
class BuddyCarePlansActivity : BaseActivity<ActivityBuddyCarePlansBinding>() {

    private val viewModel: BuddyCarePlansActivityVM by viewModels()
    private lateinit var adapterBuddyCarePlan: RVAdapter<BuddyCareBean, HolderBuddyCarePlanBinding>

    companion object {
        fun newIntent(activity: Activity): Intent {
            val intent = Intent(activity, BuddyCarePlansActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_buddy_care_plans
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initAdapter()
        initOnclick()
    }

    private fun initAdapter() {
        adapterBuddyCarePlan = RVAdapter(
            R.layout.holder_buddy_care_plan,
            BR.bean,
            object : RVAdapter.Callback<BuddyCareBean> {
                override fun onItemClick(v: View?, m: BuddyCareBean) {

                }

                override fun onPositionClick(v: View?, m: BuddyCareBean, pos: Int) {

                }
            })
        binding.rvBuddyCare.adapter = adapterBuddyCarePlan
        adapterBuddyCarePlan.list = AppUtils.getBuddyCarePlanList()
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnBookConsultation -> {
                    showSuccessToast("Consultation booked")
                }
            }
        }
    }

    private fun initView() {
        binding.toolBar.tvTitle.text = getString(R.string.buddy_care_plans)
        intent.getSerializableExtra(Constants.MODEL_CLASS_PASS)?.let {
            binding.bean = it as CarePlanBean?
        }
    }

}