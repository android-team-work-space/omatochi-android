package com.techwin.omatochi.ui.home_payer.fragment.message

import android.content.Intent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.InboxMessagesModel
import com.techwin.omatochi.data.model.MessageNotificationsModel
import com.techwin.omatochi.databinding.CustomInboxMessagesItemBinding
import com.techwin.omatochi.databinding.FragmentMessageBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_payer.HomePayerActivity
import com.techwin.omatochi.ui.home_payer.new_user_chat.NewChatActivity
import com.techwin.omatochi.ui.home_payer.account.profile.ProfileActivity
import com.techwin.omatochi.ui.home_payer.search.SearchActivity
import com.techwin.omatochi.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MessageFragment : BaseFragment<FragmentMessageBinding>() {

    private val viewModel: MessageFragmentVM by viewModels()
    private lateinit var homePayerActivity: HomePayerActivity
    private lateinit var notificationAdapter: RVAdapter<MessageNotificationsModel, CustomInboxMessagesItemBinding>
    private lateinit var inboxMessageAdapter: RVAdapter<InboxMessagesModel, CustomInboxMessagesItemBinding>


    companion object {
        fun newIntent(): Fragment {
            return MessageFragment()
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_message
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        initView()
        initOnclick()
        initNotificationAdapter()
        initInboxMessagesAdapter()

    }

    private fun initInboxMessagesAdapter() {
        inboxMessageAdapter = RVAdapter(
            R.layout.custom_inbox_messages_item,
            BR.bean,
            object : RVAdapter.Callback<InboxMessagesModel> {
                override fun onItemClick(v: View?, m: InboxMessagesModel) {
                    startActivity(Intent(context, NewChatActivity::class.java))
                }

                override fun onPositionClick(v: View?, m: InboxMessagesModel, pos: Int) {
                    super.onPositionClick(v, m, pos)

                }
            })
        binding.rvInboxMessages.adapter = inboxMessageAdapter
        inboxMessageAdapter.list = AppUtils.getInboxMessagesList()
    }

    private fun initNotificationAdapter() {
        notificationAdapter = RVAdapter(
            R.layout.custom_notification_item,
            BR.bean,
            object : RVAdapter.Callback<MessageNotificationsModel> {
                override fun onItemClick(v: View?, m: MessageNotificationsModel) {}

                override fun onPositionClick(v: View?, m: MessageNotificationsModel, pos: Int) {
                    super.onPositionClick(v, m, pos)

                }
            })
        binding.rvNotifications.adapter = notificationAdapter
        notificationAdapter.list = AppUtils.getNotificationList()

    }

    private fun initOnclick() {
        homePayerActivity = activity as HomePayerActivity
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivMenu -> {
                    homePayerActivity.openSideMenu()
                }
                R.id.tvNewChat -> {
                    startActivity(Intent(context, SearchActivity::class.java))
                }
                R.id.ivProfile -> {
                    homePayerActivity.startNewActivity(Intent(requireContext(), ProfileActivity::class.java))
                }
            }
        }

    }

    private fun initView() {
        //binding.toolBar.tvTitle.text = getString(R.string.messages)
        homePayerActivity = activity as HomePayerActivity
        homePayerActivity.window.statusBarColor =
            ContextCompat.getColor(homePayerActivity, R.color.white)
    }

}