package com.techwin.omatochi.ui.home_payer.health_records.allergies.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.AllergyModel
import com.techwin.omatochi.data.model.InActiveMedicationModel
import com.techwin.omatochi.databinding.CustomActiveAllergyItemBinding
import com.techwin.omatochi.databinding.CustomActiveMedicationItemBinding
import com.zerobranch.layout.SwipeLayout


class CustomActiveAllergyAdapter(val callback: CardCallback) :
    RecyclerView.Adapter<CustomActiveAllergyAdapter.RequestHolder>() {

    private var moreBeans: List<AllergyModel> = ArrayList()
    var swipeLayout: SwipeLayout? = null

    interface CardCallback {
        fun onItemClick(v: View?, m: AllergyModel?, pos: Int?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RequestHolder {
        val layoutTodoListBinding: CustomActiveAllergyItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.custom_active_allergy_item, parent, false
        )
        layoutTodoListBinding.setVariable(BR.callback, callback)
        return RequestHolder(layoutTodoListBinding)
    }

    override fun onBindViewHolder(
        holder: RequestHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        holder.layoutTodoListBinding.bean = moreBeans[position]
        holder.layoutTodoListBinding.pos = position
        holder.layoutTodoListBinding.swipeLayout.setOnActionsListener(object :
            SwipeLayout.SwipeActionsListener {
            override fun onOpen(direction: Int, isContinuous: Boolean) {
                if (direction == SwipeLayout.LEFT) {
                    if (swipeLayout != null && swipeLayout != holder.layoutTodoListBinding.swipeLayout) {
                        swipeLayout?.close(true)
                    }
                    swipeLayout = holder.layoutTodoListBinding.swipeLayout
                }
            }

            override fun onClose() {
                // the main view has returned to the default state
                if (swipeLayout == holder.layoutTodoListBinding.swipeLayout) swipeLayout = null
            }
        })
    }

    override fun getItemCount(): Int {
        return moreBeans.size
    }

    fun setList(moreBeans: List<AllergyModel>) {
        this.moreBeans = moreBeans
        notifyDataSetChanged()
    }

    fun clear() {
        moreBeans.isEmpty()
        notifyDataSetChanged()
    }

    fun getList(): List<AllergyModel> {
        return moreBeans
    }

    class RequestHolder(val layoutTodoListBinding: CustomActiveAllergyItemBinding) :
        RecyclerView.ViewHolder(layoutTodoListBinding.root)
}