package com.techwin.omatochi.ui.home_payer.new_user_chat

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.provider.MediaStore
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityNewChatBinding
import com.techwin.omatochi.databinding.SendImageConfirmationDialogPopupBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_caregiver.new_user_chat.CustomDummyChatCareGiverAdapter
import com.techwin.omatochi.ui.home_payer.ImageShowActivity
import com.techwin.omatochi.ui.home_payer.VideoPlayerActivity
import com.techwin.omatochi.ui.home_payer.account.payments_invoice.CreditCardBean
import com.techwin.omatochi.utils.dialog.BaseCustomDialog

class NewChatActivity : BaseActivity<ActivityNewChatBinding>(), BaseCustomDialog.Listener {
    private val viewModel: NewChatActivityVM by viewModels()
    private val MY_CAMERA_PERMISSION_CODE = 100
    private lateinit var popup: BaseCustomDialog<SendImageConfirmationDialogPopupBinding>
    private lateinit var customDummyChatAdapter: CustomDummyChatAdapter

    override fun getLayoutResource(): Int {
        return R.layout.activity_new_chat
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView() {
        initView()
        initOnClick()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBackNewChat -> {
                    finish()
                }
                R.id.ivCameraNewChat -> {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                            arrayOf(Manifest.permission.CAMERA),
                            MY_CAMERA_PERMISSION_CODE
                        )
                    } else {
                        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        resultLauncher.launch(cameraIntent)
                    }
                }
                R.id.ivCloseNewChat -> {
                    binding.cvNewChatImagePickUp.visibility = View.GONE
                }
                R.id.ivSendChatNewChat -> {
                    if (binding.etvNewChat.text.toString() == "") {
                        showToast("Please enter message")
                    } else {
                        binding.tvStartMsgNow.visibility = View.GONE
                        binding.tvNoConvNewChat.visibility = View.GONE
                        customDummyChatAdapter.addItemTOList(binding.etvNewChat.text.toString())
                        binding.cvNewChatImagePickUp.visibility = View.INVISIBLE
                        binding.etvNewChat.setText("")
                        binding.rvDummyChatShow.smoothScrollToPosition(customDummyChatAdapter.getList().size-1)
                    }
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                resultLauncher.launch(cameraIntent)
            } else {
                showToast("camera permission required")
            }
        }
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                val photo = data?.extras!!["data"] as Bitmap?
                binding.ivImageNewChat.setImageBitmap(photo)
                popup.show()
            }
        }

    private fun initView() {
        initDialog()
        initCustomChatAdapterAdapter()
    }

    private fun initDialog() {
        popup = BaseCustomDialog<SendImageConfirmationDialogPopupBinding>(
            this,
            R.layout.send_image_confirmation_dialog_popup,
            this
        )
        popup.setCanceledOnTouchOutside(false)
    }

    override fun onViewClick(view: View?) {
        when (view?.id) {
            R.id.tvCancelImageSend -> {
                binding.cvNewChatImagePickUp.visibility = View.VISIBLE
                popup.dismiss()
            }
            R.id.tvSubmitImageSend -> {
                binding.cvNewChatImagePickUp.visibility = View.VISIBLE
                popup.dismiss()
            }
        }
    }

    private fun initCustomChatAdapterAdapter() {
        customDummyChatAdapter = CustomDummyChatAdapter(object :CustomDummyChatAdapter.ChatItemCallback{
            override fun onItemClick(v: View?, m: String?, pos: Int?) {
                when(pos) {
                    0 -> { // for static video playing
                        startActivity(Intent(this@NewChatActivity, VideoPlayerActivity::class.java))
                    }
                    3 -> { // for static image show
                        startActivity(Intent(this@NewChatActivity, ImageShowActivity::class.java))
                    }
                }
            }

        })
        val layoutManager = LinearLayoutManager(this)
        layoutManager.stackFromEnd = true
        binding.rvDummyChatShow.layoutManager = layoutManager
        binding.rvDummyChatShow.adapter = customDummyChatAdapter
        //customDummyChatAdapter.setList(msgList)
    }
}