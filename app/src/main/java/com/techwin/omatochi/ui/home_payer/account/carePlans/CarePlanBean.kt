package com.techwin.omatochi.ui.home_payer.account.carePlans

import java.io.Serializable

data class CarePlanBean(
    var image: Int,
    var title: String,
    var subTitle: String,
    var activated: Boolean
) : Serializable