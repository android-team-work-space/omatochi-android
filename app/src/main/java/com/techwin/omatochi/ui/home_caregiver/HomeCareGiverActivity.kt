package com.techwin.omatochi.ui.home_caregiver

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationBarView
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityCareGiverBinding
import com.techwin.omatochi.databinding.HolderMenuBinding
import com.techwin.omatochi.databinding.RateUsDialogPopupBinding
import com.techwin.omatochi.ui.auth.login.LoginActivity
import com.techwin.omatochi.ui.auth.verification.VerificationActivity
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_caregiver.faqs_caregiver.FAQActivity2
import com.techwin.omatochi.ui.home_caregiver.fragment.new_shift.NewShiftFragment
import com.techwin.omatochi.ui.home_caregiver.fragment.notes.NotesFragment
import com.techwin.omatochi.ui.home_caregiver.fragment.past_shifts.PastShiftsFragment
import com.techwin.omatochi.ui.home_caregiver.privacy_policy_caregiver.PrivacyPolicyActivity2
import com.techwin.omatochi.ui.home_caregiver.profile.CareGiverProfile
import com.techwin.omatochi.ui.home_caregiver.support_caregiver.SupportActivityCG
import com.techwin.omatochi.ui.home_caregiver.terms_and_conditions_caregiver.TermsAndConditionsActivityCG
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.dialog.BaseCustomDialog
import com.techwin.omatochi.utils.showInfoToast
import com.techwin.omatochi.utils.showSuccessToast
import dagger.hilt.android.AndroidEntryPoint
@AndroidEntryPoint
class HomeCareGiverActivity : BaseActivity<ActivityCareGiverBinding>(), BaseCustomDialog.Listener {
    private val viewModel: HomeCareGiverActivityVM by viewModels()
    private lateinit var adapterMenu: RVAdapter<MenuItemBean2, HolderMenuBinding>
    private lateinit var navController: NavController
    private var exit: Boolean = false
    private lateinit var popup: BaseCustomDialog<RateUsDialogPopupBinding>

    companion object {
        fun newIntent(activity: Activity): Intent {
            val intent = Intent(activity, HomeCareGiverActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_care_giver
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onSaveInstanceState(oldInstanceState: Bundle) {
        super.onSaveInstanceState(oldInstanceState)
        oldInstanceState.clear()
    }

    override fun onCreateView() {
        binding.root.isSaveFromParentEnabled = false
        initView()
        initOnclick()
    }

    private fun initView() {
        initDialog()
        initAdapter()
        initBottomNavigation()
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnForgot -> {
                    val intent = VerificationActivity.newIntent(this)
                    startActivity(intent)
                }
                R.id.btnLogout -> {
                    openSideMenu()
                    showSuccessToast("Logout successfully.")
                    val intent = LoginActivity.newIntent(this)
                    startActivity(intent)
                }
            }
        }
    }

    fun openSideMenu() {
        if (binding.mainDl.isDrawerOpen(GravityCompat.START))
            binding.mainDl.closeDrawer(GravityCompat.START)
        else binding.mainDl.openDrawer(GravityCompat.START)
    }

    private fun initAdapter() {
        adapterMenu =
            RVAdapter(R.layout.holder_menu, BR.bean, object : RVAdapter.Callback<MenuItemBean2> {
                override fun onItemClick(v: View?, m: MenuItemBean2) {

                }

                override fun onPositionClick(v: View?, m: MenuItemBean2, pos: Int) {
                    when (pos) {
                        0 -> {  // Help Guide
                            openSideMenu()
                            NewShiftFragment.helpGuide.value = true
                            PastShiftsFragment.helpGuide.value = true
                            NotesFragment.helpGuide.value = true
                        }
                        1 -> {  // My account
                            openSideMenu()
                            val intent = CareGiverProfile.newIntent(this@HomeCareGiverActivity)
                            startActivity(intent)
                        }
                        2 -> { //shift
                            //not implemented yet....
                        }
                        3 -> {  //Rate Us
                            openSideMenu()
                            popup.show()
                        }
                        4 -> { //T&C
                            openSideMenu()
                            startNewActivity(
                                Intent(
                                    this@HomeCareGiverActivity,
                                    TermsAndConditionsActivityCG::class.java
                                )
                            )
                        }
                        5 -> { //privacy policy
                            openSideMenu()
                            startNewActivity(
                                Intent(
                                    this@HomeCareGiverActivity,
                                    PrivacyPolicyActivity2::class.java
                                )
                            )
                        }
                        6 -> { //FAQs
                            openSideMenu()
                            startNewActivity(
                                Intent(
                                    this@HomeCareGiverActivity,
                                    FAQActivity2::class.java
                                )
                            )
                        }
                        7 -> { // Support
                            openSideMenu()
                            startNewActivity(
                                Intent(
                                    this@HomeCareGiverActivity,
                                    SupportActivityCG::class.java
                                )
                            )
                        }
                    }
                }
            })
        binding.rvMenuItems.adapter = adapterMenu
        adapterMenu.list = AppUtils.getSideMenuList2()
    }

    private fun initBottomNavigation() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
        navController = navHostFragment!!.navController
        NavigationUI.setupWithNavController(binding.cgBottomNavigation, navController)
    }

    @SuppressLint("RestrictedApi")
    override fun onBackPressed() {
        if (navController.backStack.size > 2) {
            if (!navController.popBackStack())
                backPress()
        } else
            backPress()
    }

    private fun backPress() {
        if (exit) {
            finishAffinity()
        } else {
            showInfoToast("Press again for exit")
            exit = true
            Handler(Looper.myLooper()!!).postDelayed({ exit = false }, 2000)
        }
    }

    private fun initDialog() {
        popup = BaseCustomDialog<RateUsDialogPopupBinding>(
            this,
            R.layout.rate_us_dialog_popup,
            this
        )
        popup.setCanceledOnTouchOutside(false)
    }

    fun navigateNewVisit() {
        binding.cgBottomNavigation.setOnItemSelectedListener(object :
            NavigationBarView.OnItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                if (item.itemId == R.id.newShiftsFragment) {
                    navController.navigate(R.id.newVisitFragment)
                    return true
                }
                if (item.itemId == R.id.pastShiftsFragment){
                    navController.navigate(R.id.pastShiftsFragment)
                    return true
                }
                if (item.itemId == R.id.recipeintsFragment){
                    navController.navigate(R.id.recipeintsFragment)
                    return true
                }
                if (item.itemId == R.id.messagesFragment){
                    navController.navigate(R.id.messagesFragment)
                    return true
                }
                if (item.itemId == R.id.notesFragment){
                    navController.navigate(R.id.notesFragment)
                    return true
                }
                else{
                    navController.navigate(R.id.newShiftsFragment)
                    return true
                }
                return false
            }
        })
    }

  fun navigateNewShift() {
        binding.cgBottomNavigation.setOnItemSelectedListener(object :
            NavigationBarView.OnItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                if (item.itemId == R.id.newShiftsFragment) {
                    navController.navigate(R.id.newShiftsFragment)
                    return true
                }
                if (item.itemId == R.id.pastShiftsFragment){
                    navController.navigate(R.id.pastShiftsFragment)
                    return true
                }
                if (item.itemId == R.id.recipeintsFragment){
                    navController.navigate(R.id.recipeintsFragment)
                    return true
                }
                if (item.itemId == R.id.messagesFragment){
                    navController.navigate(R.id.messagesFragment)
                    return true
                }
                if (item.itemId == R.id.notesFragment){
                    navController.navigate(R.id.notesFragment)
                    return true
                }
                else{
                    navController.navigate(R.id.newShiftsFragment)
                    return true
                }
                return false
            }
        })
    }

    override fun onViewClick(view: View?) {
        when (view?.id) {
            R.id.tvCancelRateUS -> {
                popup.dismiss()
            }
            R.id.tvSubmitRateUS -> {
                popup.dismiss()
            }
        }
    }
}