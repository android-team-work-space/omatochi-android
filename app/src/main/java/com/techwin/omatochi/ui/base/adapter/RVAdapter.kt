package com.techwin.omatochi.ui.base.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.techwin.omatochi.BR
import java.util.*

class RVAdapter<M, B : ViewDataBinding>(
    @field:LayoutRes @param:LayoutRes private val layoutResId: Int,
    private val modelVariableId: Int,
    private val callback: Callback<M>
) : RecyclerView.Adapter<RVAdapter.Holder<B>>() {

    private val dataList: MutableList<M> = ArrayList()

    fun removeItem(i: Int) {
        try {
            if (i != -1) {
                dataList.removeAt(i)
                notifyItemRemoved(i)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    operator fun set(i: Int, scanResult: M?) {
        if (scanResult == null) return
        dataList.add(i, scanResult)
        notifyItemChanged(i)
    }

    interface Callback<M> {
        fun onItemClick(v: View?, m: M)
        fun onPositionClick(v: View?, m: M, pos: Int) {}
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    var list: List<M>?
        get() = dataList
        set(newDataList) {
            dataList.clear()
            if (newDataList != null)
                dataList.addAll(newDataList)
            notifyDataSetChanged()
        }

    fun addToList(newDataList: List<M>?) {
        var newDataList = newDataList
        if (newDataList == null) {
            newDataList = emptyList()
        }
        val positionStart = dataList.size
        val itemCount = newDataList.size
        dataList.addAll(newDataList)
        notifyItemRangeInserted(positionStart, itemCount)
    }

    fun clearList() {
        dataList.clear()
        notifyDataSetChanged()
    }

    fun addData(data: M) {
        val positionStart = dataList.size
        dataList.add(data)
        notifyItemInserted(positionStart)
    }

    /**
     * Simple view holder for this adapter
     *
     * @param <S>
    </S> */
    class Holder<S : ViewDataBinding>(var binding: S) : RecyclerView.ViewHolder(
        binding.root
    )

    override fun onBindViewHolder(holder: Holder<B>, position: Int) {
        onBind(holder.binding, dataList[position], position)
        holder.binding.setVariable(BR.holder, holder)
        holder.binding.setVariable(BR.pos,position)
        holder.binding.executePendingBindings()
    }

    fun onBind(binding: B, bean: M, position: Int) {
        binding.setVariable(modelVariableId, bean)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder<B> {
        val binding: B =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), layoutResId, parent, false)
        binding.setVariable(BR.callback, callback)
        return Holder(binding)
    }
}
