package com.techwin.omatochi.ui.home_caregiver.activity.exercise

import android.annotation.SuppressLint
import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.provider.MediaStore
import android.view.View
import android.widget.TimePicker
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityAddExerciseCaregiverBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddExerciseCaregiverActivity : BaseActivity<ActivityAddExerciseCaregiverBinding>(),
    TimePickerDialog.OnTimeSetListener {
    private val viewModel: AddExerciseCaregiverActivityVM by viewModels()
    private val MY_CAMERA_PERMISSION_CODE = 100
    var timeCheck: Int = 0

    override fun getLayoutResource(): Int {
        return R.layout.activity_add_exercise_caregiver
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    @SuppressLint("SimpleDateFormat")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView() {
        initView()
        initOnClick()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.llAddVideo -> {
                    if (checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                            arrayOf(android.Manifest.permission.CAMERA),
                            MY_CAMERA_PERMISSION_CODE
                        )
                    } else {
                        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        resultLauncher.launch(cameraIntent)
                    }
                }
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnAddExercise -> {
                    when {
                        binding.etvAddExerciseTitle.text.toString() == "" -> {
                            showToast("Please enter exercise title.")
                        }
                        binding.etStartTime.text.toString() == "" -> {
                            showToast("please select start time.")
                        }
                        binding.etEndTime.text.toString() == "" -> {
                            showToast("please select end time.")
                        }
                        binding.etSets.text.toString() == "" -> {
                            showToast("please select number of sets.")
                        }
                        binding.etReps.text.toString() == "" -> {
                            showToast("please select number of reps.")
                        }
                        binding.etNote.text.toString() == "" -> {
                            showToast("please enter note.")
                        }
                        else -> {
                            showToast("Exercise Will be added")
                        }
                    }
                }
                R.id.etStartTime -> {
                    timeCheck = 0
                    openTimePicker()
                }
                R.id.etEndTime -> {
                    timeCheck = 1
                    openTimePicker()
                }
            }
        }
    }

    private fun openTimePicker() {
        val calendar = java.util.Calendar.getInstance()
        val hourOfDay = calendar.get(java.util.Calendar.HOUR_OF_DAY)
        val minute = calendar.get(java.util.Calendar.MINUTE)
        val timePickerDialog = TimePickerDialog(this, this, hourOfDay, minute, true)
        timePickerDialog.show()
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                val photo = data?.extras!!["data"] as Bitmap?
                binding.ivAddVideo.visibility = View.VISIBLE
                binding.ivAddVideoPlus.visibility = View.GONE
                binding.ivAddVideo.setImageBitmap(photo)
            }
        }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                resultLauncher.launch(cameraIntent)
            } else {
                showToast("camera permission required")
            }
        }
    }

    private fun initView() {
        binding.tbAddExercise.tvTitle.text = getString(R.string.add_exercise)
    }

    @SuppressLint("SetTextI18n")
    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        if (timeCheck == 0) {
            binding.etStartTime.setText("$hourOfDay:$minute")
        } else {
            binding.etEndTime.setText("$hourOfDay:$minute")
        }
    }
}