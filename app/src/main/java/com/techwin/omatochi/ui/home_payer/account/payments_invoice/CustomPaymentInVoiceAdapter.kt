package com.techwin.omatochi.ui.home_payer.account.payments_invoice

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.CustomAllCardsBinding

import com.zerobranch.layout.SwipeLayout
import com.zerobranch.layout.SwipeLayout.NO_ID
import com.zerobranch.layout.SwipeLayout.SwipeActionsListener

class CustomPaymentInVoiceAdapter(val callback: CardCallback) :
    RecyclerView.Adapter<CustomPaymentInVoiceAdapter.RequestHolder>() {

    private var moreBeans = ArrayList<CreditCardBean>()
    var swipeLayout: SwipeLayout? = null

   interface CardCallback {
        fun onItemClick(v: View?, m: CreditCardBean?, pos: Int?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RequestHolder {
        val layoutTodoListBinding: CustomAllCardsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.custom_all_cards, parent, false
        )
        layoutTodoListBinding.setVariable(BR.callback, callback)
        return RequestHolder(layoutTodoListBinding)
    }

    override fun onBindViewHolder(
        holder: RequestHolder,
        position: Int
    ) {
        holder.layoutTodoListBinding.bean = moreBeans[position]
        holder.layoutTodoListBinding.pos = position

        holder.layoutTodoListBinding.swipeLayout.setOnActionsListener(object :
            SwipeActionsListener {
            override fun onOpen(direction: Int, isContinuous: Boolean) {
                if (direction == SwipeLayout.LEFT) {
                    if (swipeLayout != null && swipeLayout != holder.layoutTodoListBinding.swipeLayout) {
                        swipeLayout?.close(true)
                    }
                    swipeLayout = holder.layoutTodoListBinding.swipeLayout
                }
            }

            override fun onClose() {
                // the main view has returned to the default state
                if (swipeLayout == holder.layoutTodoListBinding.swipeLayout) swipeLayout = null
            }
        })
    }

    override fun getItemCount(): Int {
       return moreBeans.size
    }

    override fun getItemId(position: Int): Long {
        return -1
    }


    fun setList(moreBea: ArrayList<CreditCardBean>) {
        this.moreBeans=moreBea
        notifyDataSetChanged()
    }

    fun clear() {
        moreBeans.isEmpty()
        notifyDataSetChanged()
    }

    fun getList(): List<CreditCardBean> {
        return moreBeans
    }

    class RequestHolder(val layoutTodoListBinding: CustomAllCardsBinding) :
        RecyclerView.ViewHolder(layoutTodoListBinding.root)
}