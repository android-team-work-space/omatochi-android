package com.techwin.omatochi.ui.home_caregiver.activity.new_activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.provider.MediaStore
import android.view.View
import android.widget.TimePicker
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityAddNewActivityBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_add_new_activity.*

@AndroidEntryPoint
class AddNewActivity : BaseActivity<ActivityAddNewActivityBinding>(), OnMapReadyCallback,
    TimePickerDialog.OnTimeSetListener {
    private val viewModel: AddNewActivityVM by viewModels()
    private val MY_CAMERA_PERMISSION_CODE = 100
    val position = LatLng(41.015137, 28.979530)
    private var markerOptions = MarkerOptions().position(position)
    private lateinit var marker: Marker
    var timeCheck : Int = 0

    override fun getLayoutResource(): Int {
        return R.layout.activity_add_new_activity
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView() {
        initView()
        initOnClick()
        with(customMapView) {
            onCreate(null)
            getMapAsync {
                MapsInitializer.initialize(applicationContext)
                setMapLocation(it)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        customMapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        customMapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        customMapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        customMapView.onLowMemory()
    }

    private fun setMapLocation(map: GoogleMap) {
        with(map) {
            moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13f))
            mapType = GoogleMap.MAP_TYPE_NORMAL
            setOnMapClickListener {
                if (::marker.isInitialized) {
                    marker.remove()
                }
                markerOptions.position(it)
                marker = addMarker(markerOptions)!!
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.llAddVideoOrImage -> {
                    if (checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                            arrayOf(android.Manifest.permission.CAMERA),
                            MY_CAMERA_PERMISSION_CODE
                        )
                    } else {
                        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        resultLauncher.launch(cameraIntent)
                    }
                }
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnAddActivity -> {
                    when {
                        binding.etActivity.text.toString() == "" -> {
                            showToast("Please enter activity type.")
                        }
                        binding.etStartTime.text.toString() == "" -> {
                            showToast("please enter activity start time.")
                        }
                        binding.etEndTime.text.toString() == "" -> {
                            showToast("please enter activity end time.")
                        }
                        binding.etAddress.text.toString() == "" -> {
                            showToast("please enter address.")
                        }
                        binding.etNote.text.toString() == "" -> {
                            showToast("please enter note.")
                        }
                        else -> {
                            showToast("Activity Will be added")
                        }
                    }
                }
                R.id.etStartTime -> {
                    timeCheck = 0
                    openTimePicker()
                }
                R.id.etEndTime -> {
                    timeCheck = 1
                    openTimePicker()
                }
            }
        }
    }

    private fun openTimePicker() {
        val calendar = java.util.Calendar.getInstance()
        val hourOfDay = calendar.get(java.util.Calendar.HOUR_OF_DAY)
        val minute = calendar.get(java.util.Calendar.MINUTE)
        val timePickerDialog = TimePickerDialog(this, this, hourOfDay, minute, true)
        timePickerDialog.show()
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                val photo = data?.extras!!["data"] as Bitmap?
                binding.ivAddVideoImage.visibility = View.VISIBLE
                binding.ivAddVideoImagePlus.visibility = View.GONE
                binding.ivAddVideoImage.setImageBitmap(photo)
            }
        }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                resultLauncher.launch(cameraIntent)
            } else {
                showToast("camera permission required")
            }
        }
    }

    private fun initView() {
        binding.tbAddNewActivity.tvTitle.text = getString(R.string.add_new_activity)
        this.window.statusBarColor = ContextCompat.getColor(this,R.color.white)
    }

    override fun onMapReady(googleMap: GoogleMap) {

    }

    @SuppressLint("SetTextI18n")
    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        if (timeCheck == 0) {
            binding.etStartTime.setText("$hourOfDay:$minute")
        } else {
            binding.etEndTime.setText("$hourOfDay:$minute")
        }
    }
}