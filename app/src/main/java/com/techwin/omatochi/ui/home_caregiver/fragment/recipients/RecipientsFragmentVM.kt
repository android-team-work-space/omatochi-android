package com.techwin.omatochi.ui.home_caregiver.fragment.recipients

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RecipientsFragmentVM @Inject constructor(): BaseViewModel() {
}