package com.techwin.omatochi.ui.home_payer.fragment.past_visits

import android.content.Intent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.CustomRateVisitDialogPopupBinding
import com.techwin.omatochi.databinding.FragmentPastVisitsBinding
import com.techwin.omatochi.databinding.HolderPostVisitsBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_payer.HomePayerActivity
import com.techwin.omatochi.ui.home_payer.account.profile.ProfileActivity
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.dialog.BaseCustomDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PastVisitsFragment : BaseFragment<FragmentPastVisitsBinding>(), BaseCustomDialog.Listener {
    private val viewModel: PastVisitFragmentVM by viewModels()
    private lateinit var adapterPastVisit: RVAdapter<PastVisitBean, HolderPostVisitsBinding>
    private lateinit var homePayerActivity: HomePayerActivity
    private lateinit var popup: BaseCustomDialog<CustomRateVisitDialogPopupBinding>
    private var check = false
    companion object {
        fun newIntent(): Fragment {
            return PastVisitsFragment()
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_past_visits
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        initView()
        initAdapter()
        initOnclick()
        initDialogPopup()
    }

    private fun initDialogPopup() {
        popup = BaseCustomDialog<CustomRateVisitDialogPopupBinding>(
            requireContext(),
            R.layout.custom_rate_visit_dialog_popup,
            this
        )
        popup.setCanceledOnTouchOutside(false)

    }

    private fun initAdapter() {
        adapterPastVisit = RVAdapter(
            R.layout.holder_post_visits,
            BR.bean,
            object : RVAdapter.Callback<PastVisitBean> {
                override fun onItemClick(v: View?, m: PastVisitBean) {

                }

                override fun onPositionClick(v: View?, m: PastVisitBean, pos: Int) {
                    when (v?.id) {
                        R.id.tvRateVisit -> {
                            popup.show()
                        }
                    }
                }
            })
        binding.rvPastVisit.adapter = adapterPastVisit
        adapterPastVisit.list = AppUtils.getPastVisitList()
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivMenu -> {
                    homePayerActivity.openSideMenu()
                }
                R.id.ivProfile -> {
                    homePayerActivity.startNewActivity(Intent(requireContext(), ProfileActivity::class.java))
                }
            }
        }
    }

    private fun initView() {
        homePayerActivity = activity as HomePayerActivity
        //binding.toolBar.tvTitle.text = getString(R.string.past_visits)
        homePayerActivity.window.statusBarColor =
            ContextCompat.getColor(homePayerActivity, R.color.white)
    }

    override fun onViewClick(view: View?) {
        when (view?.id) {
            R.id.tvCancelRateVisit -> {
                popup.dismiss()
            }
            R.id.tvSubmitRateVisit -> {
                popup.dismiss()
            }
            R.id.ivRateVisitUserLike -> {
                check = if (check){
                    popup.binding.ivRateVisitUserLike.setBackgroundResource(R.drawable.ic_red_filled_heart)
                    false
                }else{
                    popup.binding.ivRateVisitUserLike.setBackgroundResource(R.drawable.ic_red_heart)
                    true
                }
            }
        }
    }
}