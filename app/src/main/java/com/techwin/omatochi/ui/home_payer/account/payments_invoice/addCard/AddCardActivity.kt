package com.techwin.omatochi.ui.home_payer.account.payments_invoice.addCard

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityAddCardBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.utils.showErrorToast
import com.techwin.omatochi.utils.showInfoToast
import dagger.hilt.android.AndroidEntryPoint
import java.util.*


@AndroidEntryPoint
class AddCardActivity : BaseActivity<ActivityAddCardBinding>() {

    private val viewModel: AddCardActivityVM by viewModels()

    companion object {
        fun newIntent(activity: Activity): Intent {
            val intent = Intent(activity, AddCardActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_add_card
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnclick()
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnValidate -> {
                    onValidate()
                }
                R.id.etExpireDate -> {
                    openChooseDatePicker()
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun openChooseDatePicker() {
        val yearSelected: Int
        val monthSelected: Int
        val calendar: Calendar = Calendar.getInstance()
        yearSelected = calendar.get(Calendar.YEAR)
        monthSelected = calendar.get(Calendar.MONTH)
        val dialogFragment = MonthYearPickerDialogFragment
            .getInstance(monthSelected, yearSelected, "Select Card Expire Date :")
        dialogFragment.show(supportFragmentManager, null)
        dialogFragment.setOnDateSetListener { year, monthOfYear ->
            val month = monthOfYear + 1
            binding.etExpireDate.text = "$month/$year"
        }
    }

    private fun onValidate() {
        if (binding.etCardNumber.text.trim().toString().isEmpty()) {
            showErrorToast("Please enter card number.")
            return
        }
        if (binding.etCardHolderName.text.trim().toString().isEmpty()) {
            showErrorToast("Please enter card holder name.")
            return
        }
        if (binding.etExpireDate.text.trim().toString().isEmpty()) {
            showErrorToast("Please enter card expire date.")
            return
        }
        if (binding.etCvv.text.trim().toString().isEmpty()) {
            showErrorToast("Please enter card cvv.")
            return
        }
        binding.tvCardNumber.text = binding.etCardNumber.text.trim().toString()
        binding.tvName.text = binding.etCardHolderName.text.trim().toString()
        binding.tvDate.text = binding.etExpireDate.text.trim().toString()
        showInfoToast("Card validate successful.")
    }


    private fun initView() {
        binding.toolBar.tvTitle.text = getString(R.string.add_card)
        window.statusBarColor = ContextCompat.getColor(this,R.color.white)
    }

}