package com.techwin.omatochi.ui.home_payer.health_records.allergies.fragments

import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.AllergyModel
import com.techwin.omatochi.databinding.FragmentInActiveAllergiesBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_payer.health_records.allergies.AllergyListActivity
import com.techwin.omatochi.ui.home_payer.health_records.allergies.adapters.CustomInActiveAllergyAdapter
import com.techwin.omatochi.ui.home_payer.health_records.allergies.vm.InActiveAllergiesFragmentVM
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.showToast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InActiveAllergiesFragment : BaseFragment<FragmentInActiveAllergiesBinding>() {
    private val viewModel: InActiveAllergiesFragmentVM by viewModels()
    private lateinit var allergyListActivity: AllergyListActivity
    private lateinit var customInActiveAllergyAdapter: CustomInActiveAllergyAdapter
    var rotationAngle = 0f


    companion object {
        fun newIntent(): Fragment {
            return InActiveAllergiesFragment()
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_in_active_allergies
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        initView()
        initOnclick()
    }


    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {

            }
        }
    }

    private fun initView() {
        allergyListActivity = activity as AllergyListActivity
        allergyListActivity.window.statusBarColor =
            ContextCompat.getColor(allergyListActivity, R.color.white)

        initAdapter()
    }

    private fun initAdapter() {
        customInActiveAllergyAdapter =
            CustomInActiveAllergyAdapter(object : CustomInActiveAllergyAdapter.CardCallback {
                override fun onItemClick(v: View?, m: AllergyModel?, pos: Int?) {
                    when (v?.id) {
                        R.id.layEdit -> {

                        }
                        R.id.layInActive -> {
                            showToast("Added to Active Allergies")
                            customInActiveAllergyAdapter.swipeLayout?.close()
                        }
                        R.id.ivDownInActiveAllergy,R.id.clInActiveAllergy -> {
                              m?.expended = !m!!.expended
                           customInActiveAllergyAdapter.notifyItemChanged(pos!!)
                        }
                    }
                }
            })
        customInActiveAllergyAdapter.setList(AppUtils.getInActiveAllergyList())
        binding.rvInActiveAllergy.adapter = customInActiveAllergyAdapter
    }

}