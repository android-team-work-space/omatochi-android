package com.techwin.omatochi.ui.welcome

import android.app.Activity
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityWelcomeBinding
import com.techwin.omatochi.ui.auth.login.LoginActivity
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.utils.SimpleSpanBuilder
import com.techwin.omatochi.utils.showSuccessToast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WelcomeActivity : BaseActivity<ActivityWelcomeBinding>() {

    private val viewModel: WelcomeActivityVM by viewModels()
    private var mSignInClient: GoogleSignInClient? = null
    
    override fun getLayoutResource(): Int {
        return R.layout.activity_welcome
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.btnLogin -> {
                    val intent = LoginActivity.newIntent(this)
                    startActivity(intent)
                }
                R.id.btnGoogleLogin -> {
                    val intent = mSignInClient!!.signInIntent
                    resultLauncher.launch(intent)

                }
            }
        }
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(result.data)
                try {
                    val account = task.getResult(ApiException::class.java)
                    handleGoogleSignInResult(task)
                } catch (e: ApiException) {
                }
            }
        }

    private fun handleGoogleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            val map = HashMap<String, String?>()
            map["email"] = account.email
            map["name"] = account.displayName
            showSuccessToast("Welcome ${account.givenName}")

            mSignInClient?.signOut()?.addOnSuccessListener {
            }

            val intent = LoginActivity.newIntent(this)
            startActivity(intent)

        } catch (e: ApiException) {
        }
    }

    private fun initView() {
        val ssb = SimpleSpanBuilder()
        ssb.appendWithSpace("Welcome to")
        ssb.append(
            "Omatochi",
            ForegroundColorSpan(resources.getColor(R.color.blue)),
            RelativeSizeSpan(1f)
        )
        binding.tveWelcome.text = ssb.build()

        val options = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .requestProfile()
            .build()
        mSignInClient = GoogleSignIn.getClient(this, options)
    }

}