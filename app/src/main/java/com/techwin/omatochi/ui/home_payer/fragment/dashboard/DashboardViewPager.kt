package com.techwin.omatochi.ui.home_payer.fragment.dashboard

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.techwin.omatochi.ui.home_payer.fragment.dashboard.dashboard2.Dashboard2Fragment
import com.techwin.omatochi.ui.home_payer.fragment.dashboard.feeds.FeedsFragment

class DashboardViewPager(fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                FeedsFragment.newIntent()
            }
            1 -> {
                Dashboard2Fragment.newIntent()
            }
            else -> {
                Dashboard2Fragment.newIntent()
            }
        }
    }
}
