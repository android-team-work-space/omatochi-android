package com.techwin.omatochi.ui.home_payer.account.payments_invoice

data class CreditCardBean(
    var image: Int,
    var name: String,
    var date: String,
    var cardNumber: String,
    var autoPay: String,
    var isSelected: Boolean
)