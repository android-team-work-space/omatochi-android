package com.techwin.omatochi.ui.home_payer.health_records.exercise

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AddExerciseActivityVM @Inject constructor() : BaseViewModel() {
}