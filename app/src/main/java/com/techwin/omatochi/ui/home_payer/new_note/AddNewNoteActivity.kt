package com.techwin.omatochi.ui.home_payer.new_note

import android.widget.ArrayAdapter
import androidx.activity.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityAddNewNoteBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_payer.AddNewNoteActivityVM
import com.techwin.omatochi.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddNewNoteActivity : BaseActivity<ActivityAddNewNoteBinding>() {

    private val viewModel: AddNewNoteActivityVM by viewModels()

    override fun getLayoutResource(): Int {
        return R.layout.activity_add_new_note
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this){
            when(it?.id){
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnAddNewNote -> {
                    showToast("Will be saved")
                }
            }
        }
    }

    private fun initView() {
        binding.tbAddContact.tvTitle.text = getString(R.string.add_note)
        initNewNoteAdapter()
    }

    private fun initNewNoteAdapter() {
        val adapter = ArrayAdapter(
            this,
            R.layout.custom_spinner,
            R.id.tvSpinner,
            AppUtils.getAddNewNoteTypeList()
        )
        adapter.setDropDownViewResource(R.layout.custom_spinner)
        binding.spAddNewNote.adapter = adapter
    }
}