package com.techwin.omatochi.ui.home_payer.health_records.medication_list

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.techwin.omatochi.ui.home_payer.health_records.medication_list.fragment.ActiveMedicationFragment
import com.techwin.omatochi.ui.home_payer.health_records.medication_list.fragment.InActiveMedicationFragment

class MedicationListViewPager(fragmentActivity: FragmentActivity):
    FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> {
                return ActiveMedicationFragment.newIntent()
            }
            1 -> {
                return InActiveMedicationFragment.newIntent()
            }
            else -> {
                return InActiveMedicationFragment.newIntent()
            }
        }
    }
}
