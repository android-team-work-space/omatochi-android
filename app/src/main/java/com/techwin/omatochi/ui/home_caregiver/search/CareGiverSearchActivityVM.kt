package com.techwin.omatochi.ui.home_caregiver.search

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CareGiverSearchActivityVM @Inject constructor() : BaseViewModel() {

}