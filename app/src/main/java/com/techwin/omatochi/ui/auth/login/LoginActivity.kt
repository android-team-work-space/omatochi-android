package com.techwin.omatochi.ui.auth.login

import android.app.Activity
import android.content.Intent
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityLoginBinding
import com.techwin.omatochi.databinding.SheetChooseAppTypeBinding
import com.techwin.omatochi.ui.auth.forgot.ForgotActivity
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_caregiver.HomeCareGiverActivity
import com.techwin.omatochi.ui.home_payer.HomePayerActivity
import com.techwin.omatochi.utils.SimpleSpanBuilder
import com.techwin.omatochi.utils.sheet.BaseCustomBottomSheet
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class LoginActivity : BaseActivity<ActivityLoginBinding>(), BaseCustomBottomSheet.Listener {

    private val viewModel: LoginActivityVM by viewModels()
    private lateinit var sheetChooseApp: BaseCustomBottomSheet<SheetChooseAppTypeBinding>

    companion object {
        fun newIntent(activity: Activity): Intent {
            val intent = Intent(activity, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_login
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnclick()
        initChooseAppSheet()
    }

    private fun initChooseAppSheet() {
        sheetChooseApp = BaseCustomBottomSheet(this, R.layout.sheet_choose_app_type, this)
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.tvForgot -> {
                    val intent = ForgotActivity.newIntent(this)
                    startActivity(intent)
                }
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnLogin -> {
                    if (binding.etEmail.text.toString() == "") {
                        showToast("Please enter email")
                    } else if (binding.etPassword.text.toString().length < 8) {
                        showToast("Password must be of 8 characters or more..")
                    } else {
                        if (this::sheetChooseApp.isInitialized) {
                            sheetChooseApp.show()
                        }
                    }
                }
                R.id.ivPassword -> {
                    changePasswordInputType(binding.etPassword, binding.ivPassword)
                }
            }
        }
    }

    private fun initView() {
        val ssb = SimpleSpanBuilder()
        ssb.appendWithSpace("Welcome to")
        ssb.append(
            "Omatochi",
            ForegroundColorSpan(ContextCompat.getColor(this, R.color.blue)),
            RelativeSizeSpan(1f)
        )
        binding.tveWelcome.text = ssb.build()
    }

    override fun onViewClick(view: View?) {
        if (this::sheetChooseApp.isInitialized && sheetChooseApp.isShowing) {
            sheetChooseApp.dismiss()
            when (view?.id) {
                R.id.tvCareCancel -> {
                }
                R.id.tvCareGiver -> {
                    val intent = HomeCareGiverActivity.newIntent(this)
                    startActivity(intent)
                }
                R.id.tvPayer -> {
                    val intent = HomePayerActivity.newIntent(this)
                    startActivity(intent)
                }
            }
        }
    }

    private fun changePasswordInputType(editText: EditText, imageView: ImageView) {
        if (editText.transformationMethod == PasswordTransformationMethod.getInstance()) {
            editText.transformationMethod = HideReturnsTransformationMethod.getInstance()
            editText.setSelection(editText.length())
            imageView.setImageResource(R.drawable.eye)
        } else {
            editText.transformationMethod = PasswordTransformationMethod.getInstance()
            editText.setSelection(editText.length())
            imageView.setImageResource(R.drawable.hide)
        }

    }

}