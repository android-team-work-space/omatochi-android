package com.techwin.omatochi.ui.home_caregiver.fragment.new_shift

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.elconfidencial.bubbleshowcase.BubbleShowCaseBuilder
import com.elconfidencial.bubbleshowcase.BubbleShowCaseSequence
import com.github.gcacace.signaturepad.views.SignaturePad
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.NewShiftModel
import com.techwin.omatochi.databinding.ClientQuestionnaireDialogBinding
import com.techwin.omatochi.databinding.IhhaQuestionnaireDialogBinding
import com.techwin.omatochi.databinding.NewShiftFragmentBinding
import com.techwin.omatochi.databinding.NewShiftItemBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_caregiver.HomeCareGiverActivity
import com.techwin.omatochi.ui.home_caregiver.profile.CareGiverProfile
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.dialog.BaseCustomDialog
import com.techwin.omatochi.utils.event.SingleLiveEvent
import com.techwin.omatochi.utils.sheet.BaseCustomBottomSheet
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NewShiftFragment : BaseFragment<NewShiftFragmentBinding>(), BaseCustomBottomSheet.Listener,
    BaseCustomDialog.Listener {
    private val viewModel: NewShiftFragmentVM by viewModels()
    private lateinit var homeCareGiverActivity: HomeCareGiverActivity
    private lateinit var bsNewShiftRVAdapter: RVAdapter<NewShiftModel, NewShiftItemBinding>
    private var popupIhha: BaseCustomDialog<IhhaQuestionnaireDialogBinding>? = null
    private var popupClient: BaseCustomDialog<ClientQuestionnaireDialogBinding>? = null

    companion object {
        val helpGuide: SingleLiveEvent<Boolean> = SingleLiveEvent()
    }

    override fun getLayoutResource(): Int {
        return R.layout.new_shift_fragment
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        initOnClick()
        initView()
    }

    private fun initView() {
        initAdapter()
        initDialog()
        initSign()
        initCalendarView()
    }

    private fun initOnClick() {
        homeCareGiverActivity = activity as HomeCareGiverActivity
        viewModel.onClick.observe(viewLifecycleOwner) {
            when (it!!.id) {
                R.id.ivMenu -> {
                    homeCareGiverActivity.openSideMenu()
                }
                R.id.ivProfile -> {
                    startActivity(Intent(requireContext(), CareGiverProfile::class.java))
                }
            }
        }
        helpGuide.value = false
        helpGuide.observe(this) {
            if (it!!) {
                getSequence().show()
            }
        }
    }

    private fun initAdapter() {
        bsNewShiftRVAdapter = RVAdapter(
            R.layout.new_shift_item,
            BR.bean,
            object : RVAdapter.Callback<NewShiftModel> {
                override fun onItemClick(v: View?, m: NewShiftModel) {
                    when (v?.id) {
                        R.id.ivLocation -> {
                            val intentUri = Uri.parse("geo:37.7749,-122.4194")
                            val mapIntent = Intent(Intent.ACTION_VIEW, intentUri)
                            mapIntent.setPackage("com.google.android.apps.maps")
                            startActivity(mapIntent)
                        }
                        R.id.btnClockIn -> {
                            popupIhha?.show()
                        }
                    }
                }
            })
        binding.rvNewShift.adapter = bsNewShiftRVAdapter
        bsNewShiftRVAdapter.list = AppUtils.getNewShiftList()
    }

    private fun getDialog1(): BubbleShowCaseBuilder {
        return BubbleShowCaseBuilder(homeCareGiverActivity)
            .description("Tap for more menu options")
            .targetView(binding.toolBar.ivMenu)
            .backgroundColor(Color.WHITE)
            .textColor(Color.BLACK)
            .descriptionTextSize(24)
    }

    private fun getDialog2(): BubbleShowCaseBuilder {
        return BubbleShowCaseBuilder(homeCareGiverActivity)
            .description("Slide down for full calendar access")
            .targetView(binding.view)
            .backgroundColor(Color.WHITE)
            .textColor(Color.BLACK)
            .descriptionTextSize(22)
    }

    private fun getSequence(): BubbleShowCaseSequence {
        return BubbleShowCaseSequence().addShowCases(
            listOf(
                getDialog1(),
                getDialog2(),
            )
        )
    }

    private fun initDialog() {
        popupIhha = BaseCustomDialog<IhhaQuestionnaireDialogBinding>(
            homeCareGiverActivity, R.layout.ihha_questionnaire_dialog, this
        )
        popupIhha!!.setCanceledOnTouchOutside(true)

        popupClient = BaseCustomDialog<ClientQuestionnaireDialogBinding>(
            homeCareGiverActivity, R.layout.client_questionnaire_dialog, this
        )
        popupClient!!.setCanceledOnTouchOutside(true)
    }

    private fun initSign() {
        popupClient!!.binding.signPad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() {
            }

            override fun onSigned() {
                popupClient!!.binding.ivClearClient.isEnabled = true
            }

            override fun onClear() {
                popupClient!!.binding.ivClearClient.isEnabled = false
            }
        })
        popupIhha!!.binding.signPad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() {
            }

            override fun onSigned() {
                popupIhha!!.binding.ivClearIhha.isEnabled = true
            }

            override fun onClear() {
                popupIhha!!.binding.ivClearIhha.isEnabled = false
            }
        })
    }

    @SuppressLint("SetTextI18n", "UseCompatLoadingForDrawables")
    private fun initCalendarView() {
        binding.toolBar.ivMenu.imageTintList =
            ContextCompat.getColorStateList(requireContext(), R.color.white)
        homeCareGiverActivity.window.statusBarColor =
            ContextCompat.getColor(homeCareGiverActivity, R.color.green)
        binding.tvMonth.text =
            AppUtils.getMonth(binding.calendarView.month + 1) + " " + binding.calendarView.year.toString()
        binding.calendarView.todayItemBackgroundDrawable =
            resources.getDrawable(R.drawable.white_radius)
        binding.calendarView.selectedItemBackgroundDrawable =
            resources.getDrawable(R.drawable.brown_radius)
        binding.calendarView.todayItemTextColor = resources.getColor(R.color.green)
        binding.calendarView.selectedItemTextColor = resources.getColor(R.color.white)

        binding.nsvNewShift.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (scrollY <= 0) { // down scroll
                if (!binding.calendarView.expanded) {
                    binding.calendarView.expand(500)
                }
            }
            if (scrollY >= 0) { // up scroll
                if (binding.calendarView.expanded) {
                    binding.calendarView.collapse(500)
                }
            }
        })
        binding.calendarView.setCalendarListener(object : CollapsibleCalendar.CalendarListener {
            override fun onClickListener() {}

            override fun onDataUpdate() {}

            override fun onDayChanged() {}

            override fun onDaySelect() {}

            override fun onItemClick(v: View) {}

            @SuppressLint("SetTextI18n")
            override fun onMonthChange() {
                binding.tvMonth.text =
                    (AppUtils.getMonth(binding.calendarView.month + 1) + " " + binding.calendarView.year.toString())
            }
            override fun onWeekChange(position: Int) {}
        })
    }

    override fun onViewClick(view: View?) {
        when (view?.id) {
            R.id.ivClearIhha -> {
                popupIhha!!.binding.signPad.clear()
            }
            R.id.ivClearClient -> {
                popupClient!!.binding.signPad.clear()
            }
            R.id.btnSubmit -> {
                popupIhha?.cancel()
                popupClient?.cancel()
                findNavController().navigate(R.id.newVisitFragment)
                homeCareGiverActivity.navigateNewVisit()
            }
            R.id.btnSubmitIhha -> {
                popupClient!!.show()
            }
        }
    }
}



