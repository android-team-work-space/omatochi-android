package com.techwin.omatochi.ui.home_caregiver.activity.new_activity

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AddNewActivityVM @Inject constructor() : BaseViewModel()
