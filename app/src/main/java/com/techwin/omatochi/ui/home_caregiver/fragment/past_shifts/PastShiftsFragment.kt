package com.techwin.omatochi.ui.home_caregiver.fragment.past_shifts

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.viewModels
import com.elconfidencial.bubbleshowcase.BubbleShowCaseBuilder
import com.elconfidencial.bubbleshowcase.BubbleShowCaseSequence
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.PastShiftModel
import com.techwin.omatochi.databinding.PastShiftFragmentBinding
import com.techwin.omatochi.databinding.PastShiftItemBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_caregiver.HomeCareGiverActivity
import com.techwin.omatochi.ui.home_caregiver.fragment.notes.NotesFragment
import com.techwin.omatochi.ui.home_caregiver.profile.CareGiverProfile
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.event.SingleLiveEvent
import com.techwin.omatochi.utils.sheet.BaseCustomBottomSheet
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PastShiftsFragment : BaseFragment<PastShiftFragmentBinding>(),
    BaseCustomBottomSheet.Listener {
    private val viewModel: PastShiftsFragmentVM by viewModels()
    private lateinit var mActivity: HomeCareGiverActivity
    private lateinit var bsPastShiftRVAdapter: RVAdapter<PastShiftModel, PastShiftItemBinding>

    companion object {
        var helpGuide: SingleLiveEvent<Boolean> = SingleLiveEvent()
    }

    override fun getLayoutResource(): Int {
        return R.layout.past_shift_fragment
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        mActivity = activity as HomeCareGiverActivity
        viewModel.onClick.observe(viewLifecycleOwner) {
            when (it!!.id) {
                R.id.ivMenu -> {
                    mActivity.openSideMenu()
                }
                R.id.ivProfile -> {
                    startActivity(
                        Intent(
                            context,
                            CareGiverProfile::class.java
                        )
                    )
                }
            }
        }
        initAdapter()
        initView()
        NotesFragment.helpGuide.value = false
        NotesFragment.helpGuide.observe(this) {
            if (it!!) {
                getSequence().show()
            }
        }
    }

    private fun initAdapter() {
        bsPastShiftRVAdapter = RVAdapter(
            R.layout.past_shift_item,
            BR.bean,
            object : RVAdapter.Callback<PastShiftModel> {
                override fun onItemClick(v: View?, m: PastShiftModel) {

                }

                override fun onPositionClick(v: View?, m: PastShiftModel, pos: Int) {

                }
            })
        binding.rvPastShift.adapter = bsPastShiftRVAdapter
        bsPastShiftRVAdapter.list = AppUtils.getPastShiftList()
    }

    @SuppressLint("SetTextI18n", "UseCompatLoadingForDrawables")
    private fun initView() {
        binding.tbPastShift.ivMenu.imageTintList =ContextCompat.getColorStateList(requireContext(),R.color.white)
        mActivity.window.statusBarColor =
            ContextCompat.getColor(mActivity, R.color.green)
        binding.tvMonth.text =
            AppUtils.getMonth(binding.calendarView.month + 1) + " " + binding.calendarView.year.toString()
        binding.calendarView.todayItemBackgroundDrawable =
            resources.getDrawable(R.drawable.white_radius)
        binding.calendarView.selectedItemBackgroundDrawable =
            resources.getDrawable(R.drawable.brown_radius)
        binding.calendarView.todayItemTextColor = resources.getColor(R.color.green)
        binding.calendarView.selectedItemTextColor = resources.getColor(R.color.white)

        binding.nsvPastShift.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (scrollY <= 0) {  // down scroll
                if (!binding.calendarView.expanded) {
                    binding.calendarView.expand(500)
                }
            }
            if (scrollY >= 0) { // up scroll
                if (binding.calendarView.expanded) {
                    binding.calendarView.collapse(500)
                }
            }
        })

        binding.calendarView.setCalendarListener(object : CollapsibleCalendar.CalendarListener {
            override fun onClickListener() {}

            override fun onDataUpdate() {}

            override fun onDayChanged() {}

            override fun onDaySelect() {}

            override fun onItemClick(v: View) {}

            override fun onMonthChange() {
                binding.tvMonth.text =
                    (AppUtils.getMonth(binding.calendarView.month + 1) + " " + binding.calendarView.year.toString())
            }

            override fun onWeekChange(position: Int) {}
        })
    }

    override fun onViewClick(view: View?) {}

    private fun getDialog1(): BubbleShowCaseBuilder {
        return BubbleShowCaseBuilder(mActivity)
            .description("Tap for more menu options")
            .targetView(binding.tbPastShift.ivMenu)
            .backgroundColor(Color.WHITE)
            .textColor(Color.BLACK)
            .descriptionTextSize(24)
    }

    private fun getDialog2(): BubbleShowCaseBuilder {
        return BubbleShowCaseBuilder(mActivity)
            .description("Slide down for full calendar access")
            .targetView(binding.view)
            .backgroundColor(Color.WHITE)
            .textColor(Color.BLACK)
            .descriptionTextSize(22)
    }

    private fun getDialog3(): BubbleShowCaseBuilder {
        return BubbleShowCaseBuilder(mActivity)
            .description("Scroll Vertically to browse through notes")
            .targetView(binding.rvPastShift[0])
            .backgroundColor(Color.WHITE)
            .textColor(Color.BLACK)
            .descriptionTextSize(22)
    }

    private fun getSequence(): BubbleShowCaseSequence {
        return BubbleShowCaseSequence().addShowCases(
            listOf(
                getDialog1(),
                getDialog2(),
                getDialog3(),
            )
        )
    }
}