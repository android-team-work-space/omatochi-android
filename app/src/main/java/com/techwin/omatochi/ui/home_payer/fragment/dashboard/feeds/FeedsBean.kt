package com.techwin.omatochi.ui.home_payer.fragment.dashboard.feeds

data class FeedsBean(
    var title: String,
    var selected : Boolean
)