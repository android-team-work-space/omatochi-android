package com.techwin.omatochi.ui.home_payer.comments

import android.content.Intent
import android.view.View
import androidx.activity.viewModels
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.AllCommentsModel
import com.techwin.omatochi.databinding.ActivityAllCommentsBinding
import com.techwin.omatochi.databinding.CustomAllCommentsItemBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_caregiver.new_user_chat.CareGiverNewChatActivity
import com.techwin.omatochi.utils.AppUtils

class AllCommentsActivity : BaseActivity<ActivityAllCommentsBinding>() {

    private val viewModel: AllCommentsActivityVM by viewModels()
    private lateinit var allCommentsAdapter: RVAdapter<AllCommentsModel, CustomAllCommentsItemBinding>

    override fun getLayoutResource(): Int {
        return R.layout.activity_all_comments
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
       initOnClick()

    }

    private fun initOnClick() {
        viewModel.onClick.observe(this){
            when(it?.id){
                R.id.ivBack -> {
                    finish()
                }
            }
        }
    }

    private fun initView() {
        binding.tbAllComments.tvTitle.text = getString(R.string.comments)
        initAllCommentsAdapter()
    }

    private fun initAllCommentsAdapter() {
        allCommentsAdapter = RVAdapter(
            R.layout.custom_all_comments_item,
            BR.bean,
            object : RVAdapter.Callback<AllCommentsModel> {
                override fun onItemClick(v: View?, m: AllCommentsModel) {
                    startNewActivity(Intent(this@AllCommentsActivity, CareGiverNewChatActivity::class.java))
                }
            })
        binding.rvAllComments.adapter = allCommentsAdapter
        allCommentsAdapter.list = AppUtils.getAllCommentsList()
    }

}