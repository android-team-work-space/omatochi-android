package com.techwin.omatochi.ui.home_caregiver.support_caregiver

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SupportActivityCGVM @Inject constructor() : BaseViewModel()