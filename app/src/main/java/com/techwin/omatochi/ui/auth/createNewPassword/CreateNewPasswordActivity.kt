package com.techwin.omatochi.ui.auth.createNewPassword

import android.app.Activity
import android.content.Intent
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.widget.EditText
import android.widget.ImageView
import androidx.activity.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityCreareNewPasswordBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_payer.HomePayerActivity
import com.techwin.omatochi.utils.showSuccessToast
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class CreateNewPasswordActivity : BaseActivity<ActivityCreareNewPasswordBinding>() {

    private val viewModel: CreateNewPasswordActivityVM by viewModels()

    companion object {
        fun newIntent(activity: Activity): Intent {
            val intent = Intent(activity, CreateNewPasswordActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_creare_new_password
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnclick()
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.ivNewPassword -> {
                    changePasswordInputType(binding.etNewPassword, binding.ivNewPassword)
                }
                R.id.ivConfirmPassword -> {
                    changePasswordInputType(binding.etConfirmPassword, binding.ivConfirmPassword)
                }
                R.id.btnSave -> {
                    when {
                        binding.etNewPassword.text.toString() == "" -> {
                            showToast("Please enter new password")
                        }
                        binding.etConfirmPassword.text.toString() == "" -> {
                            showToast("Please enter confirm password")
                        }
                        binding.etNewPassword.text.toString().length < 8 -> {
                            showToast("Password must be of 8 characters or more..")
                        }
                        binding.etConfirmPassword.text.toString().length < 8 -> {
                            showToast("Confirm Password must be of 8 characters or more..")
                        }
                        binding.etNewPassword.text.toString() != binding.etConfirmPassword.text.toString() -> {
                            showToast("Confirm password not matched")
                        }
                        else -> {
                            showSuccessToast("Password created successfully")
                            val intent = HomePayerActivity.newIntent(this)
                            startActivity(intent)
                        }
                    }
                }
            }
        }
    }

    private fun changePasswordInputType(editText: EditText, imageView: ImageView) {
        if (editText.transformationMethod === PasswordTransformationMethod.getInstance()) {
            editText.transformationMethod = HideReturnsTransformationMethod.getInstance()
            editText.setSelection(editText.length())
            imageView.setImageResource(R.drawable.eye)
        } else {
            editText.transformationMethod = PasswordTransformationMethod.getInstance()
            editText.setSelection(editText.length())
            imageView.setImageResource(R.drawable.hide)
        }
    }

    private fun initView() {
        binding.toolBar.tvTitle.text = getString(R.string.create_new_password)
    }

}