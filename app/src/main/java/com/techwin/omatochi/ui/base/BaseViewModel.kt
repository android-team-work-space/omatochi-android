package com.techwin.omatochi.ui.base

import android.view.View
import androidx.lifecycle.ViewModel
import com.techwin.omatochi.utils.event.SingleLiveEvent

open class BaseViewModel : ViewModel() {

    val onClick: SingleLiveEvent<View> = SingleLiveEvent()

    override fun onCleared() {
        super.onCleared()
    }

    open fun onClick(view: View?) {
        onClick.value = view
    }
}