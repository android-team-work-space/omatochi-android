package com.techwin.omatochi.ui.home_payer.account.profile

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.view.View
import android.widget.DatePicker
import android.widget.PopupMenu
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityProfileBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.utils.Constants
import com.techwin.omatochi.utils.permission.Permission
import com.techwin.omatochi.utils.permission.PermissionHandler
import com.techwin.omatochi.utils.showErrorToast
import com.techwin.omatochi.utils.showInfoToast
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class ProfileActivity : BaseActivity<ActivityProfileBinding>(), DatePickerDialog.OnDateSetListener {
    private val viewModel: ProfileActivityVM by viewModels()
    private lateinit var popupGender: PopupMenu

    companion object {
        fun newIntent(activity: Activity): Intent {
            val intent = Intent(activity, ProfileActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_profile
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initPlaceSearch()
        initOnclick()
    }

    private fun initPlaceSearch() {
        Places.initialize(this, Constants.GOOGLE_PLACE_KEY)
    }


    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.etGender -> {
                    initGenderPopup(binding.etGender)
                }
                R.id.etDateOfBirth -> {
                    openDatePicker()
                }
                R.id.etRelationship -> {
                    initRelationShipPopup(binding.etRelationship)
                }
                R.id.etAddress -> {
                    openPlaceSearchFragment()
                }
                R.id.ivEditProfile -> {
                    getLocalImage()
                }
                R.id.btnSave -> {
                    showInfoToast("Saved")
                }
            }
        }
    }

    private fun getLocalImage() {
        Permission.check(
            this,
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            ),
            0,
            Permission.Options(),
            object : PermissionHandler() {
                override fun onGranted() {
                    ImagePicker.with(this@ProfileActivity)
                        .crop()
                        .start()
                }
            })
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            val uri = data.data
            binding.ivImage.setImageURI(uri)
        }
    }

    private var resultPlaceSearch = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        try {
            if (result.resultCode == Activity.RESULT_OK) {
                val place = Autocomplete.getPlaceFromIntent(result.data!!)
                binding.etAddress.setText(place.address)
            }
        } catch (e: Exception) {
            showErrorToast("Place result Exception : " + e.message)
        }
    }

    private fun openPlaceSearchFragment() {
        val fields = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS)
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
            .build(this)
        resultPlaceSearch.launch(intent)
    }

    private fun initRelationShipPopup(view: View) {
        popupGender = PopupMenu(this, view)
        popupGender.inflate(R.menu.popup_select_relationship)
        popupGender.setOnMenuItemClickListener { menuItem ->
            binding.etRelationship.setText(menuItem.title.toString())
            binding.otherRelationshipSelected =
                menuItem.title.toString().lowercase() == getString(R.string.other)
            if (menuItem.title.toString() == "Other"){
                binding.tvSpecialisationOther.visibility = View.VISIBLE
                binding.etSpecialisationOther.visibility = View.VISIBLE
            }else{
                binding.tvSpecialisationOther.visibility = View.GONE
                binding.etSpecialisationOther.visibility = View.GONE
            }
            true
        }
        popupGender.show()
    }

    private fun openDatePicker() {
        val myCalendar = Calendar.getInstance()
        val year = myCalendar.get(Calendar.YEAR)
        val month = myCalendar.get(Calendar.MONTH)
        val day = myCalendar.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(this, this, year, month, day)
        datePickerDialog.show()
    }

    private fun initGenderPopup(view: View) {
        popupGender = PopupMenu(this, view)
        popupGender.inflate(R.menu.popup_select_gender)
        popupGender.setOnMenuItemClickListener { menuItem ->
            binding.etGender.setText(menuItem.title.toString())

            true
        }
        popupGender.show()
    }

    private fun initView() {
        binding.otherRelationshipSelected = false
        window.statusBarColor = ContextCompat.getColor(this, R.color.blue)
        binding.toolBar.tvTitle.text = getString(R.string.edit_profile)
        binding.toolBar.ivBack.setColorFilter(ContextCompat.getColor(this, R.color.white))
        binding.toolBar.tvTitle.setTextColor(ContextCompat.getColor(this, R.color.white))
    }

    @SuppressLint("SetTextI18n")
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        binding.etDateOfBirth.setText("$dayOfMonth/$month/$year")
    }
}