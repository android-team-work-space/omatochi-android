package com.techwin.omatochi.ui.home_payer.health_records.medication_list.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.InActiveMedicationModel
import com.techwin.omatochi.databinding.CustomAllCardsBinding
import com.techwin.omatochi.databinding.CustomInactiveMedicationItemBinding
import com.techwin.omatochi.ui.home_payer.account.payments_invoice.CreditCardBean
import com.zerobranch.layout.SwipeLayout


class CustomInActiveMedicationAdapter(val callback: CardCallback) :
    RecyclerView.Adapter<CustomInActiveMedicationAdapter.RequestHolder>() {

    private var moreBeans: List<InActiveMedicationModel> = ArrayList()
    var swipeLayout: SwipeLayout? = null

    interface CardCallback {
        fun onItemClick(v: View?, m: InActiveMedicationModel?, pos: Int?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RequestHolder {
        val layoutTodoListBinding: CustomInactiveMedicationItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.custom_inactive_medication_item, parent, false
        )
        layoutTodoListBinding.setVariable(BR.callback, callback)
        return RequestHolder(layoutTodoListBinding)
    }

    override fun onBindViewHolder(
        holder: RequestHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        holder.layoutTodoListBinding.bean = moreBeans[position]
        holder.layoutTodoListBinding.pos = position
        holder.layoutTodoListBinding.swipeLayout.setOnActionsListener(object :
            SwipeLayout.SwipeActionsListener {
            override fun onOpen(direction: Int, isContinuous: Boolean) {
                if (direction == SwipeLayout.LEFT) {
                    if (swipeLayout != null && swipeLayout != holder.layoutTodoListBinding.swipeLayout) {
                        swipeLayout?.close(true)
                    }
                    swipeLayout = holder.layoutTodoListBinding.swipeLayout
                }
            }

            override fun onClose() {
                // the main view has returned to the default state
                if (swipeLayout == holder.layoutTodoListBinding.swipeLayout) swipeLayout = null
            }
        })
    }

    override fun getItemCount(): Int {
        return moreBeans.size
    }

    fun setList(moreBeans: List<InActiveMedicationModel>) {
        this.moreBeans = moreBeans
        notifyDataSetChanged()
    }

    fun clear() {
        moreBeans.isEmpty()
        notifyDataSetChanged()
    }

    fun getList(): List<InActiveMedicationModel> {
        return moreBeans
    }

    class RequestHolder(val layoutTodoListBinding: CustomInactiveMedicationItemBinding) :
        RecyclerView.ViewHolder(layoutTodoListBinding.root)
}