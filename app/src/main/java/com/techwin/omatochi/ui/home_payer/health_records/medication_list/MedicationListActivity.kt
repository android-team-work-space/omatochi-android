package com.techwin.omatochi.ui.home_payer.health_records.medication_list

import android.content.Intent
import androidx.activity.viewModels
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityMedicationListBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_payer.health_records.medication_list.add_medication.AddMedicationActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MedicationListActivity : BaseActivity<ActivityMedicationListBinding>() {
    private val viewModel: MedicationListActivityVM by viewModels()

    override fun getLayoutResource(): Int {
        return R.layout.activity_medication_list
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.ivAddNewMedication -> {
                    //throw RuntimeException("Test Crash") // Force a crash just for testing need to remove this line after test
                    startNewActivity(Intent(this, AddMedicationActivity::class.java))
                }
                R.id.ivShareMedicationPdf -> {
                    val sendIntent = Intent(Intent.ACTION_SEND)
                    sendIntent.type = "text/plain"
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to test.")
                    startActivity(sendIntent)
                }
            }
        }
    }

    private fun initView() {
        binding.tbMedicationList.tvTitle.text = getString(R.string.medication_list)
        initViewPager()
    }

    private fun initViewPager() {
        binding.tlMedicationList.removeAllTabs()
        binding.tlMedicationList.addTab(
            binding.tlMedicationList.newTab().setText(getString(R.string.active_medication))
        )
        binding.tlMedicationList.addTab(
            binding.tlMedicationList.newTab().setText(getString(R.string.inactive_medication))
        )
        binding.tlMedicationList.tabGravity = TabLayout.GRAVITY_FILL
        binding.tlMedicationList.isSmoothScrollingEnabled = true
        binding.vpMedicationList.adapter = MedicationListViewPager(this)
        binding.vpMedicationList.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        binding.vpMedicationList.isUserInputEnabled = false
        // binding.tabLayout.addOnTabSelectedListener()


        binding.tlMedicationList.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                binding.tlMedicationList.setScrollPosition(tab.position, 0f, true)
                binding.vpMedicationList.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

    }
}