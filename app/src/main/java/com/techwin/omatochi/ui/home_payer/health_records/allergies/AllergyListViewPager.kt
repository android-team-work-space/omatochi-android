package com.techwin.omatochi.ui.home_payer.health_records.allergies

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.techwin.omatochi.ui.home_payer.health_records.allergies.fragments.ActiveAllergyFragment
import com.techwin.omatochi.ui.home_payer.health_records.allergies.fragments.InActiveAllergiesFragment
import com.techwin.omatochi.ui.home_payer.health_records.medication_list.fragment.ActiveMedicationFragment
import com.techwin.omatochi.ui.home_payer.health_records.medication_list.fragment.InActiveMedicationFragment

class AllergyListViewPager(fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> {
                return ActiveAllergyFragment.newIntent()
            }
            1 -> {
                return InActiveAllergiesFragment.newIntent()
            }
            else -> {
                return InActiveAllergiesFragment.newIntent()
            }
        }
    }
}
