package com.techwin.omatochi.ui.home_payer.fragment.contacts

data class ContactBean(
    var title: String,
    var image: Int,
    var check: Boolean
)