package com.techwin.omatochi.ui.home_caregiver.fragment.new_visits

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NewVisitsFragmentVM @Inject constructor(): BaseViewModel() {
}