package com.techwin.omatochi.ui.home_payer.about.terms_and_conditions

import android.os.Handler
import android.os.Looper
import androidx.activity.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityTermsAndConditionsBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.utils.Constants.Companion.TERMS_CONDITIONS_URL
import com.techwin.omatochi.utils.RetrivePDFfromUrl
import dagger.hilt.android.AndroidEntryPoint
import io.ak1.pix.helpers.hide
import io.ak1.pix.helpers.show

@AndroidEntryPoint
class TermsAndConditionsActivity : BaseActivity<ActivityTermsAndConditionsBinding>() {

    private val viewModel: TermsAndConditionsActivityVM by viewModels()

    override fun getLayoutResource(): Int {
        return R.layout.activity_terms_and_conditions
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
            }
        }
    }

    private fun initView() {
        binding.tbTermsAndConditions.tvTitle.text = getString(R.string.terms_and_conditions)
        binding.pbTermsAndConditions.show()
        loadTermsAndConditionsUrl()
    }

    private fun loadTermsAndConditionsUrl() {
        val retrivePDFfromUrl = RetrivePDFfromUrl(binding.idPDFView)
        retrivePDFfromUrl.execute(TERMS_CONDITIONS_URL)
        Handler(Looper.myLooper()!!).postDelayed({
            binding.pbTermsAndConditions.hide()
        }, 1500)
    }
}