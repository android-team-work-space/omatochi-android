package com.techwin.omatochi.ui.base.binding

import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.skyhope.showmoretextview.ShowMoreTextView
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.HolderSideMenu2Binding
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_payer.HomePayerActivity
import com.techwin.omatochi.utils.Resource

object TextViewBinding {

    @JvmStatic
    @BindingAdapter("setShowMoreText")
    fun setShowMoreText(textView: ShowMoreTextView, textInt: Int) {
        textView.setText(R.string.feeds_long_text)
        textView.setShowingLine(2)
        textView.addShowMoreText("more")
        textView.addShowLessText("less")
        textView.setShowMoreColor(ContextCompat.getColor(textView.context, R.color.blue))
        textView.setShowLessTextColor(ContextCompat.getColor(textView.context, R.color.red3))
    }

    @JvmStatic
    @BindingAdapter("setMenuRecyclerView")
    fun setMenuRecyclerView(recyclerView: RecyclerView, list: List<String>) {
        val adapterMenu: RVAdapter<String, HolderSideMenu2Binding> =
            RVAdapter(R.layout.holder_side_menu2, BR.bean, object : RVAdapter.Callback<String> {
                override fun onItemClick(v: View?, m: String) {

                }

                override fun onPositionClick(v: View?, m: String, pos: Int) {
                    HomePayerActivity.menuSubItemClick.value = Resource.success(m)
                }
            })
        recyclerView.adapter = adapterMenu
        adapterMenu.list = list
    }
}