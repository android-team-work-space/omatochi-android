package com.techwin.omatochi.ui.home_payer.fragment.feedback

import android.annotation.SuppressLint
import android.content.Intent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.CustomNotesHistoryItemBinding
import com.techwin.omatochi.databinding.FragmentFeedbackBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_payer.HomePayerActivity
import com.techwin.omatochi.ui.home_payer.account.profile.ProfileActivity
import com.techwin.omatochi.ui.home_payer.new_note.AddNewNoteActivity
import com.techwin.omatochi.ui.home_payer.new_note.NoteHistoryModel
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.AppUtils.Companion.getMonth
import com.techwin.omatochi.utils.sheet.BaseCustomBottomSheet
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FeedbackFragment : BaseFragment<FragmentFeedbackBinding>(), BaseCustomBottomSheet.Listener {
    private lateinit var homePayerActivity: HomePayerActivity
    private lateinit var bsNotesHistoryRVAdapter: RVAdapter<NoteHistoryModel, CustomNotesHistoryItemBinding>
    private val viewModel: FeedbackFragmentVM by viewModels()

    companion object {
        fun newIntent(): Fragment {
            return FeedbackFragment()
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_feedback
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        homePayerActivity = activity as HomePayerActivity

        homePayerActivity.window.statusBarColor =
            ContextCompat.getColor(homePayerActivity, R.color.blue)
        initOnclick()
        initView()
        initAdapter()
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivAddNewNoteNotes -> {
                    startActivity(
                        Intent(
                            context,
                            AddNewNoteActivity::class.java
                        )
                    )
                }
                R.id.ivMenu -> {
                    homePayerActivity.openSideMenu()
                }
                R.id.ivProfile -> {
                    homePayerActivity.startNewActivity(
                        Intent(
                            requireContext(),
                            ProfileActivity::class.java
                        )
                    )
                }
            }
        }
    }

    private fun initAdapter() {
        bsNotesHistoryRVAdapter =
            RVAdapter(
                R.layout.custom_notes_history_item,
                BR.bean,
                object : RVAdapter.Callback<NoteHistoryModel> {
                    override fun onItemClick(v: View?, m: NoteHistoryModel) {
                    }

                    override fun onPositionClick(v: View?, m: NoteHistoryModel, pos: Int) {

                    }
                })
        binding.rvNotesHistoryfeed.adapter = bsNotesHistoryRVAdapter
        bsNotesHistoryRVAdapter.list = AppUtils.getNoteHistoryList()
    }


    @SuppressLint("SetTextI18n", "UseCompatLoadingForDrawables")
    private fun initView() {
        binding.tbNotes.tvTitle.visibility = View.GONE
        binding.tvMonth.text =
            (getMonth(binding.calendarView.month + 1) + " " + binding.calendarView.year.toString())
        binding.calendarView.todayItemBackgroundDrawable =
            resources.getDrawable(R.drawable.white_radius)
        binding.calendarView.selectedItemBackgroundDrawable =
            resources.getDrawable(R.drawable.brown_radius)
        binding.calendarView.todayItemTextColor =
            ContextCompat.getColor(requireContext(), R.color.blue)
        binding.calendarView.selectedItemTextColor =
            ContextCompat.getColor(requireContext(), R.color.white)


        binding.nsvFeedback.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (scrollY <= 0) {  // down scroll
                if (!binding.calendarView.expanded) {
                    binding.calendarView.expand(500)
                }
            }
            if (scrollY >= 0) { // up scroll
                if (binding.calendarView.expanded) {
                    binding.calendarView.collapse(500)
                }
            }
        })

        binding.calendarView.setCalendarListener(object : CollapsibleCalendar.CalendarListener {
            override fun onClickListener() {}

            override fun onDataUpdate() {}

            override fun onDayChanged() {}

            override fun onDaySelect() {}

            override fun onItemClick(v: View) {}

            @SuppressLint("SetTextI18n")
            override fun onMonthChange() {
                binding.tvMonth.text =
                    (getMonth(binding.calendarView.month + 1) + " " + binding.calendarView.year.toString())
            }

            override fun onWeekChange(position: Int) {}
        })
    }

    override fun onViewClick(view: View?) {

    }
}