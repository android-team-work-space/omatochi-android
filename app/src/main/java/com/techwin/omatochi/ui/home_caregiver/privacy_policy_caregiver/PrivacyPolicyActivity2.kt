package com.techwin.omatochi.ui.home_caregiver.privacy_policy_caregiver

import android.os.Looper
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityPrivacyPolicy2Binding
import com.techwin.omatochi.databinding.ActivityPrivacyPolicyBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_payer.support.PrivacyPolciyActivityVM
import com.techwin.omatochi.utils.Constants
import com.techwin.omatochi.utils.RetrivePDFfromUrl
import dagger.hilt.android.AndroidEntryPoint
import io.ak1.pix.helpers.hide
import io.ak1.pix.helpers.show

@AndroidEntryPoint
class PrivacyPolicyActivity2 : BaseActivity<ActivityPrivacyPolicy2Binding>() {

    private val viewModel: PrivacyPolciyActivity2VM by viewModels()

    override fun getLayoutResource(): Int {
        return R.layout.activity_privacy_policy_2
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
            }
        }
    }

    private fun initView() {
        binding.tbPrivacyPolicy.tvTitle.text = getString(R.string.privacy_policy)
        binding.pbPrivacyPolicy2.show()
        loadPrivacyPolicyCAreGiverUrl()
    }

    private fun loadPrivacyPolicyCAreGiverUrl() {
        android.os.Handler(Looper.myLooper()!!).postDelayed({
            binding.pbPrivacyPolicy2.hide()
        },1500)
        val retrievePDFFromUrl = RetrivePDFfromUrl(binding.idPDFView)
        retrievePDFFromUrl.execute(Constants.PRIVACY_POLICY_URL)
    }
}