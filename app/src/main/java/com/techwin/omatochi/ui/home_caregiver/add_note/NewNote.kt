package com.techwin.omatochi.ui.home_caregiver.add_note
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.widget.DatePicker
import androidx.activity.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.NewNoteBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class NewNote : BaseActivity<NewNoteBinding>(), DatePickerDialog.OnDateSetListener {
    private val viewModel: NewNoteVM by viewModels()

    override fun getLayoutResource(): Int {
        return R.layout.new_note
    }

    override fun getViewModel(): BaseViewModel {
      return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this){
            when(it?.id){
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnAddNewNote -> {
                    showToast("Will be saved")
                }
                R.id.etDateTime->{
                    openDatePicker()
                }
            }
        }
    }

    private fun initView(){
        binding.tbAddNote.tvTitle.text = getString(R.string.add_note)
    }

    private fun openDatePicker() {
        val myCalendar = Calendar.getInstance()
        val year = myCalendar.get(Calendar.YEAR)
        val month = myCalendar.get(Calendar.MONTH)
        val day = myCalendar.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(this, this, year, month, day)
        datePickerDialog.show()
    }


    @SuppressLint("SetTextI18n")
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        binding.etDateTime.setText("$dayOfMonth/$month/$year")
    }

}