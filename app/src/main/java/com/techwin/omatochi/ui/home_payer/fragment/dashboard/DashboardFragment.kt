package com.techwin.omatochi.ui.home_payer.fragment.dashboard

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.FragmentDashboardBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_payer.HomePayerActivity
import com.techwin.omatochi.ui.home_payer.fragment.dashboard.dashboard2.Dashboard2Fragment
import com.techwin.omatochi.ui.home_payer.account.profile.ProfileActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardFragment : BaseFragment<FragmentDashboardBinding>() {

    private val viewModel: DashboardFragmentVM by viewModels()
    private lateinit var homePayerActivity: HomePayerActivity

    companion object {
        var callBackLiveData = MutableLiveData<Boolean>()
        fun newIntent(): Fragment {
            return DashboardFragment()
        }
    }

    override fun onPause() {
        super.onPause()
        Log.e("Dashboard","onPauseParent")
        if (Dashboard2Fragment.isLive){
            callBackLiveData.value = true
        }

    }

    override fun onStop() {
        super.onStop()
        if (Dashboard2Fragment.isLive){
            callBackLiveData.value = true
        }
        Log.e("Dashboard","onStopParent")
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_dashboard
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        initView()
        initViewPager()
        initOnclick()
    }

    private fun initViewPager() {
        binding.tabLayout.removeAllTabs()
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(getString(R.string.feed)))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(getString(R.string.dashboard)))
        binding.tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        binding.tabLayout.isSmoothScrollingEnabled = true
        binding.viewPager2.adapter = DashboardViewPager(homePayerActivity)
        binding.viewPager2.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        binding.viewPager2.isUserInputEnabled = false
        // binding.tabLayout.addOnTabSelectedListener()

        binding.tabLayout.setOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                binding.tabLayout.setScrollPosition(tab.position, 0f, true)
                binding.viewPager2.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivMenu -> {
                    homePayerActivity.openSideMenu()
                }
                R.id.ivProfile -> {
                    homePayerActivity.startNewActivity(Intent(requireContext(), ProfileActivity::class.java))
                }
            }
        }
    }

    private fun initView() {
        binding.toolBar.tvTitle.visibility = View.GONE
        homePayerActivity = activity as HomePayerActivity
        homePayerActivity.window.statusBarColor =
            ContextCompat.getColor(homePayerActivity, R.color.white)
    }
}