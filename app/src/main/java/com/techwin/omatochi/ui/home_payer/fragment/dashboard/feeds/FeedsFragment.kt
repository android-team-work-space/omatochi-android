package com.techwin.omatochi.ui.home_payer.fragment.dashboard.feeds

import android.content.Intent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.FragmentFeedsBinding
import com.techwin.omatochi.databinding.HolderFeedBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_payer.HomePayerActivity
import com.techwin.omatochi.ui.home_payer.VideoPlayerActivity
import com.techwin.omatochi.ui.home_payer.comments.AllCommentsActivity
import com.techwin.omatochi.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FeedsFragment : BaseFragment<FragmentFeedsBinding>() {
    private val viewModel: FeedsFragmentVM by viewModels()
    private lateinit var homePayerActivity: HomePayerActivity
    private lateinit var adapterFeeds: RVAdapter<FeedsBean, HolderFeedBinding>

    companion object {
        fun newIntent(): Fragment {
            return FeedsFragment()
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_feeds
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        initView()
        initOnclick()
        initAdapter()
    }

    private fun initAdapter() {
        adapterFeeds =
            RVAdapter(R.layout.holder_feed, BR.bean, object : RVAdapter.Callback<FeedsBean> {
                override fun onItemClick(v: View?, m: FeedsBean) {
                }

                override fun onPositionClick(v: View?, m: FeedsBean, pos: Int) {
                    when (v?.id) {
                        R.id.tvViewAllComments -> {
                            startActivity(Intent(context, AllCommentsActivity::class.java))
                        }
                        R.id.ivPlayVideo -> {
                            startActivity(Intent(context, VideoPlayerActivity::class.java))
                        }
                    }
                }
            })
        binding.rvFeeds.adapter = adapterFeeds
        adapterFeeds.list = AppUtils.getFeedList()
    }


    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivMenu -> {
                    homePayerActivity.openSideMenu()
                }
            }
        }
    }

    private fun initView() {
        homePayerActivity = activity as HomePayerActivity
        homePayerActivity.window.statusBarColor =
            ContextCompat.getColor(homePayerActivity, R.color.white)
    }

}