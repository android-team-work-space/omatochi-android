package com.techwin.omatochi.ui.home_payer.new_user_chat

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.CustomDummyChatItemBinding
import com.techwin.omatochi.ui.home_caregiver.new_user_chat.CustomDummyChatCareGiverAdapter
import com.techwin.omatochi.ui.home_payer.account.payments_invoice.CreditCardBean

class CustomDummyChatAdapter(val callback: ChatItemCallback) :
    RecyclerView.Adapter<CustomDummyChatAdapter.RequestHolder>() {

    private var moreBeans = ArrayList<String>()

    interface ChatItemCallback {
        fun onItemClick(v: View?, m: String?, pos: Int?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RequestHolder {
        val layoutTodoListBinding: CustomDummyChatItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.custom_dummy_chat_item, parent, false
        )
        layoutTodoListBinding.setVariable(BR.callback, callback)
        return RequestHolder(layoutTodoListBinding)
    }

    override fun onBindViewHolder(
        holder: RequestHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        holder.layoutTodoListBinding.bean = moreBeans[position]
        holder.layoutTodoListBinding.pos = position
        if (position == 0){
           holder.layoutTodoListBinding.cvHowAreYouChat.visibility = View.VISIBLE
           holder.layoutTodoListBinding.cvHowAreYouChatImage.visibility = View.GONE
           holder.layoutTodoListBinding.cvSender.visibility = View.GONE
           holder.layoutTodoListBinding.cvReceiver.visibility = View.GONE
        }
        else if (position == 3){
            holder.layoutTodoListBinding.cvHowAreYouChat.visibility = View.GONE
            holder.layoutTodoListBinding.cvHowAreYouChatImage.visibility = View.VISIBLE
            holder.layoutTodoListBinding.cvReceiver.visibility = View.GONE
            holder.layoutTodoListBinding.cvSender.visibility = View.GONE
        }
        else if (position % 2 != 0 && position != 3) {
            holder.layoutTodoListBinding.cvHowAreYouChat.visibility = View.GONE
            holder.layoutTodoListBinding.cvSender.visibility = View.VISIBLE
            holder.layoutTodoListBinding.cvReceiver.visibility = View.GONE
            holder.layoutTodoListBinding.cvHowAreYouChatImage.visibility = View.GONE
            holder.layoutTodoListBinding.tvSenderMsg.text = moreBeans[position]
        }else if (position % 2 == 0 && position != 0){
            holder.layoutTodoListBinding.tvMsgReceiver.text = moreBeans[position]
            holder.layoutTodoListBinding.cvHowAreYouChat.visibility = View.GONE
            holder.layoutTodoListBinding.cvHowAreYouChatImage.visibility = View.GONE
            holder.layoutTodoListBinding.cvSender.visibility = View.GONE
            holder.layoutTodoListBinding.cvReceiver.visibility = View.VISIBLE
        }
    }

    override fun getItemCount(): Int {
        return moreBeans.size
    }

    fun setList(moreBeans: ArrayList<String>) {
        this.moreBeans = moreBeans
        notifyDataSetChanged()
    }

    fun upDateList(upDatedList: ArrayList<String>) {
        this.moreBeans.addAll(upDatedList)
        notifyDataSetChanged()
    }

    fun addItemTOList(item: String) {
        this.moreBeans.add(item)
        notifyDataSetChanged()
    }

    fun clear() {
        moreBeans.isEmpty()
        notifyDataSetChanged()
    }

    fun getList(): List<String> {
        return moreBeans
    }

    class RequestHolder(val layoutTodoListBinding: CustomDummyChatItemBinding) :
        RecyclerView.ViewHolder(layoutTodoListBinding.root)

}