package com.techwin.omatochi.ui.home_payer.health_records.allergies.fragments

import androidx.fragment.app.Fragment
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.AllergyModel
import com.techwin.omatochi.data.model.InActiveMedicationModel
import com.techwin.omatochi.databinding.FragmentActiveAllergyBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_payer.health_records.allergies.AllergyListActivity
import com.techwin.omatochi.ui.home_payer.health_records.allergies.adapters.CustomActiveAllergyAdapter
import com.techwin.omatochi.ui.home_payer.health_records.allergies.vm.ActiveAllergyFragmentVM
import com.techwin.omatochi.ui.home_payer.health_records.medication_list.adapters.CustomInActiveMedicationAdapter
import com.techwin.omatochi.utils.AppUtils
import com.techwin.omatochi.utils.showToast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ActiveAllergyFragment : BaseFragment<FragmentActiveAllergyBinding>() {

    private val viewModel: ActiveAllergyFragmentVM by viewModels()
    private lateinit var allergyListActivity: AllergyListActivity
    private lateinit var customActiveAllergyAdapter: CustomActiveAllergyAdapter

    companion object {
        fun newIntent(): Fragment {
            return ActiveAllergyFragment()
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_active_allergy
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        initView()
        initOnclick()
    }


    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {

            }
        }
    }

    private fun initView() {
        allergyListActivity = activity as AllergyListActivity
        allergyListActivity.window.statusBarColor =
            ContextCompat.getColor(allergyListActivity, R.color.white)

        initAdapter()
    }

    private fun initAdapter() {
        customActiveAllergyAdapter =
            CustomActiveAllergyAdapter(object : CustomActiveAllergyAdapter.CardCallback {
                override fun onItemClick(v: View?, m: AllergyModel?, pos: Int?) {
                    when (v?.id) {
                        R.id.layEdit -> {

                        }
                        R.id.layInActive -> {
                            showToast("Added to InActive Allergies")
                            customActiveAllergyAdapter.swipeLayout?.close()
                        }
                        R.id.ivDownActiveAllergy,R.id.clActiveAllergy -> {
                            m?.expended = !m!!.expended
                            customActiveAllergyAdapter.notifyItemChanged(pos!!)
                        }
                    }
                }
            })
        customActiveAllergyAdapter.setList(AppUtils.getInActiveAllergyList())
        binding.rvActiveAllergy.adapter = customActiveAllergyAdapter
    }

}