package com.techwin.omatochi.ui.home_payer.health_records.exercise

import android.content.Intent
import android.view.View
import androidx.activity.viewModels
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityExerciseListBinding
import com.techwin.omatochi.databinding.CustomExerciseItemBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_payer.VideoPlayerActivity
import com.techwin.omatochi.ui.home_payer.fragment.dashboard.feeds.FeedsBean
import com.techwin.omatochi.utils.AppUtils

class ExerciseListActivity : BaseActivity<ActivityExerciseListBinding>() {

    private val viewModel: ExerciseListActivityVM by viewModels()
    private lateinit var adapterExercise: RVAdapter<FeedsBean, CustomExerciseItemBinding>

    override fun getLayoutResource(): Int {
        return R.layout.activity_exercise_list
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
        initAdapter()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.ivAddExercise -> {
                    startActivity(Intent(this,AddExerciseActivity::class.java))
                }
            }
        }
    }

    private fun initView() {
        binding.tbExercise.tvTitle.text = getString(R.string.exercise_list)
    }

    private fun initAdapter() {
        adapterExercise =
            RVAdapter(
                R.layout.custom_exercise_item,
                BR.bean,
                object : RVAdapter.Callback<FeedsBean> {
                    override fun onItemClick(v: View?, m: FeedsBean) {
                    }

                    override fun onPositionClick(v: View?, m: FeedsBean, pos: Int) {
                        startActivity(Intent(this@ExerciseListActivity,VideoPlayerActivity::class.java))
                    }
                })
        binding.rvFeeds.adapter = adapterExercise
        adapterExercise.list = AppUtils.getFeedList()
    }
}