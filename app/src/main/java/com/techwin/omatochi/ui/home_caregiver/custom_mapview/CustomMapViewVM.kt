package com.techwin.omatochi.ui.home_caregiver.custom_mapview
import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CustomMapViewVM @Inject constructor() :BaseViewModel()
