package com.techwin.omatochi.ui.home_caregiver.fragment.messages

import android.content.Intent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.InboxMessagesModelCG
import com.techwin.omatochi.data.model.MessageNotificationsModelCG
import com.techwin.omatochi.databinding.*
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_caregiver.HomeCareGiverActivity
import com.techwin.omatochi.ui.home_caregiver.new_user_chat.CareGiverNewChatActivity
import com.techwin.omatochi.ui.home_caregiver.search.CareGiverSearchActivity
import com.techwin.omatochi.utils.AppUtils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MessagesFragment : BaseFragment<MessagesFargmentBinding>() {
    private val viewModel: MessagesFragmentVM by viewModels()
    private lateinit var mActivity: HomeCareGiverActivity
    private lateinit var notificationAdapter: RVAdapter<MessageNotificationsModelCG, CustomNotificationItem2Binding>
    private lateinit var inboxMessageAdapter: RVAdapter<InboxMessagesModelCG, CustomInboxMessagesItem2Binding>

    override fun getLayoutResource(): Int {
        return R.layout.messages_fargment
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView(view: View) {
        mActivity = activity as (HomeCareGiverActivity)

        mActivity.window.statusBarColor = ContextCompat.getColor(requireContext(),R.color.white)
        viewModel.onClick.observe(viewLifecycleOwner){
            when (it!!.id) {
                R.id.ivMenu -> {
                    mActivity.openSideMenu()
                }
                R.id.tvNewChat -> {
                    startActivity(Intent(context, CareGiverSearchActivity::class.java))
                }
            }
        }

        initNotificationAdapter()
        initInboxMessagesAdapter()
    }

    private fun initInboxMessagesAdapter() {
        inboxMessageAdapter = RVAdapter(
            R.layout.custom_inbox_messages_item_2,
            BR.bean,
            object : RVAdapter.Callback<InboxMessagesModelCG> {
                override fun onItemClick(v: View?, m: InboxMessagesModelCG) {
                    startActivity(Intent(context, CareGiverNewChatActivity::class.java))
                }

            })
        binding.rvInboxMessages.adapter = inboxMessageAdapter
        inboxMessageAdapter.list = AppUtils.getInboxMessagesList2()
    }

    private fun initNotificationAdapter() {
        notificationAdapter = RVAdapter(
            R.layout.custom_notification_item_2,
            BR.bean,
            object : RVAdapter.Callback<MessageNotificationsModelCG> {
                override fun onItemClick(v: View?, m: MessageNotificationsModelCG) {

                }

            })
        binding.rvNotifications.adapter = notificationAdapter
        notificationAdapter.list = AppUtils.getNotificationList2()
    }
}