package com.techwin.omatochi.ui.home_payer.addContact

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityAddContactBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class AddContactActivity : BaseActivity<ActivityAddContactBinding>() {

    private val viewModel: AddContactActivityVM by viewModels()

    override fun getLayoutResource(): Int {
        return R.layout.activity_add_contact
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.btnSaveContact -> {
                    when {
                        binding.etEnterName.text.toString().isEmpty() -> {
                            showToast("Please enter name")
                        }
                        binding.etEnterEmailAddress.text.toString().isEmpty() -> {
                            showToast("Please enter email address")
                        }
                        binding.etPhoneNumber.text.toString().isEmpty() -> {
                            showToast("Please enter phone number")
                        }
                        binding.etAddress.text.toString().isEmpty() -> {
                            showToast("Please enter address")
                        }
                        else -> {
                            showToast("Will be saved")
                            finish()
                        }
                    }
                }
                R.id.ivBack -> {
                    finish()
                }
            }
        }
    }

    private fun initView() {
        binding.tbAddContact.tvTitle.text = getString(R.string.add_contact)
        addContactTypeSpinnerAdapter()
        addRelationShipSpinnerAdapter()

        binding.spContactType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                val selectedItem =
                    parent.getItemAtPosition(position).toString() //this is your selected item
                if (selectedItem == "Care Provider") {
                    addCareSpinnerAdapter()
                    binding.tilEnterOrganisation.visibility = View.VISIBLE
                    binding.tilEnterRelationship.visibility = View.GONE
                }else{
                    addRelationShipSpinnerAdapter()
                    binding.tilEnterOrganisation.visibility = View.GONE
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        binding.spRelationship.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                val selectedItem =
                    parent.getItemAtPosition(position).toString() //this is your selected item
                if (binding.spContactType.selectedItem.toString() == "Family Members") {
                    if (selectedItem == "Other") {
                        binding.tilEnterRelationship.visibility = View.VISIBLE
                    } else {
                        binding.tilEnterRelationship.visibility = View.GONE
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun addContactTypeSpinnerAdapter() {
        val adapter = ArrayAdapter(
            this,
            R.layout.custom_spinner,
            R.id.tvSpinner,
            listOf("Family Member", "Care Provider")
        )
        adapter.setDropDownViewResource(R.layout.custom_spinner)
        binding.spContactType.adapter = adapter
    }

    private fun addRelationShipSpinnerAdapter() {
        val adapter = ArrayAdapter(
            this,
            R.layout.custom_spinner,
            R.id.tvSpinner,
            listOf("Self", "Son", "Daughter", "Spouse", "Partner", "Other")
        )
        adapter.setDropDownViewResource(R.layout.custom_spinner)
        binding.spRelationship.adapter = adapter
    }

    private fun addCareSpinnerAdapter() {
        val adapter = ArrayAdapter(
            this,
            R.layout.custom_spinner,
            R.id.tvSpinner,
            listOf("Physician", "Laboratory", "Dentist", "Radiologist", "Pharmacist")
        )
        adapter.setDropDownViewResource(R.layout.custom_spinner)
        binding.spRelationship.adapter = adapter
    }
}