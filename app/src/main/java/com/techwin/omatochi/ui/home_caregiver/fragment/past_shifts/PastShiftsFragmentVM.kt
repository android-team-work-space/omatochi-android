package com.techwin.omatochi.ui.home_caregiver.fragment.past_shifts

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PastShiftsFragmentVM @Inject constructor(): BaseViewModel() {
}