package com.techwin.omatochi.ui.welcome

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class WelcomeActivityVM @Inject constructor() : BaseViewModel() {

}