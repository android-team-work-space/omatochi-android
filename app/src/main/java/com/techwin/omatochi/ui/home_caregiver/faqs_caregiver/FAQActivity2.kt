package com.techwin.omatochi.ui.home_caregiver.faqs_caregiver

import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityFaqactivity2Binding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.utils.Constants
import io.ak1.pix.helpers.hide
import io.ak1.pix.helpers.show

class FAQActivity2 : BaseActivity<ActivityFaqactivity2Binding>() {

    private val viewModel: FAQActivity2VM by viewModels()

    override fun getLayoutResource(): Int {
        return R.layout.activity_faqactivity_2
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
            }
        }
    }

    private fun initView() {
        binding.tbFAQs.tvTitle.text = getString(R.string.faq)
        binding.pbFAQ.show()
        loadTermsAndConditionsUrl()
    }

    private fun loadTermsAndConditionsUrl() {
        binding.wvFAQ.settings.loadWithOverviewMode = true
        binding.wvFAQ.isVerticalScrollBarEnabled = true
        binding.wvFAQ.settings.javaScriptEnabled = true

        binding.wvFAQ.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                //binding.pbFAQ.show()
                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                binding.pbFAQ.hide()

            }
        }
        binding.wvFAQ.loadUrl(Constants.FAQ_URL)
    }
}