package com.techwin.omatochi.ui.home_payer.support

import android.widget.ArrayAdapter
import androidx.activity.viewModels
import com.freshchat.consumer.sdk.Freshchat
import com.freshchat.consumer.sdk.FreshchatConfig
import com.freshchat.consumer.sdk.FreshchatMessage
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivitySupportBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.utils.AppUtils.Companion.getSupportOptionsList
import com.techwin.omatochi.utils.Constants.Companion.FRESHDESK_APPID
import com.techwin.omatochi.utils.Constants.Companion.FRESHDESK_APP_KEY
import com.techwin.omatochi.utils.Constants.Companion.FRESHDESK_DOMAIN
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SupportActivity : BaseActivity<ActivitySupportBinding>() {
    private val viewModel: SupportActivityVM by viewModels()

    override fun getLayoutResource(): Int {
        return R.layout.activity_support
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
    }

    private fun initView() {
        binding.tbSupport.tvTitle.text = getString(R.string.omatochi_support)
        initSupportOptionsAdapter()
        initFreshDesk()
    }

    private fun initFreshDesk() {
        val config = FreshchatConfig(FRESHDESK_APPID, FRESHDESK_APP_KEY)
        config.domain = FRESHDESK_DOMAIN
        /*config.isCameraCaptureEnabled = true
        config.isGallerySelectionEnabled = true
        config.isResponseExpectationEnabled = true*/
        Freshchat.getInstance(this).init(config)
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
                R.id.btnSubmitSupportFeedback -> {
                    if (binding.edtUserProblemText.text.toString() == "") {
                        showToast("Please enter your message")
                    } else {
                        val tag = binding.edtUserProblemText.text.toString()
                        val msgText =
                            (binding.spSupportOptions.selectedItem.toString() + " : " + binding.edtUserProblemText.text.toString())
                        val freshChatMessage = FreshchatMessage().setTag(tag).setMessage(msgText)
                        Freshchat.sendMessage(this, freshChatMessage)
                        Freshchat.showConversations(this)
                    }
                }
                R.id.btnSupportBubbleGoToChat -> {
                    Freshchat.showConversations(this)
                }
            }
        }
    }

    private fun initSupportOptionsAdapter() {
        val adapter = ArrayAdapter(
            this,
            R.layout.custom_spinner,
            R.id.tvSpinner,
            getSupportOptionsList()
        )
        adapter.setDropDownViewResource(R.layout.custom_spinner)
        binding.spSupportOptions.adapter = adapter
    }
}