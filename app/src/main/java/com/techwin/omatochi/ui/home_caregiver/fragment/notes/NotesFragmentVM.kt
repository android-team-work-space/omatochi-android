package com.techwin.omatochi.ui.home_caregiver.fragment.notes

import com.techwin.omatochi.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NotesFragmentVM @Inject constructor(): BaseViewModel() {
}