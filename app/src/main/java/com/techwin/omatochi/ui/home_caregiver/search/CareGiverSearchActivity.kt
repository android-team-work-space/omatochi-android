package com.techwin.omatochi.ui.home_caregiver.search

import android.content.Intent
import android.view.View
import androidx.activity.viewModels
import com.techwin.omatochi.BR
import com.techwin.omatochi.R
import com.techwin.omatochi.data.model.InboxMessagesModel
import com.techwin.omatochi.databinding.CaregiverActivitySearchBinding
import com.techwin.omatochi.databinding.CustomCaregiverSearchItemBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.base.adapter.RVAdapter
import com.techwin.omatochi.ui.home_caregiver.new_user_chat.CareGiverNewChatActivity
import com.techwin.omatochi.utils.AppUtils

class CareGiverSearchActivity : BaseActivity<CaregiverActivitySearchBinding>() {

    private val viewModel: CareGiverSearchActivityVM by viewModels()
    private lateinit var searchAdapter: RVAdapter<InboxMessagesModel, CustomCaregiverSearchItemBinding>

    override fun getLayoutResource(): Int {
        return R.layout.caregiver_activity_search
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
            }
        }
    }

    private fun initView() {
        binding.tbSearch.tvTitle.text = getString(R.string.new_chat)
        initSearchAdapter()
    }

    private fun initSearchAdapter() {
        searchAdapter = RVAdapter(
            R.layout.custom_caregiver_search_item,
            BR.bean,
            object : RVAdapter.Callback<InboxMessagesModel> {
                override fun onItemClick(v: View?, m: InboxMessagesModel) {
                    startNewActivity(
                        Intent(
                            this@CareGiverSearchActivity,
                            CareGiverNewChatActivity::class.java
                        )
                    )
                }
            })
        binding.rvSearch.adapter = searchAdapter
        searchAdapter.list = AppUtils.getInboxMessagesList()
    }
}