package com.techwin.omatochi.ui.home_payer.fragment.dashboard.dashboard2

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.viewpager2.widget.ViewPager2
import com.github.mikephil.charting.charts.ScatterChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.interfaces.datasets.IScatterDataSet
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.SessionsClient
import com.google.android.gms.fitness.data.DataPoint
import com.google.android.gms.fitness.data.DataSet
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.google.android.gms.fitness.request.DataReadRequest
import com.google.android.gms.fitness.request.OnDataPointListener
import com.google.android.gms.fitness.request.SensorRequest
import com.google.android.gms.fitness.request.SessionReadRequest
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.FragmentDashboard2Binding
import com.techwin.omatochi.databinding.HealthKitDialogPopupBinding
import com.techwin.omatochi.ui.base.BaseFragment
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.ui.home_payer.HomePayerActivity
import com.techwin.omatochi.ui.home_payer.fragment.dashboard.DashboardFragment
import com.techwin.omatochi.ui.home_payer.fragment.dashboard.DashboardViewPager2
import com.techwin.omatochi.ui.home_payer.fragment.dashboard.ViewPagerBean
import com.techwin.omatochi.utils.chartUtils.ChartUtil.Companion.getEndTimeString
import com.techwin.omatochi.utils.chartUtils.ChartUtil.Companion.getGoogleAccount
import com.techwin.omatochi.utils.chartUtils.ChartUtil.Companion.getStartTimeString
import com.techwin.omatochi.utils.chartUtils.MyAxisValueFormatter
import com.techwin.omatochi.utils.dialog.BaseCustomDialog
import com.techwin.omatochi.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.absoluteValue


@AndroidEntryPoint
class Dashboard2Fragment : BaseFragment<FragmentDashboard2Binding>(),
    GoogleApiClient.OnConnectionFailedListener, OnChartValueSelectedListener,
    BaseCustomDialog.Listener, GoogleApiClient.ConnectionCallbacks {
    private val viewModel: Dashboard2FragmentVM by viewModels()
    private lateinit var sessionsClient: SessionsClient
    private var days = 0
    var perDayStepsList: ArrayList<Float>? = null
    var perDayCaloriesList: ArrayList<Float>? = null
    var perDaySleepList: ArrayList<Float>? = null
    var perDayHeartRateList: ArrayList<Float>? = null
    private var xAxisLabelsList: ArrayList<String>? = null
    private lateinit var popup: BaseCustomDialog<HealthKitDialogPopupBinding>
    private lateinit var homePayerActivity: HomePayerActivity
    private var mClient: GoogleApiClient? = null
    private val TAGG = "Dashboard2Fragment"
    var tfLight: Typeface? = null
    private var spinnerList = listOf("Sleep", "Heart Rate", "Calories", "Steps")

    companion object {
        var isLive = false
        fun newIntent(): Fragment {
            return Dashboard2Fragment()
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_dashboard2
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(view: View) {
        initView()
        initViewPager()
        initOnclick()
        initializeApiClient()
        initDialog()
        addHealthKitSpinner()
    }

    private fun initView() {
        homePayerActivity = activity as HomePayerActivity
        homePayerActivity.window.statusBarColor =
            ContextCompat.getColor(homePayerActivity, R.color.white)
    }

    private fun initViewPager() {
        val list: ArrayList<ViewPagerBean> = ArrayList()
        list.add(ViewPagerBean(""))
        list.add(ViewPagerBean(""))
        binding.viewPager2.adapter = DashboardViewPager2(list)
        binding.viewPager2.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        binding.indicator.setViewPager(binding.viewPager2)
    }

    private fun initOnclick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivMenu -> {
                    homePayerActivity.openSideMenu()
                }
            }
        }
    }

    private fun initializeApiClient() {
        if (mClient == null || mClient!!.isConnected) {
            mClient = GoogleApiClient.Builder(requireContext())
                .addApi(Fitness.SENSORS_API)
                .addApi(Fitness.HISTORY_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(object : GoogleApiClient.ConnectionCallbacks {
                    @RequiresApi(Build.VERSION_CODES.O)
                    override fun onConnected(bundle: Bundle?) {
                        Log.e(TAGG, "mClientOnConnected")
                    }

                    override fun onConnectionSuspended(i: Int) {
                        if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST) {
                            Log.i(TAGG, "Connection lost.  Cause: Network Lost.")
                        } else if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED) {
                            Log.i(TAGG, "Connection lost.  Reason: Service Disconnected")
                        }
                    }
                })
                .enableAutoManage(requireActivity()) { result ->
                    Log.e(
                        TAGG, "!_@@ERROR :: Google Play services connection failed. Cause: $result"
                    )
                }
                .build()
            //mClient!!.connect()
        }
    }

    private fun initDialog() {
        popup = BaseCustomDialog<HealthKitDialogPopupBinding>(
            requireContext(),
            R.layout.health_kit_dialog_popup,
            this
        )
    }

    private fun addHealthKitSpinner() {
        val adapter = ArrayAdapter(
            requireContext(),
            R.layout.custom_spinner_health_kit,
            R.id.tvSpinner, spinnerList
        )
        binding.spHealthKit.adapter = adapter
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isLive = true
        DashboardFragment.callBackLiveData.observe(requireActivity()) {
            if (it) {
                mClient?.stopAutoManage(requireActivity())
                mClient?.disconnect()
            }
        }

        perDayStepsList = ArrayList()
        perDayCaloriesList = ArrayList()
        perDayHeartRateList = ArrayList()
        perDaySleepList = ArrayList()
        xAxisLabelsList = ArrayList()
        tfLight = ResourcesCompat.getFont(requireContext(), R.font.mark_pro_bold)

        //getSleepHistory()
        //subscribeAndGetRealTimeData("steps", DataType.TYPE_STEP_COUNT_DELTA)
        //subscribeAndGetRealTimeData("heartRate", DataType.TYPE_HEART_POINTS)
        //subscribeAndGetRealTimeData("calories", DataType.TYPE_CALORIES_EXPENDED)

        val cal = Calendar.getInstance()
        val sdff = SimpleDateFormat("yyyy-MM-dd")
        val year = cal.get(Calendar.YEAR)
        val currentMonthDay = cal.get(Calendar.DAY_OF_MONTH)
        var month = cal.get(Calendar.MONTH)
        cal.add(Calendar.MONTH, -1)
        var previousMonth = cal.get(Calendar.MONTH)
        val previousMonthDay = cal.get(Calendar.DAY_OF_MONTH)

        month += 1
        previousMonth += 1
        days = getDaysDifference(sdff.parse("$year-$previousMonth-$previousMonthDay"), sdff.parse("$year-$month-$currentMonthDay"))
        var startDate = sdff.parse("$year-$previousMonth-$previousMonthDay")
        val endDate = sdff.parse("$year-$month-$currentMonthDay")
        val spf = SimpleDateFormat("MMM dd, yyyy")

        binding.tvDate.text = ("${spf.format(startDate)}" + " - " + "${spf.format(endDate)}")

        val call = Calendar.getInstance()
        val sdf = SimpleDateFormat("MMM d")
        call.add(Calendar.DAY_OF_YEAR, -days)

        for (i in 1..days) {
            call.add(Calendar.DAY_OF_YEAR, 1)
            xAxisLabelsList?.add(sdf.format(call.time))
        }

        initBarChart()
        initScatteredChart()

        binding.spHealthKit.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                binding.chart.invalidate()
                when (spinnerList[p2]) {
                    "Sleep" -> {
                        //getSleepHistory()
                        //getSleepHistoryPerMonth()
                        showToast("Work in progress")
                        //OnHold need to take input from user for sleep schedule
                    }
                    "Heart Rate" -> {
                        binding.tvTime.text = "177 BPM"
                        binding.tvAverage.text = "Checked :- April 12, 2022"
                        binding.chart.visibility = View.GONE
                        binding.scatteredChart.visibility = View.VISIBLE
                        perDayHeartRateList?.clear()
                        getHeartRatePointsHistoryPerMonth()
                    }
                    "Calorie" -> {
                        binding.tvAverage.text = "Average Calories"
                        binding.chart.visibility = View.VISIBLE
                        binding.scatteredChart.visibility = View.GONE
                        perDayCaloriesList?.clear()
                        getCalorieHistoryPerMonth()
                    }
                    "Steps" -> {
                        binding.tvAverage.text = "Average Steps"
                        binding.chart.visibility = View.VISIBLE
                        binding.scatteredChart.visibility = View.GONE
                        perDayStepsList?.clear()
                        getStepCountHistoryPerMonth()
                    }
                }
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {}
        }
    }

    private fun initScatteredChart() {
        binding.scatteredChart.description.isEnabled = false
        binding.scatteredChart.setOnChartValueSelectedListener(this)
        binding.scatteredChart.setDrawGridBackground(false)
        binding.scatteredChart.setTouchEnabled(true)
        binding.scatteredChart.xAxis.position = XAxisPosition.BOTTOM
        binding.scatteredChart.xAxis.valueFormatter = IndexAxisValueFormatter(xAxisLabelsList)
        binding.scatteredChart.axisRight.valueFormatter = MyAxisValueFormatter()
        binding.scatteredChart.isDragEnabled = false
        binding.scatteredChart.setScaleEnabled(false)
        binding.scatteredChart.setPinchZoom(true)
        binding.scatteredChart.isHighlightPerDragEnabled = false
        //binding.scatteredChart.isHighlightPerTapEnabled = false

        val l: Legend = binding.scatteredChart.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.VERTICAL
        l.setDrawInside(false)
        l.typeface = tfLight
        l.xOffset = 5f

        val yl: YAxis = binding.scatteredChart.axisLeft
        yl.typeface = tfLight
        yl.axisMinimum = 0f // this replaces setStartAtZero(true)
        binding.scatteredChart.axisRight.isEnabled = false

        val xl: XAxis = binding.scatteredChart.xAxis
        xl.typeface = tfLight
        xl.setDrawGridLines(false)

        binding.scatteredChart.animateY(5000)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun setScatteredData(perDayHeartRateList: ArrayList<Float>?) {
        val values1 = ArrayList<Entry>()
        for (i in 0 until perDayHeartRateList!!.size) {
            values1.add(Entry(i.toFloat(), perDayHeartRateList[i]))
        }

        // create a dataset and give it a type
        // create a dataset and give it a type
        val set1 = ScatterDataSet(values1, "")
        set1.setDrawValues(true)
        set1.setDrawValues(false)
        set1.setScatterShape(ScatterChart.ScatterShape.TRIANGLE)
        set1.color = requireContext().getColor(R.color.blue)
        val dataSets = ArrayList<IScatterDataSet>()
        dataSets.add(set1) // add the data sets

        // create a data object with the data sets
        // create a data object with the data sets
        val data = ScatterData(dataSets)
        data.setValueTypeface(tfLight)
        binding.scatteredChart.data = data
        binding.scatteredChart.invalidate()
    }

    private fun subscribeAndGetRealTimeData(type: String, dataType: DataType) {
        Fitness.getRecordingClient(requireContext(), getGoogleAccount(requireContext()))
            .subscribe(dataType)
            .addOnSuccessListener {
                Log.i(TAGG, "Subscribed")
            }
            .addOnFailureListener {
                Log.i(TAGG, "Failed to subscribe" + it.localizedMessage)
            }
        getDataUsingSensor(type, dataType)
    }

    private fun getDataUsingSensor(type: String, d: DataType) {
        Fitness.getSensorsClient(requireContext(), getGoogleAccount(requireContext()))
            .add(SensorRequest.Builder().setDataType(d)
                .setSamplingRate(1, TimeUnit.MILLISECONDS)
                .build(),
                object : OnDataPointListener, (DataPoint) -> Unit {
                    @RequiresApi(Build.VERSION_CODES.O)
                    override fun onDataPoint(dataPoints: DataPoint) {
                        Log.i(TAGG, dataPoints.getValue(Field.FIELD_STEPS).asInt().toString())
                        when (type) {
                            "steps" -> {
                                //getStepsCount()
                            }
                            "heartRate" -> {
                                //getHeartRate()
                            }
                            "calories" -> {
                                //getCaloriesCount()
                            }
                        }
                    }

                    override fun invoke(p1: DataPoint) {

                    }
                })
    }

    private fun getDaysDifference(fromDate: Date?, toDate: Date?): Int {
        return if (fromDate == null || toDate == null) 0 else ((toDate.time - fromDate.time) / (1000 * 60 * 60 * 24)).toInt()
    }

    private fun initBarChart() {
        binding.chart.setOnChartValueSelectedListener(this)
        binding.chart.setDrawBarShadow(false)
        binding.chart.setDrawValueAboveBar(true)
        binding.chart.description.isEnabled = false
        binding.chart.setMaxVisibleValueCount(0)
        binding.chart.setPinchZoom(false)
        binding.chart.xAxis.labelCount = 8
        binding.chart.isDoubleTapToZoomEnabled = false
        binding.chart.isAutoScaleMinMaxEnabled = false
        binding.chart.setDrawGridBackground(false)
        binding.chart.isDragEnabled = false
        binding.chart.setScaleEnabled(false)
        binding.chart.xAxis.position = XAxisPosition.BOTTOM
        Log.i(TAGG, xAxisLabelsList.toString())
        binding.chart.xAxis.valueFormatter = IndexAxisValueFormatter(xAxisLabelsList)
        binding.chart.axisRight.valueFormatter = MyAxisValueFormatter()
        val custom = MyAxisValueFormatter()
        val leftAxis = binding.chart.axisLeft
        leftAxis.typeface = tfLight
        leftAxis.setLabelCount(8, false)
        leftAxis.valueFormatter = custom
        leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART)
        leftAxis.spaceTop = 15f
        leftAxis.axisMinimum = 0f // this replaces setStartAtZero(true)

        val rightAxis = binding.chart.axisRight
        rightAxis.setDrawGridLines(false)
        rightAxis.setLabelCount(0, true)
        rightAxis.setDrawLabels(false)
        rightAxis.setDrawTopYLabelEntry(false)
        rightAxis.setDrawZeroLine(false)
        rightAxis.setDrawTopYLabelEntry(false)
        // rightAxis.valueFormatter = xAxisFormatter
        rightAxis.spaceTop = 0f
        rightAxis.axisMinimum = 0f // this replaces setStartAtZero(true)
    }

    private fun setData(type: String) {
        val values = ArrayList<BarEntry>()
        if (type == "steps") {
            for (i in 0..perDayStepsList!!.size - 1) {
                values.add(BarEntry(i.toFloat(), perDayStepsList!![i]))
            }
            val av = perDayStepsList?.average()
            binding.tvTime.text = av.toString()
        } else if (type == "calorie") {
            for (i in 0..perDayCaloriesList!!.size - 1) {
                values.add(BarEntry(i.toFloat(), perDayCaloriesList!![i]))
            }
            val av = perDayCaloriesList?.average()
            binding.tvTime.text = av.toString()
        }


        val set1: BarDataSet
        if (binding.chart.data != null &&
            binding.chart.data.dataSetCount > 0
        ) {
            set1 = binding.chart.data.getDataSetByIndex(0) as BarDataSet
            set1.values = values
            binding.chart.data.notifyDataChanged()
            binding.chart.notifyDataSetChanged()
        } else {
            set1 = BarDataSet(values, "The year 2022")
            set1.setDrawIcons(false)
            set1.color = ContextCompat.getColor(requireContext(), R.color.blue)
            val dataSets = ArrayList<IBarDataSet>()
            dataSets.add(set1)
            val data = BarData(dataSets)
            data.setValueTextSize(10f)
            data.setValueTypeface(tfLight)
            data.barWidth = 0.9f
            binding.chart.data = data
            binding.chart.notifyDataSetChanged()
        }
        binding.chart.animateY(1000)
        binding.chart.invalidate()
    }

    private fun getSleepHistory() {
        val cal: Calendar = Calendar.getInstance()
        val now = Date()
        cal.time = now
        val endTime: Long = cal.timeInMillis
        cal.add(Calendar.MONTH, -1)
        val startTime: Long = cal.timeInMillis

        val SLEEP_STAGE_NAMES = arrayOf(
            "Unused",
            "Awake (during sleep)",
            "Sleep",
            "Out-of-bed",
            "Light sleep",
            "Deep sleep",
            "REM sleep")

        val request = SessionReadRequest.Builder()
            .readSessionsFromAllApps()
            // By default, only activity sessions are included, so it is necessary to explicitly
            // request sleep sessions. This will cause activity sessions to be *excluded*.
            .includeSleepSessions()
            // Sleep segment data is required for details of the fine-granularity sleep, if it is present.
            .read(DataType.TYPE_SLEEP_SEGMENT)
            .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS)
            .build()
        sessionsClient = Fitness.getSessionsClient(requireContext(), getGoogleAccount(requireContext()))
        sessionsClient.readSession(request)
            .addOnSuccessListener { response ->
                for (session in response.sessions) {
                    val sessionStart = session.getStartTime(TimeUnit.MILLISECONDS)
                    val sessionEnd = session.getEndTime(TimeUnit.MILLISECONDS)
                    Log.i(TAGG, "Sleep between $sessionStart and $sessionEnd")

                    // If the sleep session has finer granularity sub-components, extract them:
                    val dataSets = response.getDataSet(session)
                    for (dataSet in dataSets) {
                        for (point in dataSet.dataPoints) {
                            val sleepStageVal = point.getValue(Field.FIELD_SLEEP_SEGMENT_TYPE).asInt()
                            val sleepStage = SLEEP_STAGE_NAMES[sleepStageVal]
                            val segmentStart = point.getStartTime(TimeUnit.MILLISECONDS)
                            val segmentEnd = point.getEndTime(TimeUnit.MILLISECONDS)
                            Log.i(TAGG, "\t* Type $sleepStage between $segmentStart and $segmentEnd")
                        }
                    }
                }
            }

        val request1 = SessionReadRequest.Builder()
            .includeSleepSessions()
            .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS)
            .read(DataType.TYPE_SLEEP_SEGMENT)
            .readSessionsFromAllApps()
            //.enableServerQueries()
            .build()

        Fitness.getSessionsClient(requireContext(), getGoogleAccount(requireContext()))
            .readSession(request)
            .addOnSuccessListener { response ->
                for (session in response.sessions) {
                    val sessionStart = session.getStartTime(TimeUnit.MILLISECONDS)
                    val sessionEnd = session.getEndTime(TimeUnit.MILLISECONDS)
                    Log.i(TAGG, "Sleep between $sessionStart and $sessionEnd")

                    val dataSets = response.getDataSet(session)
                    for (dataSet in dataSets) {
                        for (point in dataSet.dataPoints) {
                            val sleepStageVal = point.getValue(Field.FIELD_SLEEP_SEGMENT_TYPE).asInt()
                            val sleepStage = SLEEP_STAGE_NAMES[sleepStageVal]
                            val segmentStart = point.getStartTime(TimeUnit.MILLISECONDS)
                            val segmentEnd = point.getEndTime(TimeUnit.MILLISECONDS)
                            Log.i(TAGG, "\t* Type $sleepStage between $segmentStart and $segmentEnd")
                        }
                    }
                }
            }
            .addOnFailureListener {
                Log.i(TAGG, it.localizedMessage)
            }
        // val a = ArrayList<Fill>
    }

    private fun getCaloriesCount() {
        Fitness.getHistoryClient(requireContext(), getGoogleAccount(requireContext()))
            .readDailyTotal(DataType.TYPE_CALORIES_EXPENDED)
            .addOnSuccessListener { result ->
                val totalSteps = result.dataPoints[0].getValue(Field.FIELD_CALORIES).toString()
                Log.i(TAGG, totalSteps.toString())
                // Do something with totalSteps
            }
            .addOnFailureListener { e ->
                Log.i(TAGG, "There was a problem getting calories.", e)
            }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getStepCountHistoryPerMonth() {
        val cal = Calendar.getInstance()
        val now = Date()
        cal.time = now
        val endTime: Long = cal.timeInMillis
        cal.add(Calendar.MONTH, -1)
        val startTime: Long = cal.timeInMillis

        val readRequest =
            DataReadRequest.Builder()
                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build()

        Fitness.getHistoryClient(requireContext(), getGoogleAccount(requireContext()))
            .readData(readRequest)
            .addOnSuccessListener { response ->
                Log.i(TAGG, response.toString())
                for (dataSet in response.buckets.flatMap {
                    it.dataSets
                }) {
                    dumpDataSet(dataSet)
                }
                Log.i(TAGG, perDayStepsList.toString())
                setData("steps")
            }
            .addOnFailureListener { e ->
                Log.i(TAGG, "There was an error reading data from Google Fit", e)
            }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getSleepHistoryPerMonth() {
        val cal = Calendar.getInstance()
        val now = Date()
        cal.time = now
        val endTime: Long = cal.timeInMillis
        cal.add(Calendar.MONTH, -1)
        val startTime: Long = cal.timeInMillis

        val readRequest =
            DataReadRequest.Builder()
                .aggregate(DataType.TYPE_SLEEP_SEGMENT, DataType.AGGREGATE_ACTIVITY_SUMMARY)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build()

        Fitness.getHistoryClient(requireContext(), getGoogleAccount(requireContext()))
            .readData(readRequest)
            .addOnSuccessListener { response ->
                Log.i(TAGG, response.toString())
                for (dataSet in response.buckets.flatMap {
                    it.dataSets
                }) {
                    var totalSleepPerDay = 0f
                    for (dp in dataSet.dataPoints) {
                        Log.i(TAGG, "\tType: ${dp.dataType.name}")
                        for (field in dp.dataType.fields) {
                            totalSleepPerDay = dp.getValue(field).toString().toFloat()
                            Log.i(TAGG, "\tField: ${field.name} Value: ${dp.getValue(field)}")
                        }
                    }
                    perDaySleepList!!.add(totalSleepPerDay)
                    Log.i(TAGG, perDaySleepList.toString())
                }
            }
            .addOnFailureListener { e ->
                Log.i(TAGG, "There was an error reading data from Google Fit", e)
            }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            RESULT_OK -> {
                getStepCountHistoryPerMonth()
            }
            else -> {
                // Handle error.
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getCalorieHistoryPerMonth() {
        val cal = Calendar.getInstance()
        val now = Date()
        cal.time = now
        val endTime: Long = cal.timeInMillis
        cal.add(Calendar.MONTH, -1)
        val startTime: Long = cal.timeInMillis

        val readRequest =
            DataReadRequest.Builder()
                .aggregate(DataType.AGGREGATE_CALORIES_EXPENDED)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build()

        Fitness.getHistoryClient(requireContext(), getGoogleAccount(requireContext()))
            .readData(readRequest)
            .addOnSuccessListener { response ->
                Log.i(TAGG, response.toString())
                for (dataSet in response.buckets.flatMap {
                    it.dataSets
                }) {
                    var totalCaloriePerDay = 0f
                    for (dp in dataSet.dataPoints) {
                        Log.i(TAGG, "Data point:")
                        Log.i(TAGG, "\tType: ${dp.dataType.name}")
                        Log.i(TAGG, "\tStart: ${dp.getStartTimeString()}")
                        Log.i(TAGG, "\tEnd: ${dp.getEndTimeString()}")
                        for (field in dp.dataType.fields) {
                            totalCaloriePerDay = dp.getValue(Field.FIELD_CALORIES).toString().toFloat()
                            Log.i(TAGG, "\tField: ${field.name} Value: ${dp.getValue(field)}")
                        }
                    }
                    perDayCaloriesList!!.add(totalCaloriePerDay)
                }
                Log.i(TAGG, perDayCaloriesList.toString())
                setData("calorie")
            }
            .addOnFailureListener { e ->
                Log.i(TAGG, "There was an error reading data from Google Fit", e)
            }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getHeartRatePointsHistoryPerMonth() {
        val cal = Calendar.getInstance()
        val now = Date()
        cal.time = now
        val endTime: Long = cal.timeInMillis
        cal.add(Calendar.MONTH, -1)
        val startTime: Long = cal.timeInMillis
        var heartRatePerDay = 0f

        val readRequest =
            DataReadRequest.Builder()
                .aggregate(DataType.TYPE_HEART_POINTS)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build()

        Fitness.getHistoryClient(requireContext(), getGoogleAccount(requireContext()))
            .readData(readRequest)
            .addOnSuccessListener { response ->
                Log.i(TAGG, response.toString())
                for (dataSet in response.buckets.flatMap {
                    it.dataSets
                }) {
                    for (dp in dataSet.dataPoints) {
                        Log.i(TAGG, "Data point:")
                        Log.i(TAGG, "\tType: ${dp.dataType.name}")
                        Log.i(TAGG, "\tStart: ${dp.getStartTimeString()}")
                        Log.i(TAGG, "\tEnd: ${dp.getEndTimeString()}")
                        for (field in dp.dataType.fields) {
                            heartRatePerDay = dp.getValue(Field.FIELD_INTENSITY).asFloat()
                            Log.i(
                                TAGG,
                                "\tHeart Rate Point: ${field.name} Value: ${dp.getValue(Field.FIELD_INTENSITY)}"
                            )
                        }
                    }
                    perDayHeartRateList?.add(heartRatePerDay)
                    Log.i(TAGG, perDayHeartRateList.toString())
                }
                setScatteredData(perDayHeartRateList)
            }
            .addOnFailureListener { e ->
                Log.i(TAGG, "There was an error reading data from Google Fit", e)
            }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun dumpDataSet(dataSet: DataSet) {
        var totalStepsPerDay = 0
        var totalCaloriePerDay = 0f

        for (dp in dataSet.dataPoints) {
            Log.i(TAGG, "Data point:")
            Log.i(TAGG, "\tType: ${dp.dataType.name}")
            Log.i(TAGG, "\tStart: ${dp.getStartTimeString()}")
            Log.i(TAGG, "\tEnd: ${dp.getEndTimeString()}")
            for (field in dp.dataType.fields) {
                if (dp.dataType.name == "com.google.calories.expended") {
                    totalCaloriePerDay = dp.getValue(field).toString().toFloat()
                } else {
                    totalStepsPerDay = dp.getValue(field).toString().toInt()
                }
                Log.i(TAGG, "\tField: ${field.name} Value: ${dp.getValue(field)}")
            }
        }
        if (dataSet.dataType.name == "com.google.calories.expended") {
            perDayCaloriesList!!.add(totalCaloriePerDay)
        } else {
            perDayStepsList!!.add(totalStepsPerDay.toFloat())
        }
    }

    private fun getStepsCount() {
        Fitness.getHistoryClient(requireContext(), getGoogleAccount(requireContext()))
            .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
            .addOnSuccessListener { result ->
                val totalSteps =
                    result.dataPoints[0].getValue(Field.FIELD_STEPS).toString()
                Log.i(TAGG, totalSteps)
            }
            .addOnFailureListener { e ->
                Log.i(TAGG, "There was a problem getting steps.", e)
            }
    }



    override fun onPause() {
        super.onPause()
        isLive = false
    }

    override fun onStop() {
        super.onStop()
        isLive = false

    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.i(TAGG, p0.errorMessage.toString())
        //showToast("onConnectionFailed")
    }



    override fun onValueSelected(e: Entry?, h: Highlight?) {
        if (this::popup.isInitialized) {
            popup.dismiss()
            popup.binding.tvStepCount.text = ("Value : " + e?.y?.absoluteValue.toString())
            popup.binding.tvDate.text =
                ("Date : " + xAxisLabelsList!![e?.x?.absoluteValue!!.toInt()])
            popup.setCancelable(true)
            popup.setCanceledOnTouchOutside(true)
            popup.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            popup.show()
        } else {
            popup.binding.tvStepCount.text = ("Value : " + e?.y?.absoluteValue.toString())
            popup.binding.tvDate.text =
                ("Date : " + xAxisLabelsList!![e?.x?.absoluteValue!!.toInt()])
            popup.setCancelable(true)
            popup.setCanceledOnTouchOutside(true)
            popup.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            popup.show()
        }
    }

    override fun onNothingSelected() {}

    override fun onViewClick(view: View?) {
        showToast("Clicked on BAR")
    }

    override fun onConnected(p0: Bundle?) {
        Log.i(TAGG, p0.toString())
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.i(TAGG, p0.toString())
    }
}