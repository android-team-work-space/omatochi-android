package com.techwin.omatochi.ui.home_caregiver.terms_and_conditions_caregiver

import android.os.Looper
import androidx.activity.viewModels
import com.techwin.omatochi.R
import com.techwin.omatochi.databinding.ActivityTermsAndConditionsCgBinding
import com.techwin.omatochi.ui.base.BaseActivity
import com.techwin.omatochi.ui.base.BaseViewModel
import com.techwin.omatochi.utils.Constants
import com.techwin.omatochi.utils.RetrivePDFfromUrl
import dagger.hilt.android.AndroidEntryPoint
import io.ak1.pix.helpers.hide
import io.ak1.pix.helpers.show

@AndroidEntryPoint
class TermsAndConditionsActivityCG : BaseActivity<ActivityTermsAndConditionsCgBinding>() {
    private val viewModel: TermsAndConditionsActivityCGVM by viewModels()

    override fun getLayoutResource(): Int {
        return R.layout.activity_terms_and_conditions_cg
    }

    override fun getViewModel(): BaseViewModel{
        return viewModel
    }

    override fun onCreateView() {
        initView()
        initOnClick()
    }

    private fun initOnClick() {
        viewModel.onClick.observe(this) {
            when (it?.id) {
                R.id.ivBack -> {
                    finish()
                }
            }
        }
    }

    private fun initView() {
        binding.tbTermsAndConditions.tvTitle.text = getString(R.string.terms_and_conditions)
        binding.pbTermsAndConditionsCG.show()
        loadTermsAndConditionsUrl()
    }

    private fun loadTermsAndConditionsUrl() {
        android.os.Handler(Looper.myLooper()!!).postDelayed({
            binding.pbTermsAndConditionsCG.hide()
        },1500)
        val retrievePDFFromUrl = RetrivePDFfromUrl(binding.idPDFView)
        retrievePDFFromUrl.execute(Constants.TERMS_CONDITIONS_URL)
    }

}

