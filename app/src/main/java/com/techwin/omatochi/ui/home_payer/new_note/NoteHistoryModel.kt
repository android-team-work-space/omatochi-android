package com.techwin.omatochi.ui.home_payer.new_note

class NoteHistoryModel(
    var time: String,
    var name: String,
    var des: String,
    var lastLineView: Boolean = false
)